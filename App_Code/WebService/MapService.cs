﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.Web.Security;
using System.Web.UI;

/// <summary>
/// Summary description for MapService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class MapService : System.Web.Services.WebService {

    public MapService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }





    [WebMethod]
    public List<TrackDetails> GetTracksOfMobEmpAfterLast(int userInfoId, long TrackId)
    {
        TrackDB trackDb = new TrackDB();
        List<TrackDetails> tracks = new List<TrackDetails>();
        tracks = trackDb.GetTracksOfUserAfterLastTrack(userInfoId, TrackId);
        return tracks;
    }
    [WebMethod]
    public List<LocationDetails> GetLocationsOfCurrentTrackAfterLastLocation(long TrackId, long lastLocationId)
    {
        LocationDB locDb = new LocationDB();
        List<LocationDetails> locOfTrack = locDb.GetLocationsOfTrackAfterLastLocation(TrackId, lastLocationId);
        return locOfTrack;
    }



    [WebMethod(EnableSession=true)]
    public List<UserInfoDetails> GetMobileEmpUserInfоDetails()
    {
        Guid UserId = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());
        SubordinationDB subDb = new SubordinationDB();
        List<UserInfoDetails> UsersInfo = new List<UserInfoDetails>();
        UsersInfo = subDb.GetSubordinationsUsersInfoDetailByUserId(UserId);

        TrackDB trackDb = new TrackDB();
        foreach (UserInfoDetails uid in UsersInfo)
        {
            uid.Tracks = trackDb.GetTracksOfUser(uid.ID);
        }
        LocationDB locDb = new LocationDB();
        foreach (UserInfoDetails uid in UsersInfo)
        {
            foreach (TrackDetails track in uid.Tracks)
            {
                track.Locations = locDb.GetLocationsOfTrackDetails(track.TrackId);
            }
        }        
        return UsersInfo;
    }

    [WebMethod(EnableSession=true)]
    public List<DestinationStationDetails> GetDestinationStationsOfSupervisor()
    {
        Guid supervisorUserId = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());
        DestinationStationDB dsDb = new DestinationStationDB();
        List<DestinationStationDetails> lstDestinationStation = dsDb.GetDestinationStationsOfSupervisor(supervisorUserId);
        return lstDestinationStation;
        
    }

    [WebMethod]
    public long RemoveDestinationStation(long id)
    {
        DestinationStationDB dsDb = new DestinationStationDB();
        dsDb.RemoveDestinationStation(id);
        return id;
    }

}
