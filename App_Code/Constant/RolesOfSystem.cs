﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RolesOfSystem
/// </summary>
public class RolesOfTraceTrack
{
    public const string Admin = "Admin";
    public const string Manager = "Manager";
    public const string Supervisor = "Supervisor";
    public const string MobileEmployee = "MobileEmployee";
}