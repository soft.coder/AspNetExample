﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AppStateTraceTrack
/// </summary>
public class AppStateTraceTrack
{
    public const string GuidOfUser = "GuidOfUser";
    public const string UsersTracks = "UsersTracks";
    public const string LastTrackOfUsers = "LastTrackOfUsers";
}