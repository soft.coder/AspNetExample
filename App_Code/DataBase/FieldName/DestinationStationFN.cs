﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DestinationStationFN
/// </summary>
public static class DestinationStationFN
{
    public const string DestinationStationId = "DestinationStationId";
    public const string UserInfoCommonId = "UserInfoCommonId";
    public const string Longitude = "Longitude";
    public const string Latitude = "Latitude";
    public const string Title = "Title";
    public const string TaskDescription = "TaskDescription";
    public const string TimeAssign = "TimeAssign";
    public const string TimeComplete = "TimeComplete";
    public const string IsCanceled = "IsCanceled";
}