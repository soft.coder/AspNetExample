﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for CompanyDB
/// </summary>
public class CompanyDB
{
    private string ConnectionString { get; set; }

	public CompanyDB()
	{
        this.ConnectionString = WebConfigurationManager.ConnectionStrings["TraceTrackMsSqlServer"].ConnectionString;
	}
    public DataSet GetCompany(int CompanyId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetCompany", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@CompanyId", SqlDbType.Int);
        cmd.Parameters["@CompanyId"].Value = CompanyId;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "Company");
        return ds;
    }
    public DataSet GetCompanies()
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetCompanies", con);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "Company");
        return ds;
    }
    public int? InsertCompany(string CompanyName, string ContactPerson, string Email, string WebSite, long? TownId, string Address)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("InsertCompany", con);
        cmd.CommandType = CommandType.StoredProcedure;
        
        cmd.Parameters.Add("@CompanyName", SqlDbType.VarChar, 500);
        cmd.Parameters["@CompanyName"].IsNullable = false;
        if (CompanyName == string.Empty)
            throw new ArgumentException("CompanyName emtpy string");
        else
            cmd.Parameters["@CompanyName"].Value = CompanyName;

        cmd.Parameters.Add("@ContactPerson", SqlDbType.VarChar, 500);
        cmd.Parameters["@ContactPerson"].IsNullable = true;
        if (ContactPerson != string.Empty)
            cmd.Parameters["@ContactPerson"].Value = ContactPerson;
        else
            cmd.Parameters["@ContactPerson"].Value = DBNull.Value;

        cmd.Parameters.Add("@Email", SqlDbType.VarChar, 300);
        cmd.Parameters["@Email"].IsNullable = true;
        if (Email != string.Empty)
            cmd.Parameters["@Email"].Value = Email;
        else
            cmd.Parameters["@Email"].Value = DBNull.Value;

        cmd.Parameters.Add("@WebSite", SqlDbType.VarChar, 300);
        cmd.Parameters["@WebSite"].IsNullable = true;
        if (WebSite != string.Empty)
            cmd.Parameters["@WebSite"].Value = WebSite;
        else
            cmd.Parameters["@WebSite"].Value = DBNull.Value;

        cmd.Parameters.Add("@TownId", SqlDbType.BigInt);
        cmd.Parameters["@TownId"].IsNullable = true;
        if (TownId.HasValue)
            cmd.Parameters["@TownId"].Value = TownId;
        else
            cmd.Parameters["@TownId"].Value = DBNull.Value;

        cmd.Parameters.Add("@Address", SqlDbType.VarChar, 500);
        cmd.Parameters["@Address"].IsNullable = true;
        if (Address != string.Empty)
            cmd.Parameters["@Address"].Value = Address;
        else
            cmd.Parameters["@Address"].Value = DBNull.Value;



        cmd.Parameters.Add("@CompanyId", SqlDbType.Int);
        cmd.Parameters["@CompanyId"].Direction = ParameterDirection.Output;

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@CompanyId"].Value != DBNull.Value)
                return (int)cmd.Parameters["@CompanyId"].Value;
            else
                return null;
        }
        catch (SqlException ex)
        {
            throw new ApplicationException("Data error");
        }
        finally
        {
            con.Close();
        }
    }
}