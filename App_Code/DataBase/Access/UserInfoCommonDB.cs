﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;


/// <summary>
/// Summary description for UserInfoCommonDB
/// </summary>
public class UserInfoCommonDB
{
    private string ConnectionString { get; set; }
	public UserInfoCommonDB()
	{
        this.ConnectionString = WebConfigurationManager.ConnectionStrings["TraceTrackMsSqlServer"].ConnectionString;
    }


    public int? GetUserInfoCommonId(Guid UserId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetUserInfoCommonId", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
        cmd.Parameters["@UserId"].IsNullable = false;
        cmd.Parameters["@UserId"].Value = UserId;
        
        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].Direction = ParameterDirection.Output;

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@UserInfoCommonId"].Value != DBNull.Value)
                return (int?)cmd.Parameters["@UserInfoCommonId"].Value;
            else
                return null;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }
    public DataSet GetUserInfo(Guid UserId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetUserInfo", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
        cmd.Parameters["@UserId"].Value = UserId;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "UserInfo");
        return ds;
    }
    public DataSet GetUsersInfo()
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetUsersInfo", con);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "UserInfo");
        return ds;
    }
    public int? InsertUserInfo(Guid UserId, int? CompanyId, string FirstName, string LastName, string Patronymic, long? TownId, 
                                string Address, bool? Sex, DateTime? DateOfBirth, byte[] Foto, string FotoMimeType, string WebSite)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("InsertUserInfo", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
        cmd.Parameters["@UserId"].IsNullable = false;
        if (UserId != null)
            cmd.Parameters["@UserId"].Value = UserId;
        else
            throw new ArgumentException("UserId is null");
        
        cmd.Parameters.Add("@CompanyId", SqlDbType.Int);
        cmd.Parameters["@CompanyId"].IsNullable = true;
        if (CompanyId.HasValue)
            cmd.Parameters["@CompanyId"].Value = CompanyId;
        else
            cmd.Parameters["@CompanyId"].Value = DBNull.Value;
        
        cmd.Parameters.Add("@FirstName", SqlDbType.VarChar, 200);
        cmd.Parameters["@FirstName"].IsNullable = true;
        if (FirstName != string.Empty)
            cmd.Parameters["@FirstName"].Value = FirstName;
        else
            cmd.Parameters["@FirstName"].Value = DBNull.Value;

        cmd.Parameters.Add("@LastName", SqlDbType.VarChar, 200);
        cmd.Parameters["@LastName"].IsNullable = true;
        if (LastName != string.Empty)
            cmd.Parameters["@LastName"].Value = LastName;
        else
            cmd.Parameters["@LastName"].Value = DBNull.Value;

        cmd.Parameters.Add("@Patronymic", SqlDbType.VarChar, 200);
        cmd.Parameters["@Patronymic"].IsNullable = true;
        if (Patronymic != string.Empty)
            cmd.Parameters["@Patronymic"].Value = Patronymic;
        else
            cmd.Parameters["@Patronymic"].Value = DBNull.Value;

        cmd.Parameters.Add("@TownId", SqlDbType.BigInt);
        cmd.Parameters["@TownId"].IsNullable = true;
        if(TownId.HasValue)
            cmd.Parameters["@TownId"].Value = TownId;
        else
            cmd.Parameters["@TownId"].Value = DBNull.Value;
        
        cmd.Parameters.Add("@Address", SqlDbType.VarChar, 500);
        cmd.Parameters["@Address"].IsNullable = true;
        if (Address != string.Empty)
            cmd.Parameters["@Address"].Value = Address;
        else
            cmd.Parameters["@Address"].Value = DBNull.Value;

        cmd.Parameters.Add("@Sex", SqlDbType.Bit);
        cmd.Parameters["@Sex"].IsNullable = true;
        if(Sex.HasValue)
            cmd.Parameters["@Sex"].Value = Sex;
        else
            cmd.Parameters["@Sex"].Value = DBNull.Value;
        
        cmd.Parameters.Add("@DateOfBirth", SqlDbType.Date);
        cmd.Parameters["@DateOfBirth"].IsNullable = true;
        if(DateOfBirth.HasValue)
            cmd.Parameters["@DateOfBirth"].Value = DateOfBirth;
        else
            cmd.Parameters["@DateOfBirth"].Value = DBNull.Value;
        
        cmd.Parameters.Add("@Foto", SqlDbType.VarBinary, -1);
        cmd.Parameters["@Foto"].IsNullable = true;
        if ( Foto != null)
            cmd.Parameters["@Foto"].Value = Foto;
        else
            cmd.Parameters["@Foto"].Value = DBNull.Value;

        cmd.Parameters.Add("@FotoMimeType", SqlDbType.VarChar, 100);
        cmd.Parameters["@FotoMimeType"].IsNullable = true;
        if (FotoMimeType != string.Empty)
            cmd.Parameters["@FotoMimeType"].Value = FotoMimeType;
        else
            cmd.Parameters["@FotoMimeType"].Value = DBNull.Value;
        
        cmd.Parameters.Add("@WebSite", SqlDbType.VarChar, 200);
        cmd.Parameters["@WebSite"].IsNullable = true;
        if (WebSite != string.Empty)
            cmd.Parameters["@WebSite"].Value = WebSite;
        else
            cmd.Parameters["@WebSite"].Value = DBNull.Value;


        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].Direction = ParameterDirection.Output;

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@UserInfoCommonId"].Value != DBNull.Value)
                return (int?)cmd.Parameters["@UserInfoCommonId"].Value;
            else
                return null;
        }
        catch (SqlException ex)
        {
            //throw new ApplicationException("Data error");
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }
    public void UpdateUserInfo(int UserInfoCommonId, int? CompanyId, string FirstName, string LastName, string Patronymic, long? TownId,
                                string Address, bool? Sex, DateTime? DateOfBirth, byte[] Foto, string FotoMimeType, string WebSite)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("UpdateUserInfo", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].IsNullable = false;
        if (UserInfoCommonId != null)
            cmd.Parameters["@UserInfoCommonId"].Value = UserInfoCommonId;
        else
            throw new ArgumentException("UserInfoCommonId is null");

        cmd.Parameters.Add("@CompanyId", SqlDbType.Int);
        cmd.Parameters["@CompanyId"].IsNullable = true;
        if (CompanyId.HasValue)
            cmd.Parameters["@CompanyId"].Value = CompanyId;
        else
            cmd.Parameters["@CompanyId"].Value = DBNull.Value;

        cmd.Parameters.Add("@FirstName", SqlDbType.VarChar, 200);
        cmd.Parameters["@FirstName"].IsNullable = true;
        if (FirstName != string.Empty && FirstName != null)
            cmd.Parameters["@FirstName"].Value = FirstName;
        else
            cmd.Parameters["@FirstName"].Value = DBNull.Value;

        cmd.Parameters.Add("@LastName", SqlDbType.VarChar, 200);
        cmd.Parameters["@LastName"].IsNullable = true;
        if (LastName != string.Empty && LastName != null)
            cmd.Parameters["@LastName"].Value = LastName;
        else
            cmd.Parameters["@LastName"].Value = DBNull.Value;

        cmd.Parameters.Add("@Patronymic", SqlDbType.VarChar, 200);
        cmd.Parameters["@Patronymic"].IsNullable = true;
        if (Patronymic != string.Empty && Patronymic != null)
            cmd.Parameters["@Patronymic"].Value = Patronymic;
        else
            cmd.Parameters["@Patronymic"].Value = DBNull.Value;

        cmd.Parameters.Add("@TownId", SqlDbType.BigInt);
        cmd.Parameters["@TownId"].IsNullable = true;
        if (TownId.HasValue)
            cmd.Parameters["@TownId"].Value = TownId;
        else
            cmd.Parameters["@TownId"].Value = DBNull.Value;

        cmd.Parameters.Add("@Address", SqlDbType.VarChar, 500);
        cmd.Parameters["@Address"].IsNullable = true;
        if (Address != string.Empty && Address != null)
            cmd.Parameters["@Address"].Value = Address;
        else
            cmd.Parameters["@Address"].Value = DBNull.Value;

        cmd.Parameters.Add("@Sex", SqlDbType.Bit);
        cmd.Parameters["@Sex"].IsNullable = true;
        if (Sex.HasValue)
            cmd.Parameters["@Sex"].Value = Sex;
        else
            cmd.Parameters["@Sex"].Value = DBNull.Value;

        cmd.Parameters.Add("@DateOfBirth", SqlDbType.Date);
        cmd.Parameters["@DateOfBirth"].IsNullable = true;
        if (DateOfBirth.HasValue)
            cmd.Parameters["@DateOfBirth"].Value = DateOfBirth;
        else
            cmd.Parameters["@DateOfBirth"].Value = DBNull.Value;

        cmd.Parameters.Add("@Foto", SqlDbType.VarBinary, -1);
        cmd.Parameters["@Foto"].IsNullable = true;
        if (Foto != null)
            cmd.Parameters["@Foto"].Value = Foto;
        else
            cmd.Parameters["@Foto"].Value = DBNull.Value;

        cmd.Parameters.Add("@FotoMimeType", SqlDbType.VarChar, 100);
        cmd.Parameters["@FotoMimeType"].IsNullable = true;
        if (FotoMimeType != string.Empty && FotoMimeType != null)
            cmd.Parameters["@FotoMimeType"].Value = FotoMimeType;
        else
            cmd.Parameters["@FotoMimeType"].Value = DBNull.Value;

        cmd.Parameters.Add("@WebSite", SqlDbType.VarChar, 200);
        cmd.Parameters["@WebSite"].IsNullable = true;
        if (WebSite != string.Empty && WebSite != null)
            cmd.Parameters["@WebSite"].Value = WebSite;
        else
            cmd.Parameters["@WebSite"].Value = DBNull.Value;

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            //throw new ApplicationException("Data error");
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }
    public FotoDetails GetUserFoto(int UserInfoCommonId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetUserFoto", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].IsNullable = false;
        cmd.Parameters["@UserInfoCommonId"].Value = UserInfoCommonId;

        try
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            FotoDetails fotoData = new FotoDetails();
            while (reader.Read())
            {
                if (reader["Foto"] != DBNull.Value)
                    fotoData.Foto = (byte[])reader["Foto"];
                else
                    fotoData.Foto = null;
                if (reader["FotoMimeType"] != DBNull.Value)
                    fotoData.FotoMimeType = (string)reader["FotoMimeType"];
                else
                    fotoData.FotoMimeType = null;                
            }

            return fotoData;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }
}