﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for AddressDB
/// </summary>
public class AddressDB
{
    private string ConnectionString { get; set; }
	public AddressDB()
	{
        this.ConnectionString = WebConfigurationManager.ConnectionStrings["TraceTrackMsSqlServer"].ConnectionString;
	}
    public AddressDB(string connectionString)
    {
        this.ConnectionString = connectionString;
    }
    public DataSet GetAllCountries()
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetAllCountries", con);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "tracetrack_Country");
        return ds;
    }
    public DataSet GetRegionsOfCountry(int CountryId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetRegionsOfCountry", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@CountryId", SqlDbType.Int);
        cmd.Parameters["@CountryId"].Value = CountryId;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "tracetrack_Region");
        return ds;
    }
    public DataSet GetTownsOfRegion(int RegionId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetTownsOfRegion", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@RegionId", SqlDbType.Int);
        cmd.Parameters["@RegionId"].Value = RegionId;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "tracetrack_Town");
        return ds;
    }
    //public DataSet GetCountryRegionTownInfo(int TownId)
    //{
    //    SqlConnection con = new SqlConnection(this.ConnectionString);
    //    SqlCommand cmd = new SqlCommand("GetTownsOfRegion", con);
    //    cmd.CommandType = CommandType.StoredProcedure;
    //    cmd.Parameters.Add("@RegionId", SqlDbType.Int);
    //    cmd.Parameters["@RegionId"].Value = RegionId;
    //    SqlDataAdapter da = new SqlDataAdapter();
    //    DataSet ds = new DataSet();
    //    da.Fill(ds, "tracetrack_Town");
    //    return ds;
    //}
}