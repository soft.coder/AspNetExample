﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DestinationStationDB
/// </summary>
public class DestinationStationDB
{
    private string ConnectionString { get; set; }
	public DestinationStationDB()
	{
        this.ConnectionString = WebConfigurationManager.ConnectionStrings["TraceTrackMsSqlServer"].ConnectionString;
	}
    public List<DestinationStationDetails> GetDestinationStationsOfSupervisor(Guid supervisorUserId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand(StoredProcedureName.GetDestinationStationsOfSupervisor, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
        cmd.Parameters["@UserId"].IsNullable = false;
        cmd.Parameters["@UserId"].Value = supervisorUserId;


        try
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<DestinationStationDetails> lstDestinationStations = new List<DestinationStationDetails>();
            while (reader.Read())
            {
                DestinationStationDetails ds = new DestinationStationDetails(
                    (long)reader[DestinationStationFN.DestinationStationId],
                    (reader[DestinationStationFN.UserInfoCommonId] != DBNull.Value) ?
                        (int?)reader[DestinationStationFN.UserInfoCommonId] : null,
                    (float)reader[DestinationStationFN.Longitude],
                    (float)reader[DestinationStationFN.Latitude],
                    (string)reader[DestinationStationFN.Title],
                    (string)reader[DestinationStationFN.TaskDescription],
                    (DateTime)reader[DestinationStationFN.TimeAssign],
                    (reader[DestinationStationFN.TimeComplete] != DBNull.Value) ? 
                        (DateTime?)reader[DestinationStationFN.TimeComplete] : null
                );
                lstDestinationStations.Add(ds);
            }
            reader.Close();
            return lstDestinationStations;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
    }
    public List<DestinationStationDetails> GetDestinationStationsOfMobileEmployee(Guid mobileEmployeeUserId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand(StoredProcedureName.GetDestinationStationsOfMobileEmployee, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
        cmd.Parameters["@UserId"].IsNullable = false;
        cmd.Parameters["@UserId"].Value = mobileEmployeeUserId;


        try
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<DestinationStationDetails> lstDestinationStations = new List<DestinationStationDetails>();
            while (reader.Read())
            {
                DestinationStationDetails ds = new DestinationStationDetails(
                    (long)reader[DestinationStationFN.DestinationStationId],
                    (reader[DestinationStationFN.UserInfoCommonId] != DBNull.Value) ?
                        (int?)reader[DestinationStationFN.UserInfoCommonId] : null,
                    (float)reader[DestinationStationFN.Longitude],
                    (float)reader[DestinationStationFN.Latitude],
                    (string)reader[DestinationStationFN.Title],
                    (string)reader[DestinationStationFN.TaskDescription],
                    (DateTime)reader[DestinationStationFN.TimeAssign],
                    (reader[DestinationStationFN.TimeComplete] != DBNull.Value) ?
                        (DateTime?)reader[DestinationStationFN.TimeComplete] : null
                );
                lstDestinationStations.Add(ds);
            }
            reader.Close();
            return lstDestinationStations;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
    }

    public DestinationStationDetails InsertDestinationStation(DestinationStationDetails destinationStation)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand(StoredProcedureName.InsertDestinationStation, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].IsNullable = false;
        cmd.Parameters["@UserInfoCommonId"].Value = destinationStation.UserInfoCommonId;

        cmd.Parameters.Add("@Longitude", SqlDbType.Real);
        cmd.Parameters["@Longitude"].IsNullable = false;
        cmd.Parameters["@Longitude"].Value = destinationStation.Longitude;

        cmd.Parameters.Add("@Latitude", SqlDbType.Real);
        cmd.Parameters["@Latitude"].IsNullable = false;
        cmd.Parameters["@Latitude"].Value = destinationStation.Latitude;

        cmd.Parameters.Add("@Title", SqlDbType.VarChar, 200);
        cmd.Parameters["@Title"].IsNullable = false;
        cmd.Parameters["@Title"].Value = destinationStation.Title;
        
        cmd.Parameters.Add("@TaskDescription", SqlDbType.VarChar, 1000);
        cmd.Parameters["@TaskDescription"].IsNullable = false;
        cmd.Parameters["@TaskDescription"].Value = destinationStation.TaskDescription;

        cmd.Parameters.Add("@DestinationStationId", SqlDbType.BigInt);
        cmd.Parameters["@DestinationStationId"].Direction = ParameterDirection.Output;

        cmd.Parameters.Add("@TimeAssign", SqlDbType.DateTime);
        cmd.Parameters["@TimeAssign"].Direction = ParameterDirection.Output;

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            destinationStation.ID = (long)cmd.Parameters["@DestinationStationId"].Value;
            destinationStation.TimeAssign = (DateTime)cmd.Parameters["@TimeAssign"].Value;
            return destinationStation;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
    }
    public void RemoveDestinationStation(long id)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand(StoredProcedureName.RemoveDestinationStation, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@DestinationStationId", SqlDbType.BigInt);
        cmd.Parameters["@DestinationStationId"].IsNullable = false;
        cmd.Parameters["@DestinationStationId"].Value = id;
        
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            throw ex;
        }
    }
    public void CompleteDestinationStation(long id)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand(StoredProcedureName.CompleteDestinationStation, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@DestinationStationId", SqlDbType.BigInt);
        cmd.Parameters["@DestinationStationId"].IsNullable = false;
        cmd.Parameters["@DestinationStationId"].Value = id;
        DateTime date = new DateTime(13241);
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            throw ex;
        }
    }
}