﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for LocationDB
/// </summary>
public class LocationDB
{
    private string ConnectionString { get; set; }

	public LocationDB()
	{
        this.ConnectionString = WebConfigurationManager.ConnectionStrings["TraceTrackMsSqlServer"].ConnectionString;
	}

    public DataSet GetLocationsOfTrackDataSet(int TrackId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetLocationsOfTrack", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@TrackId", SqlDbType.BigInt);
        cmd.Parameters["@TrackId"].Value = TrackId;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "Location");
        return ds;
    }

    public List<LocationDetails> GetLocationsOfTrackDetails(long TrackId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetLocationsOfTrack", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@TrackId", SqlDbType.BigInt);
        cmd.Parameters["@TrackId"].Value = TrackId;
        try
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<LocationDetails> locations = new List<LocationDetails>();
            while (reader.Read())
            {
                LocationDetails locNew = new LocationDetails();
                locNew.LocationId = (long)reader["LocationId"];
                locNew.Time = (DateTime)reader["Time"];
                locNew.Longitude = (float)reader["Longitude"];
                locNew.Latitude = (float)reader["Latitude"];
                
                if (reader["Accuracy"] != DBNull.Value)
                    locNew.Accuracy = (float)reader["Accuracy"];
                else
                    locNew.Accuracy = null;
                
                if (reader["Provider"] != DBNull.Value)
                    locNew.Provider = (string)reader["Provider"];
                else
                    locNew.Provider = "";
                
                if (reader["Bearing"] != DBNull.Value)
                    locNew.Bearing = (float)reader["Bearing"];
                else
                    locNew.Bearing = null;
                
                if (reader["Speed"] != DBNull.Value)
                    locNew.Speed = (float)reader["Speed"];
                else
                    locNew.Speed = null;
                
                if (reader["Distance"] != DBNull.Value)
                    locNew.Distance = (float)reader["Distance"];
                else
                    locNew.Distance = null;

                locations.Add(locNew);
            }
            reader.Close();
            return locations;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public List<LocationDetails> GetLocationsOfTrackAfterLastLocation(long TrackId, long LocationId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetLocationsOfTrackAfterLastLocation", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@TrackId", SqlDbType.BigInt);
        cmd.Parameters["@TrackId"].Value = TrackId;

        cmd.Parameters.Add("@LocationId", SqlDbType.BigInt);
        cmd.Parameters["@LocationId"].Value = LocationId;

        try
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<LocationDetails> locations = new List<LocationDetails>();
            while (reader.Read())
            {
                LocationDetails locNew = new LocationDetails();
                locNew.LocationId = (long)reader["LocationId"];
                locNew.Time = (DateTime)reader["Time"];
                locNew.Longitude = (float)reader["Longitude"];
                locNew.Latitude = (float)reader["Latitude"];

                if (reader["Accuracy"] != DBNull.Value)
                    locNew.Accuracy = (float)reader["Accuracy"];
                else
                    locNew.Accuracy = null;

                if (reader["Provider"] != DBNull.Value)
                    locNew.Provider = (string)reader["Provider"];
                else
                    locNew.Provider = null;

                if (reader["Bearing"] != DBNull.Value)
                    locNew.Bearing = (float)reader["Bearing"];
                else
                    locNew.Bearing = null;

                if (reader["Speed"] != DBNull.Value)
                    locNew.Speed = (float)reader["Speed"];
                else
                    locNew.Speed = null;

                if (reader["Distance"] != DBNull.Value)
                    locNew.Distance = (float)reader["Distance"];
                else
                    locNew.Distance = null;

                locations.Add(locNew);
            }
            reader.Close();
            return locations;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public long? InsertLocation(long TrackId, LocationDetails location)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("InsertLocation", con);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@TrackId", SqlDbType.BigInt);
        cmd.Parameters["@TrackId"].IsNullable = false;
        cmd.Parameters["@TrackId"].Value = TrackId;

        cmd.Parameters.Add("@Time", SqlDbType.DateTime);
        cmd.Parameters["@Time"].IsNullable = false;
        cmd.Parameters["@Time"].Value = location.Time;

        cmd.Parameters.Add("@Longitude", SqlDbType.Real);
        cmd.Parameters["@Longitude"].IsNullable = true;
        cmd.Parameters["@Longitude"].Value = location.Longitude;

        cmd.Parameters.Add("@Latitude", SqlDbType.Real);
        cmd.Parameters["@Latitude"].IsNullable = false;
        cmd.Parameters["@Latitude"].Value = location.Latitude;

        cmd.Parameters.Add("@Accuracy", SqlDbType.Float);
        cmd.Parameters["@Accuracy"].IsNullable = true;
        if (location.Accuracy.HasValue)
            cmd.Parameters["@Accuracy"].Value = location.Accuracy.Value;
        else
            cmd.Parameters["@Accuracy"].Value = DBNull.Value;

        cmd.Parameters.Add("@Provider", SqlDbType.VarChar, 500);
        cmd.Parameters["@Provider"].IsNullable = true;
        if (location.Provider != string.Empty)
            cmd.Parameters["@Provider"].Value = location.Provider;
        else
            cmd.Parameters["@Provider"].Value = DBNull.Value;

        cmd.Parameters.Add("@Bearing", SqlDbType.Float);
        cmd.Parameters["@Bearing"].IsNullable = true;
        if (location.Bearing.HasValue)
            cmd.Parameters["@Bearing"].Value = location.Bearing.Value;
        else
            cmd.Parameters["@Bearing"].Value = DBNull.Value;

        cmd.Parameters.Add("@Speed", SqlDbType.Float);
        cmd.Parameters["@Speed"].IsNullable = true;
        if (location.Speed.HasValue)
            cmd.Parameters["@Speed"].Value = location.Speed.Value;
        else
            cmd.Parameters["@Speed"].Value = DBNull.Value;

        cmd.Parameters.Add("@Distance", SqlDbType.Float);
        cmd.Parameters["@Distance"].IsNullable = true;
        if (location.Distance.HasValue)
            cmd.Parameters["@Distance"].Value = location.Distance.Value;
        else
            cmd.Parameters["@Distance"].Value = DBNull.Value;



        cmd.Parameters.Add("@LocationId", SqlDbType.BigInt);
        cmd.Parameters["@LocationId"].Direction = ParameterDirection.Output;

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@LocationId"].Value != DBNull.Value)
                return (long)cmd.Parameters["@LocationId"].Value;
            else
                return null;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }

}