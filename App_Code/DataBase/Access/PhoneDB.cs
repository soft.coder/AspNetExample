﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;



/// <summary>
/// Summary description for PhoneDB
/// </summary>
public class PhoneDB
{
    private string ConnectionString { get; set; }
	public PhoneDB()
	{
        this.ConnectionString = WebConfigurationManager.ConnectionStrings["TraceTrackMsSqlServer"].ConnectionString;
	}
    public DataSet GetPhonesOfUser(int UserInfoCommonId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetPhonesOfUser", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].Value = UserInfoCommonId;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "UserPhone");
        return ds;
    }
    public DataSet GetPhonesOfCompany(int CompanyId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetPhonesOfCompany", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@CompanyId", SqlDbType.Int);
        cmd.Parameters["@CompanyId"].Value = CompanyId;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "CompanyPhone");
        return ds;
    }
    public DataSet GetTypesOfPhone()
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetTypesOfPhone", con);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "TypePhone");
        return ds;
    }
    public void InsertTypeOfPhone(string TypePhoneName)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("InserTypeOfPhone", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@TypePhoneName", SqlDbType.VarChar, 200);
        cmd.Parameters["@TypePhoneName"].IsNullable = false;
        if (TypePhoneName != string.Empty)
            cmd.Parameters["@TypePhoneName"].Value = TypePhoneName;
        else
            throw new ArgumentException("typephonename is empty string");
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            throw new ApplicationException("Data error");
        }
        finally
        {
            con.Close();
        }
    }
    public long? InsertPhoneOfUser(int UserInfoCommonId, string PhoneDescription, string PhoneNumber, int? TypePhoneId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("InsertPhoneOfUser", con);
        cmd.CommandType = CommandType.StoredProcedure;
        
        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].IsNullable = false;
        cmd.Parameters["@UserInfoCommonId"].Value = UserInfoCommonId;
        
        cmd.Parameters.Add("@PhoneDescription", SqlDbType.VarChar, 300);
        cmd.Parameters["@PhoneDescription"].IsNullable = true;
        if (PhoneDescription != string.Empty && PhoneDescription != null)
            cmd.Parameters["@PhoneDescription"].Value = PhoneDescription;
        else
            cmd.Parameters["@PhoneDescription"].Value = DBNull.Value;

        
        cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar, 100);
        cmd.Parameters["@PhoneNumber"].IsNullable = false;
        if (PhoneNumber != string.Empty && PhoneNumber != null)
            cmd.Parameters["@PhoneNumber"].Value = PhoneNumber;
        else
            throw new ArgumentException("PhoneNumber empty string");
        
        cmd.Parameters.Add("@TypePhoneId", SqlDbType.Int);
        cmd.Parameters["@TypePhoneId"].IsNullable = true;
        if (TypePhoneId.HasValue)
            cmd.Parameters["@TypePhoneId"].Value = TypePhoneId;
        else
            cmd.Parameters["@TypePhoneId"].Value = DBNull.Value;

        cmd.Parameters.Add("@ContactPhoneId", SqlDbType.BigInt);
        cmd.Parameters["@ContactPhoneId"].Direction = ParameterDirection.Output;
        
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@ContactPhoneId"].Value != DBNull.Value)
                return (long)cmd.Parameters["@ContactPhoneId"].Value;
            else
                return null;
        }
        catch (SqlException ex)
        {
            throw new ApplicationException("Data error");
        }
        finally
        {
            con.Close();
        }
    }
    public long? InsertPhoneOfCompany(int CompanyId, string PhoneDescription, string PhoneNumber, int? TypePhoneId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("InsertPhoneOfCompany", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@CompanyId", SqlDbType.Int);
        cmd.Parameters["@CompanyId"].IsNullable = false;
        cmd.Parameters["@CompanyId"].Value = CompanyId;

        cmd.Parameters.Add("@PhoneDescription", SqlDbType.VarChar, 300);
        cmd.Parameters["@PhoneDescription"].IsNullable = true;
        if (PhoneDescription != string.Empty)
            cmd.Parameters["@PhoneDescription"].Value = PhoneDescription;
        else
            cmd.Parameters["@PhoneDescription"].Value = DBNull.Value;


        cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar, 100);
        cmd.Parameters["@PhoneNumber"].IsNullable = false;
        if (PhoneNumber != string.Empty)
            cmd.Parameters["@PhoneNumber"].Value = PhoneNumber;
        else
            throw new ArgumentException("PhoneNumber empty string");

        cmd.Parameters.Add("@TypePhoneId", SqlDbType.Int);
        cmd.Parameters["@TypePhoneId"].IsNullable = true;
        if (TypePhoneId.HasValue)
            cmd.Parameters["@TypePhoneId"].Value = TypePhoneId;
        else
            cmd.Parameters["@TypePhoneId"].Value = DBNull.Value;

        cmd.Parameters.Add("@ContactPhoneId", SqlDbType.BigInt);
        cmd.Parameters["@ContactPhoneId"].Direction = ParameterDirection.Output;
        
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@ContactPhoneId"].Value != DBNull.Value)
                return (long)cmd.Parameters["@ContactPhoneId"].Value;
            else
                return null;
        }
        catch (SqlException ex)
        {
            throw new ApplicationException("Data error");
        }
        finally
        {
            con.Close();
        }
    }
    public void DeletePhoneOfUser(long ContactPhoneId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("DeletePhoneOfUser", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@ContactPhoneId", SqlDbType.BigInt);
        cmd.Parameters["@ContactPhoneId"].IsNullable = false;
        cmd.Parameters["@ContactPhoneId"].Value = ContactPhoneId;

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            throw new ApplicationException("Data error");
        }
        finally
        {
            con.Close();
        }
    }


}