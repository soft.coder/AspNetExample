﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;


/// <summary>
/// Summary description for TrackDB
/// </summary>
public class TrackDB
{
    private string ConnectionString { get; set; }

	public TrackDB()
	{
        this.ConnectionString = WebConfigurationManager.ConnectionStrings["TraceTrackMsSqlServer"].ConnectionString;
	}



    public List<TrackDetails> GetTracksOfUser(int UserInfoCommonId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetTracksOfUser", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].Value = UserInfoCommonId;
        try
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<TrackDetails> tracksId = new List<TrackDetails>();
            while (reader.Read())
            {
                tracksId.Add(new TrackDetails((long)reader["TrackId"]));
            }
            reader.Close();
            return tracksId;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public TrackDetails GetLastTrackOfUserByUserId(Guid UserId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand(StoredProcedureName.GetLastTrackOfUserByUserId, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserId", SqlDbType.Int);
        cmd.Parameters["@UserId"].Value = UserId;
        try
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            TrackDetails track = null;
            while (reader.Read())
            {
                track = new TrackDetails((long)reader["TrackId"]);
            }
            reader.Close();
            return track;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public List<TrackDetails> GetTracksOfUserAfterLastTrack(int UserInfoCommonId, long TrackId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetTracksOfUserAfterLastTrack", con);
        cmd.CommandType = CommandType.StoredProcedure;
        
        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].IsNullable = false;
        cmd.Parameters["@UserInfoCommonId"].Value = UserInfoCommonId;

        cmd.Parameters.Add("@TrackId", SqlDbType.BigInt);
        cmd.Parameters["@TrackId"].IsNullable = false;
        cmd.Parameters["@TrackId"].Value = TrackId;

        try
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<TrackDetails> tracksId = new List<TrackDetails>();
            while (reader.Read())
            {
                tracksId.Add(new TrackDetails((long)reader["TrackId"]));
            }
            reader.Close();
            return tracksId;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public int? InsertTrack(int UserInfoCommonId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("InsertTrack", con);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].IsNullable = false;
        cmd.Parameters["@UserInfoCommonId"].Value = UserInfoCommonId;

        cmd.Parameters.Add("@TrackId", SqlDbType.Int);
        cmd.Parameters["@TrackId"].Direction = ParameterDirection.Output;

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@TrackId"].Value != DBNull.Value)
                return (int)cmd.Parameters["@TrackId"].Value;
            else
                return null;
        }
        catch (SqlException ex)
        {
            throw ex;
        }
        finally
        {
            con.Close();
        }
    }
}