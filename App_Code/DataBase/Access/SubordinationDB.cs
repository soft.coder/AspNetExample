﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for SubordinationDB
/// </summary>
public class SubordinationDB
{
    private string ConnectionString { get; set; }

	public SubordinationDB()
	{
        this.ConnectionString = WebConfigurationManager.ConnectionStrings["TraceTrackMsSqlServer"].ConnectionString;
	}
    public DataSet GetSubordinationsUserInfo(Object objUserId)
    {
        Guid UserId = (Guid)objUserId;
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetSubordinationsUserInfo", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
        cmd.Parameters["@UserId"].Value = UserId;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "Subordination");
        return ds;
    }
    public List<UserInfoDetails> GetSubordinationsUsersInfoDetailByUserId(Guid UserId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetSubordinationsUserInfoDetailsByUserID", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
        cmd.Parameters["@UserId"].Value = UserId;

        try
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<UserInfoDetails> lstUserInfoDet = new List<UserInfoDetails>();
            while (reader.Read())
            {
                UserInfoDetails userInfoDet = new UserInfoDetails();
                
                userInfoDet.ID = (int)reader["UserInfoCommonId"];

                if (reader["UserName"] != DBNull.Value)
                    userInfoDet.UserName = (string)reader["UserName"];
                else
                    userInfoDet.UserName = null;

                if ( reader["LastName"] != DBNull.Value ) 
                    userInfoDet.LastName = (string)reader["LastName"];
                else
                    userInfoDet.LastName = null;
                
                if (reader["FirstName"] != DBNull.Value)
                    userInfoDet.FirstName = (string)reader["FirstName"];
                else
                    userInfoDet.FirstName = null;

                if (reader["Patronymic"] != DBNull.Value)
                    userInfoDet.Patronymic = (string)reader["Patronymic"];
                else
                    userInfoDet.Patronymic = null;

                if (reader["Address"] != DBNull.Value)
                    userInfoDet.Street = (string)reader["Address"];
                else
                    userInfoDet.Street = null;
                if ( reader["Sex"] != DBNull.Value ) 
                    userInfoDet.Sex = (bool)reader["Sex"];
                else
                    userInfoDet.Sex = null;
                if (reader["DateOfBirth"] != DBNull.Value)
                    userInfoDet.DateOfBirth = (DateTime)reader["DateOfBirth"];
                else
                    userInfoDet.DateOfBirth = null;

                if (reader["WebSite"] != DBNull.Value)
                    userInfoDet.WebSite = (string)reader["WebSite"];
                else
                    userInfoDet.WebSite = null;

                lstUserInfoDet.Add(userInfoDet);
            }
            reader.Close();
            return lstUserInfoDet;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public DataSet GetSuperiorUserInfo(int UserInfoCommonId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("GetSuperiorUserInfo", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@UserInfoCommonId", SqlDbType.Int);
        cmd.Parameters["@UserInfoCommonId"].Value = UserInfoCommonId;
        SqlDataAdapter da = new SqlDataAdapter();
        da.SelectCommand = cmd;
        DataSet ds = new DataSet();
        da.Fill(ds, "Subordination");
        return ds;
    }
    public long InsertSubordination(int SuperiorUserInfoId, int SubordinateUserInfoId)
    {
        SqlConnection con = new SqlConnection(this.ConnectionString);
        SqlCommand cmd = new SqlCommand("InsertSubordination", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@SubordinateUserInfoId", SqlDbType.Int);
        cmd.Parameters["@SubordinateUserInfoId"].Value = SubordinateUserInfoId;
        
        cmd.Parameters.Add("@SuperiorUserInfoId", SqlDbType.Int);
        cmd.Parameters["@SuperiorUserInfoId"].Value = SuperiorUserInfoId;
        
        cmd.Parameters.Add("@SubordinationId", SqlDbType.BigInt);
        cmd.Parameters["@SubordinationId"].Direction = ParameterDirection.Output;
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            if (cmd.Parameters["@SubordinationId"].Value != DBNull.Value)
                return (long)cmd.Parameters["@SubordinationId"].Value;
            else
                return (long)-1;
        }
        catch (SqlException ex)
        {
            throw new ApplicationException("Data error");
        }
        finally
        {
            con.Close();
        }
    }
    public int UpdateUserInfo(int UserInfoCommonId, int? CompanyId, string FirstName, string LastName, string Patronymic, long? TownId,
                            string Address, bool? Sex, DateTime? DateOfBirth, byte[] Foto, string FotoMimeType, string WebSite)
    {
        UserInfoCommonDB udb = new UserInfoCommonDB();
        udb.UpdateUserInfo(UserInfoCommonId, CompanyId, FirstName, LastName, Patronymic, TownId, Address, Sex, DateOfBirth, Foto, FotoMimeType, WebSite);
        return UserInfoCommonId;
    }
}