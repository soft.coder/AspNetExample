﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DestinationStationDetails
/// </summary>
public class DestinationStationDetails
{
    public long? ID { get; set; }
    public int? UserInfoCommonId { get; set; }
    public double Longitude { get; private set; }
    public double Latitude { get; private set; }
    public string Title { get;  set; }
    public string TaskDescription { get; set; }
    public DateTime? TimeAssign { get; set; }
    private DateTime? fTimeComplete;
    public DateTime? TimeComplete 
    {
        get { return this.fTimeComplete; }
        set
        {
            if (value.HasValue == true)
            {
                if(value.Value.CompareTo(this.TimeAssign) >= 0) 
                    this.fTimeComplete = value;
                else
                    throw new ArgumentOutOfRangeException("TimeComplete < TimeAssign" );
            }
            else 
                this.fTimeComplete = null;
        }
    }    

	public DestinationStationDetails()
	{ }
    public DestinationStationDetails(
        long? id,
        int? userInfoCommonId, 
        double longitude, 
        double latitude,
        string title, 
        string description,
        DateTime? timeAssign,
        DateTime? timeComplete)
    {
        this.ID = id;
        this.UserInfoCommonId = userInfoCommonId;
        this.Longitude = longitude;
        this.Latitude = latitude;
        this.Title = title;
        this.TaskDescription = description;
        this.TimeAssign = timeAssign;
        this.TimeComplete = timeComplete;
    }
}