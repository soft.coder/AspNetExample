﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MobileEmpName
/// </summary>
public class MobileEmpName
{
    public int UserInfoCommonId { get; set; }
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string Patronymic { get; set; }

    public MobileEmpName()
    { }

    public MobileEmpName(int userInfoCommonId, string lastName, string firstName, string patronymic)
	{
        this.UserInfoCommonId = userInfoCommonId;
        this.LastName = lastName;
        this.FirstName = firstName;
        this.Patronymic = patronymic;
	}
}