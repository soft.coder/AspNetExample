﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserInfoDetails
/// </summary>
public class UserInfoDetails
{
    public string UserName { get; set; } 
    public int ID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Patronymic { get; set; }
    public bool? Sex { get; set; }
    public DateTime? DateOfBirth { get; set; }
    public string WebSite { get; set; }
    public string FotoUrl { get; set; }
    public string Street { get; set; }

    public TownDetails TownInfo { get; set; }

    public List<TrackDetails> Tracks { get; set; }

	public UserInfoDetails()
	{
        this.Tracks = new List<TrackDetails>();
	}
}