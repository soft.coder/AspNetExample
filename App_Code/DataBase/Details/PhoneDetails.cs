﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PhoneDetails
/// </summary>
[Serializable]
public class PhoneDetails
{
    public string PhoneDescription { get; set; }
    public string PhoneNumber { get; set; }    
    public int? TypePhoneId { get; set; }
    public string TypePhoneName { get; set; }

    public PhoneDetails(string PhoneDescription, string PhoneNumber, int? TypePhoneId = null, string TypePhoneName = "")
	{
        this.PhoneDescription = PhoneDescription;
        this.PhoneNumber = PhoneNumber;
        this.TypePhoneId = TypePhoneId;
        this.TypePhoneName = TypePhoneName;
	}

}