﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Drawing;
using System.Drawing.Imaging;

/// <summary>
/// Summary description for FotoDetails
/// </summary>
public class FotoDetails
{
    public string FotoMimeType { get; set; }
    public byte[] Foto { get; set; }    
}