﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TownDetails
/// </summary>
public class TownDetails
{
    public int CountryId { get; set; }
    public string CountryName { get; set; }
    public int RegionId { get; set; }
    public string RegionName { get; set; }
    public int TownId { get; set; }
    public string TownName { get; set; }



	public TownDetails()
	{

	}
}