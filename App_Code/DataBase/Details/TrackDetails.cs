﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TrackDetails
/// </summary>
public class TrackDetails
{
    public long TrackId { get; set; }
    [Obsolete]
    public string UserName { get; set; }
    public List<LocationDetails> Locations { get; set; }

    private TrackDetails()
    {
        this.Locations = new List<LocationDetails>();
    }
    public TrackDetails(long TrackId)
        : this()
    {
        this.TrackId = TrackId;
    }

    [Obsolete]
	public TrackDetails(string UserName) 
	{
        Locations = new List<LocationDetails>();
        this.UserName = UserName;
	}
    [Obsolete]
    public TrackDetails(long TrackId, string UserName)
    {
        Locations = new List<LocationDetails>();
        this.TrackId = TrackId;
        this.UserName = UserName;
    }

}