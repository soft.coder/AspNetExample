﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LocationDetails
/// </summary>
public class LocationDetails
{
    #region publicProp

    public long LocationId { get; set; } 
    public DateTime Time { get; set; }
    public double Longitude { get; set; }
    public double Latitude { get; set; }
    public float? Accuracy { get; set; }
    public string Provider { get; set; }
    public float? Bearing { get; set; }
    public float? Speed { get; set; }
    public float? Distance { get; set; }

    #endregion publicProp

    public LocationDetails()
	{

	}

}