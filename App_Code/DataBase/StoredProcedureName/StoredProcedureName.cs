﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StoredProcedureName
/// </summary>
public static class StoredProcedureName
{
    public const string GetDestinationStationsOfSupervisor = "GetDestinationStationsOfSupervisor";
    public const string GetDestinationStationsOfMobileEmployee = "GetDestinationStationsOfMobileEmployee";
    public const string InsertDestinationStation = "InsertDestinationStation";
    public const string RemoveDestinationStation = "RemoveDestinationStation";
    public const string CompleteDestinationStation = "CompleteDestinationStation";
    public const string GetLastTrackOfUserByUserId = "GetLastTrackOfUserByUserId";
}