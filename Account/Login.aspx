﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Account_Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="loginWrap">
        <h2>
            Вход в систему
        </h2>
        <p style="text-align:center">
            Пожалуйства введите имя пользователя и пароль.            
        </p>
        <asp:Login ID="LoginUser" runat="server" EnableViewState="false" RenderOuterTable="false" 
                    OnLoggedIn="LoginUser_LoggedIn" RememberMeSet="true" >
            <LayoutTemplate>
                <span class="failureNotification">
                    <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" 
                     ValidationGroup="LoginUserValidationGroup"/>
                <div class="accountInfo">
                    <fieldset class="login">
                        <legend>Информация о пользователе</legend>
                        <p>
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Имя пользователя:</asp:Label>
                            <asp:TextBox ID="UserName" runat="server" CssClass="textEntry" TabIndex="1"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                                 CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="Для входа в систему небходимо ввести имя пользователя." 
                                 ValidationGroup="LoginUserValidationGroup" Font-Names="sans-serif">&#9668;</asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Пароль:</asp:Label>
                            <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" TextMode="Password" TabIndex="2"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                                 CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Для входа в систему небходимо ввести пароль." 
                                 ValidationGroup="LoginUserValidationGroup" Font-Names="sans-serif">&#9668;</asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:CheckBox ID="RememberMe" runat="server" TabIndex="3"/>
                            <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Запомнить меня</asp:Label>
                        </p>
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Войти" ValidationGroup="LoginUserValidationGroup" TabIndex="4"/>
                    </p>
                </div>
            </LayoutTemplate>
        </asp:Login>

    </div>
</asp:Content>