﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Account/Admin/Admin.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="Managers.aspx.cs" Inherits="Account_Manager_Supervisors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="listUsers">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
    DataKeyNames="UserInfoCommonId" DataSourceID="SqlDataSource1" 
        AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" 
        GridLines="None" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" 
        OnRowDataBound="GridView1_RowDataBound" Width="900px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="UserInfoCommonId" HeaderText="ID" 
                ReadOnly="True" SortExpression="UserInfoCommonId" />
            <asp:BoundField DataField="UserName" HeaderText="Логин" ReadOnly="True" 
                SortExpression="UserName" />
            <asp:BoundField DataField="FirstName" HeaderText="Имя" 
                SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="Фамилия" 
                SortExpression="LastName" />
            <asp:BoundField DataField="Patronymic" HeaderText="Отчество" 
                SortExpression="Patronymic" />
            <asp:BoundField DataField="Company" HeaderText="Компания" ReadOnly="True" 
                SortExpression="Company" />
            <asp:BoundField DataField="Town" HeaderText="Город" ReadOnly="True" 
                SortExpression="Town" />
            <asp:BoundField DataField="Address" HeaderText="Адрес" 
                SortExpression="Address" />
            <asp:BoundField DataField="DateOfBirth" HeaderText="Дата рождения" 
                SortExpression="DateOfBirth" DataFormatString="{0:d}" />
            <asp:BoundField DataField="WebSite" HeaderText="Веб-сайт" 
                SortExpression="WebSite" Visible="False" />
            <asp:HyperLinkField DataNavigateUrlFields="WebSite" DataTextField="WebSite" 
                HeaderText="Веб-сайт" />
            <asp:ButtonField ButtonType="Image" ImageUrl="~/Img/unlock.png" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:tracetrackConnectionString %>" 
        SelectCommand="
                SELECT 
	                [UserInfoCommonId], 
		                (SELECT 
			                UserName  
		                FROM aspnet_Users 
		                WHERE aspnet_Users.UserId = ui.UserId) 
	                AS UserName,
	                [FirstName],
	                [LastName],
	                [Patronymic],
	                (SELECT CompanyName FROM tracetrack_Company WHERE ui.CompanyId = tracetrack_Company.CompanyId)
	                AS Company,	
	                (SELECT TownName FROM tracetrack_Town WHERE ui.TownId = tracetrack_Town.TownId)
	                AS Town,
	                [Address],
	                [DateOfBirth],
	                [Sex],
	                [WebSite]
                FROM [tracetrack_UserInfoCommon] AS ui
                WHERE ui.UserId IN 
	                ( SELECT UserId FROM aspnet_UsersInRoles WHERE RoleId = 
		                ( SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'Manager')
	                 );">
    </asp:SqlDataSource>
    <asp:DetailsView ID="DetailsView1" runat="server" Height="50px" Width="300px" 
        AutoGenerateRows="False" CellPadding="4" DataKeyNames="UserInfoCommonId" 
        DataSourceID="SqlDataSource2" ForeColor="#333333" GridLines="None" 
        Visible="False">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
        <EditRowStyle BackColor="#999999" />
        <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
        <Fields>
            <asp:BoundField DataField="UserInfoCommonId" HeaderText="ID" ReadOnly="True" 
                SortExpression="UserInfoCommonId" />
            <asp:BoundField DataField="UserName" HeaderText="Имя пользователя" 
                ReadOnly="True" SortExpression="UserName" />
            <asp:BoundField DataField="FirstName" HeaderText="Имя" 
                SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="Фамилия" 
                SortExpression="LastName" />
            <asp:BoundField DataField="Patronymic" HeaderText="Отчество" 
                SortExpression="Patronymic" />
            <asp:BoundField DataField="Company" HeaderText="Компания" ReadOnly="True" 
                SortExpression="Company" />
            <asp:BoundField DataField="Town" HeaderText="Город" ReadOnly="True" 
                SortExpression="Town" />
            <asp:BoundField DataField="Address" HeaderText="Адрес" 
                SortExpression="Address" />
            <asp:BoundField DataField="DateOfBirth" HeaderText="Дата рождения" 
                SortExpression="DateOfBirth" />
            <asp:BoundField DataField="WebSite" HeaderText="Веб-сайт" 
                SortExpression="WebSite" />
        </Fields>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    </asp:DetailsView>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:tracetrackConnectionString %>" 
        SelectCommand="SELECT 
	[UserInfoCommonId], 
		(SELECT 
			UserName  
		FROM aspnet_Users 
		WHERE aspnet_Users.UserId = ui.UserId) 
	AS UserName,
	[FirstName],
	[LastName],
	[Patronymic],
	(SELECT CompanyName FROM tracetrack_Company WHERE ui.CompanyId = tracetrack_Company.CompanyId)
	AS Company,	
	(SELECT TownName FROM tracetrack_Town WHERE ui.TownId = tracetrack_Town.TownId)
	AS Town,
	[Address],
	[DateOfBirth],
	[Sex],
	[WebSite]
FROM [tracetrack_UserInfoCommon] AS ui
WHERE ui.UserId = ( SELECT UserId FROM tracetrack_UserInfoCommon WHERE UserInfoCommonId = @UserInfoCommand)">
        <SelectParameters>
            <asp:ControlParameter ControlID="GridView1" DefaultValue="1" 
                Name="UserInfoCommand" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
</div>
</asp:Content>

