﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Account/Admin/Admin.master" AutoEventWireup="true"
    CodeFile="RegisterManager.aspx.cs" Inherits="Account_Register" %>
<%@ Register Src="~/usercontrols/registeruser.ascx" TagPrefix="uc" TagName="RegisterUser" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <uc:RegisterUser runat="server" ID="ucRegisterUser" RoleForNewUser="Manager" 
                        GreetingUserCommonInfo="Информация о менеджере"
                        GreetingCompany="Информация о компании"
                        ProfilePage="~/Account/Admin/AdminProfile.aspx"
                        GreetingCreatingUser="Создание менеджера"
                        CreateUserButtonText="Создать менеджера"
                        GreetingCompleteStep="Регистрация менедежера завершена" />

</asp:Content>