﻿/// <reference path="yandex-map.html" />
/*
Android = {};
Android.getLastKnownLocation = function () {
    return "48.03034, 46.349636";
};
Android.getCurrentMapType = function (map) {
    if (map == "yandex")
        return YMaps.MapType.MAP;
}
Android.showCurrentDestinationStation = function () {
    alert("current destination station dialog");
};
Android.getPolylineColorRgbHex = function () {
    return "#DDDDDD"
}
Android.getPolylineColorRgbaHexYMap = function () {
    return "ff00009B";
}
Android.getPolylineOpacityPercent = function () {
    return 70;
}
Android.getPolylineWidthPx = function () {
    return 3;
};
Android.getCurrentDestinationStation = function () {
    return '{"TaskDescription":"Доставить кофе","UserInfoCommonId":9,"TimeAssign":{"minute":33,"month":9,"year":2012,"hour":14,"day":4,"second":28},"ID":4,"Latitude":46.3515357971191,"Longitude":48.028865814209,"TimeComplete":null,"Title":"Магазин ААА"}';
};
*/
// Создание обработчика для события window.onLoad
YMaps.jQuery(function () {
    try {
        //console.log("test console");
        /*
        if (!('console' in this)) console = {};
        'log info warn error dir clear'.replace(/\w+/g, function (f) {
        if (!(f in console)) console[f] = console.log || new Function;
        });
        */
        var windowSize = vSeRGv.getWindowClientWidthHeight();

        YMaps.jQuery("#YMapsID").width(windowSize.ClientWidth).height(windowSize.ClientHeight);
        YMaps.jQuery("body").css("margin", "0px");

        map = new YMaps.Map(YMaps.jQuery("#YMapsID")[0]);

        var lastKnownLocation;
        try {
            var strLoc = Android.getLastKnownLocation();
            if (strLoc != "") {
                var coordinates = strLoc.split(",");
                lastKnownLocation = new YMaps.GeoPoint(parseFloat(coordinates[0]), parseFloat(coordinates[1]));
            }
            else {
                lastKnownLocation = new YMaps.GeoPoint(48.03034, 46.349636);
            }
        }
        catch (ex) {
            lastKnownLocation = new YMaps.GeoPoint(48.03034, 46.349636);
        }

        console.log("test console.log");

        // Установка для карты ее центра и масштаба 
        map.setCenter(lastKnownLocation, 17);

        var mapType;
        try {
            mapType = Android.getCurrentMapType("yandex");
            if (mapType == "")
                throw new Error("getCurrentMapType: Android return empty string");

            switch (mapType) {
                case YMaps.MapType.HYBRID.getName():
                    map.setType(YMaps.MapType.HYBRID);
                    break;
                case YMaps.MapType.MAP.getName():
                    map.setType(YMaps.MapType.MAP);
                    break;
                case YMaps.MapType.SATELLITE.getName():
                    map.setType(YMaps.MapType.SATELLITE);
                    break;
                default:
                    console.log("Init map type - unexpected map type: " + mapType);
                    break;
            }
            console.log("Android mapType setted: " + mapType);
        }
        catch (ex) {
            console.log("Error when setting map type of yandex: " + ex);
            mapType = YMaps.MapType.MAP;
            map.setType(mapType);
            console.log("Defalut mapType setted: " + mapType);
        }

        menuCommand.map = map;

        function MoblieZoomControl(anchor, offsets) {
            var zcMap;
            var zoomControl;
            this.onAddToMap = function (map, controlPosition) {
                zcMap = map;
                var nodeYMap = document.getElementById("YMapsID");
                zoomControl = vSeRGv.zoomControl.Create(this._onZoomInListener, this._onZoomOutListener);
                nodeYMap.appendChild(zoomControl);
            };

            this.onRemoveFromMap = function () { };
            this._onZoomInListener = function () {
                var currZoom = zcMap.getZoom();
                if (currZoom < zcMap.getMaxZoom(zcMap.getBounds())) {
                    zcMap.setZoom(currZoom + 1, { smooth: true });
                }
            }
            this._onZoomOutListener = function () {
                var currZoom = zcMap.getZoom();
                if (currZoom > zcMap.getMinZoom(zcMap.getBounds())) {
                    zcMap.setZoom(currZoom - 1, { smooth: true });
                }
            }
            this._zoomComplete = function (zoomId) { }
        }


        var mobileZoom = new MoblieZoomControl(YMaps.ControlPosition.TOP_LEFT, new YMaps.Point(10, 10));
        map.addControl(mobileZoom);


        // Создание стиля
        var style = new YMaps.Style("default#greenPoint");
        style.lineStyle = new YMaps.LineStyle();
        try {
            style.lineStyle.strokeColor = Android.getPolylineColorRgbaHexYMap();
            style.lineStyle.strokeWidth = Android.getPolylineWidthPx();
        } catch (ex) {
            style.lineStyle.strokeColor = "ff00009B";
            style.lineStyle.strokeWidth = 3;
        }

        YMaps.Styles.add("style#Android", style);

        var canvasDom = YMaps.jQuery("<canvas></canvas>").
								attr("id", "id_canvas").
								attr("width", map.getContainerSize().getX()).
								attr("height", map.getContainerSize().getX()).
								css("z-index", 50).
								css("position", "absolute").
								appendTo(".YMaps-layer-container").
								get(0);

        // Создание ломанной
        polyline = new PolylineCanvas(polylinePoints = [
        new YMaps.GeoPoint(37.600963, 55.575358), new YMaps.GeoPoint(37.508095, 55.596356), new YMaps.GeoPoint(37.435574, 55.66045),
        new YMaps.GeoPoint(37.414553, 55.685285), new YMaps.GeoPoint(37.385385, 55.713877), new YMaps.GeoPoint(37.369431, 55.745985),
        new YMaps.GeoPoint(37.369952, 55.76645), new YMaps.GeoPoint(37.376554, 55.794178), new YMaps.GeoPoint(37.397018, 55.835862),
        new YMaps.GeoPoint(37.392517, 55.849757), new YMaps.GeoPoint(37.400378, 55.864414), new YMaps.GeoPoint(37.446695, 55.882156),
        /*new YMaps.GeoPoint(37.478774, 55.88562), new YMaps.GeoPoint(37.536023, 55.907798), new YMaps.GeoPoint(37.590731, 55.910397),
        new YMaps.GeoPoint(37.641432, 55.897044), new YMaps.GeoPoint(37.704493, 55.89294), new YMaps.GeoPoint(37.835378, 55.827924),
        new YMaps.GeoPoint(37.846571,55.77758),  new YMaps.GeoPoint(37.830249,55.685899), new YMaps.GeoPoint(37.842627,55.655717),
        new YMaps.GeoPoint(37.766432,55.605648), new YMaps.GeoPoint(37.687812,55.574365), new YMaps.GeoPoint(37.600963,55.575358)*/
    ], { style: "style#Android" });

        polyline2 = new PolylineCanvas(polylinePoints = [
        /*new YMaps.GeoPoint(37.600963, 55.575358), new YMaps.GeoPoint(37.508095, 55.596356), new YMaps.GeoPoint(37.435574, 55.66045),
        new YMaps.GeoPoint(37.414553, 55.685285), new YMaps.GeoPoint(37.385385, 55.713877), new YMaps.GeoPoint(37.369431, 55.745985),
        new YMaps.GeoPoint(37.369952, 55.76645), new YMaps.GeoPoint(37.376554, 55.794178), new YMaps.GeoPoint(37.397018, 55.835862),
        new YMaps.GeoPoint(37.392517, 55.849757), new YMaps.GeoPoint(37.400378, 55.864414), new YMaps.GeoPoint(37.446695, 55.882156),
        new YMaps.GeoPoint(37.478774, 55.88562), new YMaps.GeoPoint(37.536023, 55.907798), new YMaps.GeoPoint(37.590731, 55.910397),*/
        new YMaps.GeoPoint(37.641432, 55.897044), new YMaps.GeoPoint(37.704493, 55.89294), new YMaps.GeoPoint(37.835378, 55.827924),
        new YMaps.GeoPoint(37.846571, 55.77758), new YMaps.GeoPoint(37.830249, 55.685899), new YMaps.GeoPoint(37.842627, 55.655717)/*,
        new YMaps.GeoPoint(37.766432,55.605648), new YMaps.GeoPoint(37.687812,55.574365), new YMaps.GeoPoint(37.600963,55.575358)*/
    ], { style: "style#Android" });
        // Добавление ломанной на карту
        polyline3 = new PolylineCanvas([new YMaps.GeoPoint(36.414553, 55.685285), new YMaps.GeoPoint(36.385385, 55.713877)], { style: "style#Android" });
        map.addOverlay(polyline);
        map.addOverlay(polyline2);
        map.addOverlay(polyline3);


        YMaps.jQuery("div#YMapsID div.YMaps-copyrights span").remove();


        var polylineGeoPoints = [];
        var polylineTracRoute;
        function DrawRoute() {
            var strLocations = Android.getRoute(polylineGeoPoints.length);
            if (strLocations != "") {
                var coordinates = strLocations.split(",");
                console.info("DrawRotue - количество новых координат:" + coordinates.length / 2);
                console.info("DrawRotue - количество старых координат:" + polylineGeoPoints.length);
                if (coordinates.length % 2 == 0) {
                    try {
                        map.removeOverlay(polylineTracRoute);

                        for (var i = 0; i < coordinates.length; i += 2) {
                            polylineGeoPoints.push(new YMaps.GeoPoint(parseFloat(coordinates[i]), parseFloat(coordinates[i + 1])));
                        }
                        map.panTo(polylineGeoPoints[polylineGeoPoints.length - 1], { flying: true });
                        polylineTracRoute = new PolylineCanvas(polylineGeoPoints, { style: "style#Android" });
                        map.addOverlay(polylineTracRoute);
                    } catch (ex) {
                        console.error("DrawRotue - произошла непредвиденная ошибка");
                    }
                }
            }
            else {
                console.info("DrawRotue: пока нет новых координат");
            }
        }

        TracksInfo = {};
        TracksInfo.Tracks = [];
        TracksInfo.CurrentDestinationStation = {};
        TracksInfo.CompleteCurrentDestinatioStation = function () {
            if (TracksInfo.CurrentDestinationStation != null) {
                map.removeOverlay(TracksInfo.CurrentDestinationStation);
                TracksInfo.CurrentDestinationStation = null;
            }
        }
        TracksInfo.Tracks[0] = new PolylineCanvas([], { style: "style#Android" });
        function DrawTracks() {
            var countOfTrack = Android.getCountOfTrack();
            var currTrack;
            var currLocations;
            for (var currTrackIndx = TracksInfo.Tracks.length - 1; currTrackIndx < countOfTrack; currTrackIndx++) {
                if (TracksInfo.Tracks[currTrackIndx] != undefined) {
                    currTrack = TracksInfo.Tracks[currTrackIndx];
                    currLocaions = TracksInfo.Tracks[currTrackIndx].getPoints();
                }
                else {
                    currTrack = null;
                    currLocaions = [];
                }
                var strLocations = Android.getTrack(currTrackIndx, currLocaions.length);
                if (strLocations != "") {
                    var coordinates = strLocations.split(",");
                    if (coordinates.length % 2 == 0) {
                        try {
                            map.removeOverlay(currTrack);

                            for (var i = 0; i < coordinates.length; i += 2) {
                                currLocaions.push(new YMaps.GeoPoint(parseFloat(coordinates[i]), parseFloat(coordinates[i + 1])));
                            }
                            currTrack = new PolylineCanvas(currLocaions, { style: "style#Android" });
                            map.addOverlay(currTrack);
                            map.panTo(currLocaions[currLocaions.length - 1], { flying: false });
                            TracksInfo.Tracks[currTrackIndx] = currTrack;
                        } catch (ex) {
                            console.error("DrawTrack - произошла непредвиденная ошибка");
                        }
                    }
                }
            }
        }
        try {

            var intervalMs = Android.getRateRedrawMs();
            TracksInfo.IntervalID = setInterval(DrawTracks, intervalMs);
            window.onunload = function () {
                clearInterval(TracksInfo.IntervalID);
                console.log("onunlod event: Yandex Map - ClearIntraval for function DrawTracks");
            }
            console.log("Intreval exec for function DrawTrack installed, intervalMs:" + intervalMs);
        } catch (ex) {

        }
        try {
            var strJsonCurrDs = Android.getCurrentDestinationStation();
            if (strJsonCurrDs != "") {
                var ds = eval('(' + strJsonCurrDs + ')');
                var placemark = new YMaps.Placemark(new YMaps.GeoPoint(ds.Longitude, ds.Latitude), { hasBalloon: false,
                    style: "default#hospitalIcon"
                });
                YMaps.Events.observe(placemark, placemark.Events.Click, function (obj) {
                    console.log("before click event");
                    Android.showCurrentDestinationStation();
                    console.log("after click event");
                });
                TracksInfo.CurrentDestinationStation = placemark;
                map.addOverlay(placemark);
            }

        } catch (ex) {

        }
    } catch (ex) {
        console.log("unexpected error: " + ex);
    }
});
menuCommand = {};
menuCommand.ZoomIn = function () {
    this.map.setZoom(map.getZoom() + 1, { smooth: true });
}
menuCommand.ZoomOut = function () {
    this.map.setZoom(map.getZoom() - 1, { smooth: true });
}
menuCommand.getLastKnownLocation = function () {
    var lastKnownLocation;
    try {
        var strLoc = Android.getLastKnownLocation();
        if (strLoc != "") {
            var coordinates = strLoc.split(",");
            lastKnownLocation = new YMaps.GeoPoint(parseFloat(coordinates[0]), parseFloat(coordinates[1]));
        }
        else {
            lastKnownLocation = new YMaps.GeoPoint(48.03034, 46.349636);
        }
    }
    catch (ex) {
        lastKnownLocation = new YMaps.GeoPoint(48.03034, 46.349636);
    }
    return lastKnownLocation
}
menuCommand.GoHome = function () {
    this.map.setCenter(menuCommand.getLastKnownLocation(), this.map.getZoom(), this.map.getType());
}
menuCommand.SetMapType = function (mapType) {
    if (this.map.getType().getName() != mapType) {
        try {
            this.map.setType(mapType);
            Android.setCurrentMapType("yandex", this.map.getType().getName());
        }
        catch (ex) {
            console.log(ex);
        }
    }

}