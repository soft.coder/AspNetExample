﻿/// <reference path="yandex-map.html" />


// Реализует наследование прототипа без исполнения конструктора родителя
// Подробнее о наследовании: http://javascript.ru/tutorial/object/inheritance
function extend(child, parent) {
    var c = function () { };
    c.prototype = parent.prototype;
    c.prototype.constructor = parent;
    return child.prototype = new c();
};
// Наследуемся от YMaps.Polyline
function PolylineCanvas(points, options, canvasDOM) {
    /// <summary>Polyline drawing by canvas</summary>
    /// <param name="points" type="array_YMaps.GeoPoint">Points for line</param>
    /// <param name="options" type="Object">Object of options</param>

    // Вызов родительского конструктора
    YMaps.Polyline.call(this, points, options);






    this.CanvasLeave;
    //var polyline = this;

    this.listenerMapMove;
    this.listenerMapMoveEnd;
    this.listenerSmoothZoomStart;

    // Вызывается при добавлении объекта на карту
    this.onAddToMap = function (map, mapContainer) {
        YMaps.Polyline.prototype.onAddToMap.call(this, map);
        this._canvasPolyLine = YMaps.jQuery("<canvas></canvas>").
								attr("width", map.getContainerSize().getX()).
								attr("height", map.getContainerSize().getY()).
								css("z-index", 50).
								css("position", "absolute").
								appendTo(".YMaps-layer-container").
								get(0);
        this.listenerMapMove = YMaps.Events.observe(map, map.Events.Move, function (map) {
            if (!this.IsFullDrawOnMapUpdate) {
                if (this.CanvasNeedRedraw(this.getBounds())) {
                    this._canvasPolyLine.style.left = 0 + "px";
                    this._canvasPolyLine.style.top = 0 + "px";
                    this.updateCanvas();
                    this.CanvasLeave = true;
                }
                else {
                    if (this.CanvasLeave == true) {
                        var rc = this._canvasPolyLine.getContext("2d");
                        rc.clearRect(0, 0, this._canvasPolyLine.width, this._canvasPolyLine.height);
                        this.CanvasLeave = false;
                    }
                }
            }
            else if (this.IsFullDrawOnMapUpdate) {
                this._canvasPolyLine.style.left = map._state.offset.x + "px";
                this._canvasPolyLine.style.top = map._state.offset.y + "px";
            }
        }, this);
        this.listenerMapMoveEnd = YMaps.Events.observe(map, map.Events.MoveEnd, function (map) {
            if (!this.IsFullDrawOnMapUpdate && this.CanvasNeedRedraw(this.getBounds())) {
                this._canvasPolyLine.style.left = 0 + "px";
                this._canvasPolyLine.style.top = 0 + "px";
                this.updateCanvas();
            }
            else if (this.IsFullDrawOnMapUpdate) {
                this._canvasPolyLine.style.left = this._map._state.offset.x + "px";
                this._canvasPolyLine.style.top = this._map._state.offset.y + "px";
            }

        }, this);
        this.listenerSmoothZoomStart = YMaps.Events.observe(map, map.Events.SmoothZoomStart, function (map) {
            var rc = this._canvasPolyLine.getContext("2d");
            rc.clearRect(0, 0, this._canvasPolyLine.width, this._canvasPolyLine.height);
        }, this);
        if (this.IsFullDrawOnMapUpdate = this._IsPolylineBoundSubsetMapBound()) {
            this._canvasPolyLine.style.left = map._state.offset.x + "px";
            this._canvasPolyLine.style.top = map._state.offset.y + "px";
        }
        this.updateCanvas();

        console.log("added polyline");
    }
    // Вызывается при удалении объекта с карты
    this.onRemoveFromMap = function () {
        // Удаление ломанной с карты
        YMaps.Polyline.prototype.onRemoveFromMap.call(this);

        // Удаление обработчика событий                
        this.listenerMapMove.cleanup();
        this.listenerMapMoveEnd.cleanup();
        this.listenerSmoothZoomStart.cleanup();

        var rc = this._canvasPolyLine.getContext('2d');
        rc.clearRect(0, 0, this._canvasPolyLine.width, this._canvasPolyLine.height);
        this._canvasPolyLine.style.left = 0 + "px";
        this._canvasPolyLine.style.top = 0 + "px";
        this._canvasPolyLine.parentNode.removeChild(this._canvasPolyLine);
        console.log("removed polyline");
    }
    // Вызывается при обновлении карты
    this.onMapUpdate = function () {
        // Обновление ломанной на карте
        YMaps.Polyline.prototype.onMapUpdate.call(this);

        this._canvasPolyLine.style.left = this._map._state.offset.x;
        this._canvasPolyLine.style.top = this._map._state.offset.y;

        var boundsPolyline = this.getBounds();
        var boundsMap = this.getMap().getBounds();


        this.IsFullDrawOnMapUpdate = this._IsPolylineBoundSubsetMapBound();


        var needRedraw = boundsMap.intersects(boundsPolyline);
        if (needRedraw) {
            this.updateCanvas();
        }
        else {
            var rc = this._canvasPolyLine.getContext('2d');
            rc.clearRect(0, 0, this._canvasPolyLine.width, this._canvasPolyLine.height);
        }
        //this._drawBound(boundsMap);
        //this._drawBound(boundsPolyline);				
        //console.log("onMapUpdate");
        //console.log("IsFullDrawOnMapUpdate: " + this.IsFullDrawOnMapUpdate);
    }

    this._drawBound = function (bounds) {
        var rc = this._canvasPolyLine.getContext("2d");
        rc.beginPath();
        var current = this.getMap().converter.coordinatesToMapPixels(bounds.getLeftTop());
        rc.moveTo(current.x, current.y);
        current = this.getMap().converter.coordinatesToMapPixels(bounds.getRightTop());
        rc.lineTo(current.x, current.y);
        current = this.getMap().converter.coordinatesToMapPixels(bounds.getRightBottom());
        rc.lineTo(current.x, current.y);
        current = this.getMap().converter.coordinatesToMapPixels(bounds.getLeftBottom());
        rc.lineTo(current.x, current.y);
        current = this.getMap().converter.coordinatesToMapPixels(bounds.getLeftTop());
        rc.lineTo(current.x, current.y);
        rc.stroke();
        rc.closePath();

    }
    this._IsPolylineBoundSubsetMapBound = function () {
        var boundsPolyline = this.getBounds();
        var boundsMap = this.getMap().getBounds();

        if (boundsMap.getLeft() < boundsPolyline.getLeft() &&
				boundsMap.getRight() > boundsPolyline.getRight() &&
				boundsMap.getTop() > boundsPolyline.getTop() &&
				boundsMap.getBottom() < boundsPolyline.getBottom())
            return true;
        else
            return false;

    }
    this._IsPolylineBoundSubsetMapBoundInPixel = function (boundsPolyline) {
        var LeftTop = this.getMap().converter.coordinatesToMapPixels(boundsPolyline.getLeftTop());
        var RightBottom = this.getMap().converter.coordinatesToMapPixels(boundsPolyline.getRightBottom());
        sizeMap = this.getMap().getContainerSize();
        if (LeftTop.x < -1 || LeftTop.y < -1 || RightBottom.x > (sizeMap.x + 1) || RightBottom.y > (sizeMap + 1))
            return false;
        else
            return true;
    }
    this.CanvasNeedRedraw = function (geoBoundsCanvas) {
        return this.getMap().getBounds().intersects(geoBoundsCanvas);
    }
    this.updateCanvas = function () {
        var rc = this._canvasPolyLine.getContext('2d');
        rc.clearRect(0, 0, this._canvasPolyLine.width, this._canvasPolyLine.height);
        if (this._styleColorRGBA != "")
            rc.strokeStyle = this._styleColorRGBA;
        rc.lineWidth = this._styleWidth
        rc.beginPath();
        rc.save();
        if (this.IsFullDrawOnMapUpdate == false)
            rc.translate(this.getMap()._state.offset.x, this.getMap()._state.offset.y);
        for (var i = 0, prev, current, points = this.getPoints(); i < points.length; i++) {
            // Пиксельные кординаты
            current = this.getMap().converter.coordinatesToMapPixels(points[i]);
            if (prev) {
                // Вектор
                rc.lineTo(current.getX(), current.getY());
            }
            else {
                rc.moveTo(current.getX(), current.getY());
            }
            prev = current;
        }
        rc.stroke();
        rc.closePath();
        rc.restore();
    };
    this.getBounds = function () {

        var leftCoord, rightCoord, topCoord, bottomCoord;
        rightCoord = topCoord = Number.NEGATIVE_INFINITY;
        leftCoord = bottomCoord = Number.POSITIVE_INFINITY;

        for (var i = 0, points = this.getPoints(); i < points.length; i++) {
            if (points[i].getX() > rightCoord) {
                rightCoord = points[i].getX();
            }
            if (points[i].getX() < leftCoord) {
                leftCoord = points[i].getX();
            }
            if (points[i].getY() < bottomCoord) {
                bottomCoord = points[i].getY();
            }
            if (points[i].getY() > topCoord) {
                topCoord = points[i].getY();
            }
        }
        var geoBoundCanvas = new YMaps.GeoBounds(new YMaps.GeoPoint(leftCoord, bottomCoord), new YMaps.GeoPoint(rightCoord, topCoord));
        return geoBoundCanvas;
    };

    this._parseStyleColorRGBA = function (style) {
        /// <summary>Parsing string of color in RGBA format</summary>
        /// <param name="style" type="String">String of color in RGBA format</param>
        var currentStyle = YMaps.Styles.get(style);
        var currentLineColor = currentStyle.lineStyle.strokeColor;

        if (currentLineColor.length == 8) {
            var colorRGBA = "rgba(";
            colorRGBA += parseInt(currentLineColor.substring(0, 2), 16) + ",";
            colorRGBA += parseInt(currentLineColor.substring(2, 4), 16) + ",";
            colorRGBA += parseInt(currentLineColor.substring(4, 6), 16) + ",";
            colorRGBA += parseInt(currentLineColor.substring(6, 8), 16) / 255;
            colorRGBA += ")";
            return colorRGBA;
        }
        else {
            console.warn("Style of polyline: strokeColor have unexpected length");
            return "";
        }

    }
    /*Intialize polyline style object*/
    this._initialize = function () {
        this._stylePolyline = options.style;
        this._styleColorRGBA = this._parseStyleColorRGBA(this._stylePolyline);
        this._styleWidth = YMaps.Styles.get(this._stylePolyline).lineStyle.strokeWidth;
    }


    this._initialize();

}



var ptp = extend(PolylineCanvas, YMaps.Polyline);