﻿/// <reference path="osm-openlayers.html" />
/*
Android = {};
Android.getLastKnownLocation = function () {
    return "48.03034, 46.349636";
};
Android.showCurrentDestinationStation = function () {
    alert("current destination station dialog");
};
Android.getPolylineColorRgbHex = function () {
    return "#DDDDDD"
}
Android.getPolylineOpacityPercent = function () {
    return 70;
}
Android.getPolylineWidthPx = function () {
    return 3;
};
Android.getCurrentDestinationStation = function () {
    return '{"TaskDescription":"Доставить кофе","UserInfoCommonId":9,"TimeAssign":{"minute":33,"month":9,"year":2012,"hour":14,"day":4,"second":28},"ID":4,"Latitude":46.3515357971191,"Longitude":48.028865814209,"TimeComplete":null,"Title":"Магазин ААА"}';
};
*/

function init() {
    var options = {
        controls: [

			new OpenLayers.Control.TouchNavigation(/*{
			    dragPanOptions: {
			        interval: 0, // non-zero kills performance on some mobile phones
			        enableKinetic: true
			    }
			}*/),
          ]
    }
    map = new OpenLayers.Map("basicMap", options);
    var mapnik = new OpenLayers.Layer.OSM();
    map.addLayer(mapnik);
    menuCommand.map = map;
    var lastKnownLocation;
    try {
        var strLoc = Android.getLastKnownLocation();
        if (strLoc != "") {
            var coordinates = strLoc.split(",");
            lastKnownLocation = new OpenLayers.LonLat(parseFloat(coordinates[0]), parseFloat(coordinates[1]));
        }
        else {
            lastKnownLocation = new OpenLayers.LonLat(48.03034, 46.349636);
        }
    }
    catch (ex) {
        lastKnownLocation = new OpenLayers.LonLat(48.03034, 46.349636);
    }


    map.setCenter(lastKnownLocation // Center of the map
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
          ), 15 // Zoom level
        );

    TracksInfo = {};
    TracksInfo.Intervals = {};
    TracksInfo.Intervals.DrawAllTracksID = null;
    TracksInfo.Intervals.RateRedraw = null;
    TracksInfo.PointsOfCurrentTrack = []
    TracksInfo.LayerForTrack = new OpenLayers.Layer.Vector("TracksLayer", { renderers: ["Canvas", "SVG", "VML"] });
    map.addLayer(TracksInfo.LayerForTrack);
    TracksInfo.LayerForDestinationStation = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(TracksInfo.LayerForDestinationStation);
    TracksInfo.CurrentDestinationStation = {};
    TracksInfo.CompleteCurrentDestinatioStation = function () {
        if (TracksInfo.CurrentDestinationStation != null) {
            TracksInfo.LayerForDestinationStation.removeMarker(TracksInfo.CurrentDestinationStation);
            TracksInfo.CurrentDestinationStation = null;
        }
    }
    TracksInfo.Tracks = [];
    TracksInfo.Tracks[0] = null;
    try {
        TracksInfo.StyleTrack = style = {
            strokeColor: Android.getPolylineColorRgbHex(),
            strokeOpacity: Android.getPolylineOpacityPercent() / 100,
            strokeWidth: Android.getPolylineWidthPx()
        };
    } catch (ex) {
        //console.log(ex);
    };



    try {
        var strJsonCurrDs = Android.getCurrentDestinationStation();
        if (strJsonCurrDs != "") {
            var ds = eval('(' + strJsonCurrDs + ')');
            TracksInfo.CurrentDestinationStation = createMarkerObj({ Longitude: ds.Longitude, Latitude: ds.Latitude }, {
                id: ds.ID,
                title: ds.Title,
                taskDesription: ds.TaskDescription,
                timeAssign: ds.TimeAssign
            });
        }
                                            
    } catch (ex) {
        
    }

    var DrawAllTracks_initNewTrack = function (indxOfTrack) {
        if (TracksInfo.PointsOfCurrentTrack.length == 2) {
            TracksInfo.Tracks[indxOfTrack] = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.LineString(TracksInfo.PointsOfCurrentTrack), null, style);
            TracksInfo.LayerForTrack.addFeatures([TracksInfo.Tracks[indxOfTrack]]);            
            //console.log("DrawTracks: initialized new polyline");
        }
        else {
            var msgError = "DrawAllTracks_initNewTrack: TracksInfo.PointsOfCurrentTrack.length dont equals 2, ";
            msgError += " NumberOfNewPoints = " + TracksInfo.PointsOfCurrentTrack.length
            console.error(msgError);
            throw new Error(msgError);
        };
    };

    function DrawAllTracks() {
        try {
            var countOfTrack = Android.getCountOfTrack();
            //console.log("countOfTrack: " + countOfTrack);
            var currTrack, currLineString;
            var strLocations, newPoint;


            var numDrawedTracks = TracksInfo.Tracks.length == 0 ? 0 : TracksInfo.Tracks.length - 1;
            //console.log("numDrawedTracks: " + numDrawedTracks);
            for (var currTrackIndx = numDrawedTracks; currTrackIndx < countOfTrack; currTrackIndx++) {
                if (TracksInfo.Tracks[currTrackIndx] == undefined) {
                    currTrack = null;
                    currLineString = null;
                    TracksInfo.PointsOfCurrentTrack = [];
                    //console.log("Start define Undefined Track");
                }
                else {
                    currTrack = TracksInfo.Tracks[currTrackIndx];
                    //console.log("currTrack: " + currTrack);
                    currLineString = TracksInfo.Tracks[currTrackIndx].geometry;
                    //console.log("currLineString: " + currLineString);
                    //console.log("Start define defined Track");
                }
                //console.log("currTrackIndx: " + currTrackIndx);
                if (currLineString != null) {
                    strLocations = Android.getTrack(currTrackIndx, currLineString.getVertices().length);
                    //console.log("currLineString.getVertices().length: " + currLineString.getVertices().length);
                }
                else if (TracksInfo.PointsOfCurrentTrack != null) {
                    strLocations = Android.getTrack(currTrackIndx, TracksInfo.PointsOfCurrentTrack.length);
                    //console.log("TracksInfo.PointsOfCurrentTrack.length: " + TracksInfo.PointsOfCurrentTrack.length);
                }
                else {
                    // Here need Clear Interval
                    console.error("currLineString == null && TrackInfo.PointsOfCurrentTrack == null; DrawTrack aborted")
                }
                if (strLocations != "") {
                    var coordinates = strLocations.split(",");
                    //console.log("coordinates.length: " + coordinates.length);
                    if (coordinates.length % 2 == 0) {
                        try {
                            for (var i = 0; i < coordinates.length; i += 2) {
                                //console.log("i= " + i);
                                var pointLonLat = new OpenLayers.LonLat(parseFloat(coordinates[i]), parseFloat(coordinates[i + 1]));
                                pointLonLat.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                                var pointXY = new OpenLayers.Geometry.Point(pointLonLat.lon, pointLonLat.lat);

                                if (currLineString != null) {
                                    currLineString.addPoint(pointXY);
                                    //console.log("currLineString added point: " + currLineString.getVertices()[currLineString.getVertices().length - 1]);
                                }
                                else {
                                    if (TracksInfo.PointsOfCurrentTrack.length < 2) {
                                        //console.log("PointsOfCurrentTrack before add point length: " + TracksInfo.PointsOfCurrentTrack.length);
                                        TracksInfo.PointsOfCurrentTrack.push(pointXY);
                                        //console.log("PointsOfCurrentTrack added point: " + TracksInfo.PointsOfCurrentTrack[TracksInfo.PointsOfCurrentTrack.length - 1]);
                                        if (TracksInfo.PointsOfCurrentTrack.length == 2) {
                                            //console.log("PointsOfCurrentTrack.length: " + TracksInfo.PointsOfCurrentTrack.length);
                                            DrawAllTracks_initNewTrack(currTrackIndx);
                                            //console.log("TracksInfo.Tracks.length: " + TracksInfo.Tracks.length);
                                            currTrack = TracksInfo.Tracks[currTrackIndx];
                                            //console.log("currTrack: " + currTrack);
                                            currLineString = TracksInfo.Tracks[currTrackIndx].geometry;
                                            //console.log("currLineString: " + currLineString);
                                        }
                                    }
                                }
                            }
                            //console.log("TracksInfo.Tracks[currTrackIndx]= " + TracksInfo.Tracks[currTrackIndx]);
                            //console.log("currLineString= " + currLineString);
                            if (TracksInfo.Tracks[currTrackIndx] != undefined && currLineString != undefined) {
                                TracksInfo.LayerForTrack.drawFeature(TracksInfo.Tracks[currTrackIndx]);
                                //console.log("DrawAllTracks: drawed feature");
                                //console.log("DrawAllTracks: TracksInfo.Tracks[currTrackIndx].geometry.getVertices(): " + TracksInfo.Tracks[currTrackIndx].geometry.getVertices());
                                //console.log("DrawAllTracks: currLineString.getVertices().length: " + currLineString.getVertices().length);
                                var lastPoint = currLineString.getVertices()[currLineString.getVertices().length - 1];
                                var lastLonLat = new OpenLayers.LonLat(lastPoint.x, lastPoint.y);
                                map.panTo(lastLonLat);
                            }
                            else if (TracksInfo.PointsOfCurrentTrack.length > 0) {
                                var lastPoint = TracksInfo.PointsOfCurrentTrack[TracksInfo.PointsOfCurrentTrack.length - 1];
                                var lastLonLat = new OpenLayers.LonLat(lastPoint.x, lastPoint.y);
                                map.panTo(lastLonLat);
                            }

                        } catch (ex) {
                            console.error("Не предвиденное искючение в время добавления координат: " + ex);
                        }
                    }
                }
            }
        }
        catch (ex) {
            //console.log("DrawAllTracks: " + ex);
            throw ex;
        }
        
    }

    function DrawTrack_FirstTwoPoint() {
        var currPoints = TracksInfo.PointsOfCurrentTrack;
        var strLocations = Android.getTrack(TracksInfo.Tracks.length - 1, currPoints.length);
        if (strLocations != "") {
            var coordinates = strLocations.split(",");
            //console.log("FirstTwoPoint: coordinates.length / 2  = " + coordinates.length / 2);
            //console.log("FirstTwoPoint: currPoints.length = " + currPoints.length);

            if (coordinates.length % 2 == 0) {
                try {

                    for (var i = 0; i < coordinates.length; i += 2) {

                        var newPoint = new OpenLayers.LonLat(parseFloat(coordinates[i]), parseFloat(coordinates[i + 1])).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                        newPoint = new OpenLayers.Geometry.Point(newPoint.lon, newPoint.lat);
                        currPoints.addPoint(newPoint);

                        if (currPoints.length >= 1) {
                            clearInterval(intervalId_DrawTraceRoute_FirstTwoPoint);
                            DrawTrack_InitOverlay();
                            break;
                        }
                    }
                    var lastPoint = currPoints[currPoints.length - 1];
                    var lastLonLat = new OpenLayers.LonLat(lastPoint.x, lastPoint.y);
                    map.panTo(lastLonLat);
                } catch (ex) {
                    console.error("FirstTwoPoint: Не предвиденное искючение в время добавления координат: " + ex);
                }
            }
        }
        else
            console.log("FirstTwoPoint: Нет новых координат");
    }
    function DrawTrack_InitOverlay() {        

        
        var traceTrackLineString = new OpenLayers.Geometry.LineString(TracksInfo.PointsOfCurrentTrack);
        TracksInfo.Tracks.push(new OpenLayers.Feature.Vector(traceTrackLineString, null, style));
        TracksInfo.LayerForTrack.addFeatures([TracksInfo.Tracks[TracksInfo.Tracks.length - 1]]);

        console.debug("DrawTrack_InitOverlay: new TrackFeature added to layer");
        DrawTraceRoute();
        try {
            var intervalMs = Android.getRateRedrawMs();
            setInterval(DrawTraceRoute, intervalMs);
            //console.log("Intreval exec for function DrawTraceRoute installed, intervalMs:" + intervalMs);
        } catch (ex) {

        }
    }


    function DrawTracks() {
        var countOfTrack = TracksInfo.Tracks.length;
        if (TracksInfo.Tracks.length != Android.getCountOfTrack()) {
            TracksInfo.PointsOfCurrentTrack = new OpenLayers.Geometry.MultiPoint([]); ;                        
            TracksInfo.Intervals.FirstTwoPointId = setInterval(DrawTrack_FirstTwoPoint, TracksInfo.Intervals.RateRedraw);
            //console.log("Intreval exec for function DrawTrack_FirstTwoPoint installed, intervalMs:" + TracksInfo.Intervals.RateRedraw);
        }
        var currTracFeature = TracksInfo.Tracks[countOfTrack - 1];
        var currLineString = currTracFeature.geometry;
        var strLocations = Android.getTrack(countOfTrack -  1, currLineString.getVertices().length);
        if (strLocations != "") {
            var coordinates = strLocations.split(",");
            console.debug("DrawTraceRoute: " + coordinates.length / 2 + "новых координат");
            console.debug("DrawTraceRoute: " + currLineString.getVertices().length + "старых координат");
            if (coordinates.length % 2 == 0) {
                try {

                    for (var i = 0; i < coordinates.length; i += 2) {
                        var newPoint = new OpenLayers.LonLat(parseFloat(coordinates[i]), parseFloat(coordinates[i + 1])).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                        newPoint = new OpenLayers.Geometry.Point(newPoint.lon, newPoint.lat);
                        currLineString.addPoint(newPoint);
                    }
                    TracksInfo.LayerForTrack.drawFeature(currTracFeature);
                    var lastPoint = currLineString.getVertices()[currLineString.getVertices().length - 1];
                    var lastLonLat = new OpenLayers.LonLat(lastPoint.x, lastPoint.y);
                    //console.log("getVertices lastPoint count: " + lastLonLat.lon + "," + lastLonLat.lon);
                    ////console.log("DrawTraceRoute: [" + lastPoint.lon +"," + lastPoint.lat + "]");
                    map.panTo(lastLonLat);
                } catch (ex) {
                    console.error("DrawTraceRoute: Не предвиденное искючение в время добавления координат: " + ex);
                }
            }
        }
        else
            console.debug("DrawTraceRoute: Нет новых координат");
    }
    try {
        TracksInfo.Intervals.RateRedraw = Android.getRateRedrawMs();
        TracksInfo.Intervals.DrawAllTracksID = setInterval(DrawAllTracks, TracksInfo.Intervals.RateRedraw);
        window.onunload = function () {
            clearInterval(TracksInfo.Intervals.DrawAllTracksID);
        }
        //console.log("Intreval exec for function DrawAllTracks installed, intervalMs:" + TracksInfo.Intervals.RateRedraw);
    } catch (ex) { }

    traceRouteCoorArr = [];
    function DrawTraceRoute_FirstTwoPoint() {
        var strLocations = Android.getRoute(traceRouteCoorArr.length);
        if (strLocations != "") {
            var coordinates = strLocations.split(",");
            //console.log("FirstTwoPoint: coordinates.length / 2  = " + coordinates.length / 2);
            //console.log("FirstTwoPoint: traceRouteCoorArr.length = " + traceRouteCoorArr.length);

            if (coordinates.length % 2 == 0) {
                try {

                    for (var i = 0; i < coordinates.length; i += 2) {

                        var newPoint = new OpenLayers.LonLat(parseFloat(coordinates[i]), parseFloat(coordinates[i + 1])).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                        newPoint = new OpenLayers.Geometry.Point(newPoint.lon, newPoint.lat);
                        traceRouteCoorArr.push(newPoint);

                        if (traceRouteCoorArr.length >= 1) {
                            clearInterval(intervalId_DrawTraceRoute_FirstTwoPoint);
                            InitializeOverlayLineString();
                            break;
                        }
                    }
                    var lastPoint = traceRouteCoorArr[traceRouteCoorArr.length - 1];
                    var lastLonLat = new OpenLayers.LonLat(lastPoint.x, lastPoint.y);
                    map.panTo(lastLonLat);
                } catch (ex) {
                    console.error("FirstTwoPoint: Не предвиденное искючение в время добавления координат: " + ex);
                }
            }
        }
        else
            console.log("FirstTwoPoint: Нет новых координат");
    }
    function InitializeOverlayLineString() {


        lineLayer = new OpenLayers.Layer.Vector("Line Layer", { renderers: ["Canvas", "SVG", "VML"] });
        style = {
            strokeColor: Android.getPolylineColorRgbHex(),
            strokeOpacity: Android.getPolylineOpacityPercent() / 100,
            strokeWidth: Android.getPolylineWidthPx()
        };
        traceRouteLineString = new OpenLayers.Geometry.LineString(traceRouteCoorArr);
        polygonFeature = new OpenLayers.Feature.Vector(traceRouteLineString, null, style);
        lineLayer.addFeatures([polygonFeature]);
        map.addLayer(lineLayer);

        console.debug("InitializeOverlayLineString: traceRouteLineString is created");
        DrawTraceRoute();
        try {
            var intervalMs = Android.getRateRedrawMs();
            setInterval(DrawTraceRoute, intervalMs);
            //console.log("Intreval exec for function DrawTraceRoute installed, intervalMs:" + intervalMs);
        } catch (ex) {

        }
    }


    function DrawTraceRoute() {

        var strLocations = Android.getRoute(traceRouteLineString.getVertices().length);
        if (strLocations != "") {
            var coordinates = strLocations.split(",");
            console.debug("DrawTraceRoute: " + coordinates.length / 2 + "новых координат");
            console.debug("DrawTraceRoute: " + traceRouteLineString.getVertices().length + "старых координат");
            if (coordinates.length % 2 == 0) {
                try {

                    for (var i = 0; i < coordinates.length; i += 2) {
                        var newPoint = new OpenLayers.LonLat(parseFloat(coordinates[i]), parseFloat(coordinates[i + 1])).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                        newPoint = new OpenLayers.Geometry.Point(newPoint.lon, newPoint.lat);
                        traceRouteLineString.addPoint(newPoint);
                    }
                    lineLayer.drawFeature(polygonFeature);
                    var lastPoint = traceRouteLineString.getVertices()[traceRouteLineString.getVertices().length - 1];
                    var lastLonLat = new OpenLayers.LonLat(lastPoint.x, lastPoint.y);
                    //console.log("getVertices lastPoint count: " + lastLonLat.lon + "," + lastLonLat.lon);
                    ////console.log("DrawTraceRoute: [" + lastPoint.lon +"," + lastPoint.lat + "]");
                    map.panTo(lastLonLat);
                } catch (ex) {
                    console.error("DrawTraceRoute: Не предвиденное искючение в время добавления координат: " + ex);
                }
            }
        }
        else
            console.debug("DrawTraceRoute: Нет новых координат");
    }
    /*
    try {
        var intervalMs = Android.getRateRedrawMs();
        intervalId_DrawTraceRoute_FirstTwoPoint = setInterval(DrawTraceRoute_FirstTwoPoint, intervalMs);
        //console.log("Intreval exec for function DrawTraceRoute_FirstTwoPoint installed, intervalMs:" + intervalMs);
    } catch (ex) { }
    */
    function ZoomControlForAndroid(map) {
        var _map = map;
        this.ZoomInListener = function () {
            map.zoomIn();
        }
        this.ZoomOutListener = function () {
            map.zoomOut();
        }
        this.AddToMap = function (/*string*/mapId) {
            this.zoomControl = vSeRGv.zoomControl.Create(this.ZoomInListener, this.ZoomOutListener);
            var mapDiv = document.getElementById(mapId);

            mapDiv.appendChild(this.zoomControl);


        }
    }
    var zoomControlOnMap = new ZoomControlForAndroid(map);
    zoomControlOnMap.AddToMap("basicMap");

    function createMarkerObj(location, options) {
        var size = new OpenLayers.Size(48, 48);
        var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
        var icon = new OpenLayers.Icon('../Img/marker.png', size, offset);
        var lonLat = new OpenLayers.LonLat(location.Longitude, location.Latitude);
        lonLat = lonLat.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
        var marker = new OpenLayers.Marker(lonLat, icon);

        marker.events.register("touchstart", marker, function () {
            Android.showCurrentDestinationStation();
        });   
        TracksInfo.LayerForDestinationStation.addMarker(marker);
        return marker;
    }
}

menuCommand = {};
menuCommand.ZoomIn = function () {
    this.map.zoomIn();
}
menuCommand.ZoomOut = function () {
    this.map.zoomOut();
}
menuCommand.getLastKnownLocation = function () {
    var lastKnownLocation;
    try {
        var strLoc = Android.getLastKnownLocation();
        if (strLoc != "") {
            var coordinates = strLoc.split(",");
            lastKnownLocation = new OpenLayers.LonLat(parseFloat(coordinates[0]), parseFloat(coordinates[1]));
        }
        else {
            lastKnownLocation = new OpenLayers.LonLat(48.03034, 46.349636);
        }
    }
    catch (ex) {
        lastKnownLocation = new OpenLayers.LonLat(48.03034, 46.349636);
    }
    return lastKnownLocation
}
menuCommand.GoHome = function () {
    this.map.setCenter(menuCommand.getLastKnownLocation() // Center of the map
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
          )
        );
}
