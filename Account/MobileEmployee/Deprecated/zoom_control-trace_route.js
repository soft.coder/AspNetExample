/// <reference path="../jquery-1.4.1-vsdoc.js">
vSeRGv = {};

(function(){
	vSeRGv.getWindowClientWidthHeight = function (){
		var windowSize = {};
		if (window.innerHeight){
			windowSize.ClientWidth = window.innerWidth;
			windowSize.ClientHeight = window.innerHeight;
		}
		//  > IE6
		else if(document.documentElement && document.documentElement.clientWidth){
			windowSize.ClientWidth = document.documentElement.clientWidth;
			windowSize.ClientHeight = document.documentElement.clientHeight;
		}
		//IE4 IE5 [IE6 without doctype]
		else if(document.body.clientWidth){
			windowSize.ClientWidth = document.body.clientWidth;
			windowSize.ClientHeight = document.body.clientHeight;
		}
		else{
			console.warn("Detemined client window fail");
			return null;
		}
		return windowSize;
	}
	
	vSeRGv.zoomControl = {};
	var zoomControlWidth;
	var zoomControlWidthMax;
	vSeRGv.zoomControl.getWidth = function(  ) {
		return zoomControlWidth;
	}
	vSeRGv.zoomControl.getMaxWidth =  function(){
		return zoomControlWidthMax;
	}
	vSeRGv.zoomControl.Create = function(/*eventListener*/zoomInListener, zoomOutListener, maxWidth){
		var zoomControl = document.createElement("div");
		zoomControl.id = "zoomControlAndroidPhone";
		

		
		var zoomIn = document.createElement("div");
		 zoomIn.id = "zoomInAndroidPhone";
		 zoomIn.className = "zoomButtonAndroidPhone";
		 zoomIn.onclick = zoomInListener

		var zoomOut = document.createElement("div");
		 zoomOut.id = "zoomOutAndroidPhone";
		 zoomOut.className = "zoomButtonAndroidPhone";
		 zoomOut.onclick = zoomOutListener;

			var zoomOutImg = document.createElement("div");
			 zoomOutImg.className = "zoomOutImage";

		 zoomOut.appendChild(zoomOutImg);


		 
		 		
		if ( maxWidth ) 
			zoomControlWidthMax = maxWidth;
		else	
			zoomControlWidthMax = 500;
			
		var windowSize = vSeRGv.getWindowClientWidthHeight();
		
		if ( windowSize ){
			if ( windowSize.ClientWidth > zoomControlWidthMax ) 
				zoomControlWidth = zoomControlWidthMax;
			else
				zoomControlWidth = windowSize.ClientWidth;
				
			zoomControl.style.left = (windowSize.ClientWidth / 2 ).toFixed(0) + "px";
		}else{
			zoomControlWidth = zoomControlWidthMax;			
		}
		
		zoomControl.style.width = zoomControlWidth + "px";
		zoomControl.style.marginLeft =  "-" + (zoomControlWidth /  2 ).toFixed(0) + "px";
		 
		 zoomIn.style.width = ((zoomControlWidth / 2).toFixed (0)- 5 * 4) + "px";
		 zoomOut.style.width = zoomIn.style.width;
		 zoomControl.appendChild(zoomIn);
		 zoomControl.appendChild(zoomOut);
		 
		 return zoomControl;
	}
	vSeRGv.getComputedStyle = function(/*DOM Element*/ domElement){
		var computedStyle;
		if(window.getComputedStyle)
			computedStyle = window.getComputedStyle(domElement, null);
		else if ( domElement.currentStyle ) 
			computedStyle = domElement.currentStyle;
		else{
			console.warn("vSeRGv.getComputedStyle: computedStyle don't support this browser");
			return null;
		}
		return computedStyle;
	}
})( );