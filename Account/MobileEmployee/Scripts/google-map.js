﻿/// <reference path="../google-map.aspx" />

var poly;
var map;
var polyOptions;
window.onload = initialize() {
    var moscow = new google.maps.LatLng(55.749917, 37.616501);

    var lastKnownLocation;
    try {
        var strLoc = Android.getLastKnownLocation();
        if (strLoc != "") {
            var coordinates = strLoc.split(",");
            lastKnownLocation = new google.maps.LatLng(parseFloat(coordinates[1]), parseFloat(coordinates[0]));
        }
        else {
            lastKnownLocation = new google.maps.LatLng(46.349636, 48.03034);
        }
    }
    catch (ex) {
        lastKnownLocation = new google.maps.LatLng(46.349636, 48.03034);
    }
    var mapType;
    try{
        mapType = Android.getCurrentMapType("google");
        if (mapType == "")
            throw new Error("getCurrentMapType: Android return empty string");
        console.log("Android mapType setted: " + mapType);
    }
    catch (ex) {
        console.log("Error when setting map type of google: " + ex);
        mapType = google.maps.MapTypeId.ROADMAP;
        console.log("Defalut mapType setted: " + mapType);
    }
    
    var myOptions = {
        zoom: 17,
        center: lastKnownLocation,
        mapTypeId: mapType,
        disableDefaultUI: true
    };
    mapDiv = document.getElementById('map_canvas');
    map = new google.maps.Map(mapDiv, myOptions);

    menuCommand.map = map;

    polyOptions;
    try {
        polyOptions = {
            strokeColor: Android.getPolylineColorRgbHex(),
            strokeOpacity: Android.getPolylineOpacityPercent() / 100,
            strokeWeight: Android.getPolylineWidthPx()
        }
    } catch (ex) {
        polyOptions = {
            strokeColor: '#FF0000',
            strokeOpacity: 0.5,
            strokeWeight: 5
        }
    }
    poly = new google.maps.Polyline(polyOptions);
    poly.setMap(map);

    polyTraceRoute = new google.maps.Polyline(polyOptions);
    polyTraceRoute.setMap(map);

    TracksInfo = {};
    TracksInfo.Tracks = [];    
    TracksInfo.Tracks[0] = new google.maps.Polyline(polyOptions);
    TracksInfo.Tracks[0].setMap(map);
    function DrawTracks() {
        var countOfTrack = Android.getCountOfTrack();

        for (var currTrackIndx = TracksInfo.Tracks.length - 1; currTrackIndx < countOfTrack; currTrackIndx++) {
            if (TracksInfo.Tracks[currTrackIndx] == undefined) {
                TracksInfo.Tracks[currTrackIndx] = new google.maps.Polyline(polyOptions);
                TracksInfo.Tracks[currTrackIndx].setMap(map);
                console.log("DrawTracks: initialized new polyline");
            }
            var currTrackLocations = TracksInfo.Tracks[currTrackIndx].getPath();
            var strLocations = Android.getTrack(currTrackIndx, currTrackLocations.getLength());
            if (strLocations != "") {
                var coordinates = strLocations.split(",");
                if (coordinates.length % 2 == 0) {
                    try {
                        for (var i = 0; i < coordinates.length; i += 2) {
                            currTrackLocations.push(new google.maps.LatLng(parseFloat(coordinates[i + 1]), parseFloat(coordinates[i])));
                            var msgAddedNewPoint = "DrawTracks: added new Point to track: " + currTrackIndx;
                            msgAddedNewPoint += " LatLng(" + currTrackLocations.getAt(0).lat() + "," + currTrackLocations.getAt(0).lng() + ")";
                            console.log(msgAddedNewPoint);
                        }
                        map.panTo(currTrackLocations.getAt(currTrackLocations.getLength() - 1));
                    } catch (ex) {
                        console.error("Не предвиденное искючение в время добавления координат: " + ex);
                    }
                }
                else if (coordinates.length == 2) {
                    map.setCenter(new google.maps.LatLng(parseFloat(coordinates[0]), parseFloat(coordinates[1])));
                }
            }
            else
                console.debug("Нет новых координат");
        }
    }

    function ZoomControlForAndroid() {
        this.ZoomInListener = function () {
            var res = map.setZoom(map.getZoom() + 1);
        }
        this.ZoomOutListener = function () {
            var res = map.setZoom(map.getZoom() - 1);
        }
        this.AddToMap = function (/*string*/mapId) {
            this.zoomControl = vSeRGv.zoomControl.Create(this.ZoomInListener, this.ZoomOutListener);
            var mapDiv = document.getElementById(mapId);

            mapDiv.appendChild(this.zoomControl);


        }
    }
    var zoomControlOnMap = new ZoomControlForAndroid();
    zoomControlOnMap.AddToMap("map_canvas");
    try {
        var intervalMs = Android.getRateRedrawMs();
        TracksInfo.IntervalID = setInterval(DrawTracks, intervalMs);
        window.onunload = function () {
            clearInterval(TracksInfo.IntervalID);
            Android.ShowToast("onunlod event: Google Map - ClearIntraval for function DrawTracks")
        }

        console.log("Intreval exec for function DrawTracks installed, intervalMs:" + intervalMs);
    } catch (ex) { }
}
menuCommand = {};
menuCommand.ZoomIn = function() {
    this.map.setZoom(map.getZoom() + 1);
}
menuCommand.ZoomOut = function() {
    this.map.setZoom(map.getZoom() - 1);
}
menuCommand.getLastKnownLocation = function () {
    var lastKnownLocation;
    try {
        var strLoc = Android.getLastKnownLocation();
        if (strLoc != "") {
            var coordinates = strLoc.split(",");
            lastKnownLocation = new google.maps.LatLng(parseFloat(coordinates[1]), parseFloat(coordinates[0]));
        }
        else {
            lastKnownLocation = new google.maps.LatLng(46.349636, 48.03034);
        }
    }
    catch (ex) {
        lastKnownLocation = new google.maps.LatLng(46.349636, 48.03034);
    }
    return lastKnownLocation
}
menuCommand.GoHome = function () {
    this.map.setCenter(menuCommand.getLastKnownLocation());
}
menuCommand.SetMapType = function (mapType) {
    if (this.map.getMapTypeId() != mapType) {
        try {
            this.map.setMapTypeId(mapType);
            Android.setCurrentMapType("google", this.map.getMapTypeId());
        }
        catch (ex) {
            console.log(ex);
        }

    }
}
function DrawTraceRoute() {
    traceRouteCoord = polyTraceRoute.getPath();
    var strLocations = Android.getRoute(traceRouteCoord.getLength());
    if (strLocations != "") {
        var coordinates = strLocations.split(",");
        console.debug(coordinates.length / 2 + "новых координат");
        console.debug(traceRouteCoord.getLength() + "старых координат");
        if (coordinates.length % 2 == 0) {
            try {
                for (var i = 0; i < coordinates.length; i += 2) {
                    traceRouteCoord.push(new google.maps.LatLng(parseFloat(coordinates[i + 1]), parseFloat(coordinates[i])));
                }
                map.panTo(traceRouteCoord.getAt(traceRouteCoord.getLength() - 1));
            } catch (ex) {
                console.error("Не предвиденное искючение в время добавления координат: " + ex);
            }
        }
        else if (coordinates.length == 2) {
            map.setCenter(new google.maps.LatLng(parseFloat(coordinates[0]), parseFloat(coordinates[1])));
        }
    }
    else
        console.debug("Нет новых координат");
}


