﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Account/MobileEmployee/MobileEmployeeMasterPage.master" AutoEventWireup="true" CodeFile="google-map.aspx.cs" Inherits="Account_MobileEmployee_google_map" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="Styles/google-map.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCNvcSJ5S7R9kRx9-ZgtWz2rRcK2OKElUQ&sensor=true">
    </script>
    <script src="Scripts/zoom_control-trace_route.js" type="text/javascript"></script>
    <link href="Styles/zoom_control-trace_route.css" rel="stylesheet" type="text/css" />    
    <script src="Scripts/google-map.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="map_canvas" style="width:100%; height:100%"></div>
</asp:Content>

