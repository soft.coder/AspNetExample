﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using System.Data;

public partial class Account_Manager_Supervisors : System.Web.UI.Page
{

    protected Guid GetUserIdOfCurrentUser()
    {
        Guid supervisorUserId = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());
        return supervisorUserId;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if ( !Page.IsPostBack )
            this.ucSubordinationUsers.SuperiorUserId = this.GetUserIdOfCurrentUser();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {


    }
    
}