﻿/// <reference path="../../Scripts/zoom_control-trace_route.js" />
/// <reference path="../Map.aspx" />

/* При успешной загрузке API выполняется
соответствующая функция */

TraceTrack = {};
TraceTrack.Placemark = [];
TraceTrack.Circle = {};
TraceTrack.Circle.Style = {
    // Цвет заливки. Последние цифры "77" - прозрачность.
    // Прозрачность заливки также можно указывать в "fillOpacity".
    fillColor: "#DB709377",
    // Цвет и прозрачность линии окружности.
    strokeColor: "#990066",
    strokeOpacity: 0.8,
    // Ширина линии окружности
    strokeWidth: 2
};
TraceTrack.Circle.Radius = 100;
TraceTrack.Circle.Items = [];

ymaps.ready(function () {
    var header = document.getElementById("siteMasterHeader");
    var headerHeight = parseInt(window.getComputedStyle(header, null).height);
    var windowSize = vSeRGv.getWindowClientWidthHeight();
    var yMapsHeight = windowSize.ClientHeight - headerHeight;
    var yMapsWidth = windowSize.ClientWidth;
    var yMapsDom = document.getElementById("map");
    yMapsDom.style.width = yMapsWidth + "px";
    yMapsDom.style.height = yMapsHeight + "px";
    var myMap = new ymaps.Map("map", {
        // Центр карты
        center: [46.348729, 48.036677],
        // Коэффициент масштабирования
        zoom: 15,
        // Тип карты
        type: "yandex#map"
    }
    );

    TraceTrack.YMap = myMap;

    myMap.controls
    // Кнопка изменения масштаба
    .add('zoomControl')
    // Список типов карты
    .add('typeSelector')
    // Стандартный набор кнопок
    .add('mapTools');
    // Также в метод add можно передать экземпляр класса, реализующего определенный элемент управления.
    // Например, линейка масштаба ('scaleLine')
    myMap.controls
    .add(new ymaps.control.ScaleLine())
    // В конструкторе элемента управления можно задавать расширенные
    // параметры, например, тип карты в обзорной карте
    .add(new ymaps.control.MiniMap({
        type: 'yandex#publicMap'
    }))




    TraceTrack.MobileEmps = {};
    MobEmps = TraceTrack.MobileEmps;
    MobEmps.Items = [];

    MobEmps.FindById = function (id) {
        var mobEmps = MobEmps.Items;
        for (var i = 0; i < mobEmps.length; i++) {
            if (mobEmps[i].ID == id)
                return mobEmps[i];
        }
    }


    MobEmps.Color = {};

    MobEmps.Color.GetRandomColorHex = function () {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.round(Math.random() * 15)];
        }
        return color;
    }

    MobEmps.Clear = function () {
        var mobEmps = MobEmps.Items;
        for (var i = 0; i < mobEmps.length; i++) {
            var lastPos = mobEmps[i].LastPosition;
            if (lastPos != undefined)
                myMap.geoObjects.remove(lastPos.Circle.YMapObject);
            var tracks = mobEmps[i].Tracks;
            for (var j = 0; j < tracks.length; j++) {
                if (tracks[j].Polyline != undefined)
                    myMap.geoObjects.remove(tracks[j].Polyline);
            }
        }
    }

    MobEmps.DrawTracksOfMobEmps = function (mobEmps) {
        for (var i = 0; i < mobEmps.length; i++) {
            mobEmps[i].ColorOfTrack = MobEmps.Color.GetRandomColorHex();
            var tracks = mobEmps[i].Tracks;
            for (var j = 0; j < tracks.length; j++) {
                var pointsOfTrack = [];
                var locations = tracks[j].Locations;
                for (var k = 0; k < locations.length; k++) {
                    pointsOfTrack.push([locations[k].Latitude, locations[k].Longitude]);
                }
                var optionsOfTrack = { strokeColor: mobEmps[i].ColorOfTrack, strokeWidth: 5, strokeOpacity: 0.8 };
                var contentOfTrack = { hintContent: "TrackId: " + tracks[j].TrackId + ";\n " + mobEmps[i].FirstName + " " + mobEmps[i].LastName };
                tracks[j].Polyline = new ymaps.Polyline(pointsOfTrack, contentOfTrack, optionsOfTrack);
                myMap.geoObjects.add(tracks[j].Polyline);

                if (j == tracks.length - 1) {
                    mobEmps[i].LastPosition = {};
                    mobEmps[i].LastPosition.Coordinate = pointsOfTrack[pointsOfTrack.length - 1];
                    mobEmps[i].LastPosition.Circle = {};
                    // 77 - прозрачность
                    mobEmps[i].LastPosition.Circle.FillColor = MobEmps.Color.GetRandomColorHex() + "77";
                    mobEmps[i].LastPosition.Circle.Radius = 25;
                    mobEmps[i].LastPosition.Circle.YMapObject = new ymaps.Circle(
                        [
                            mobEmps[i].LastPosition.Coordinate,
                            mobEmps[i].LastPosition.Circle.Radius
                        ],
                        { hintContent: mobEmps[i].FirstName + " " + mobEmps[i].LastName },
                        {
                            fillColor: mobEmps[i].LastPosition.Circle.FillColor
                        }
                    );
                    myMap.geoObjects.add(mobEmps[i].LastPosition.Circle.YMapObject);
                }

            }
        }
    }


    MobEmps.UpdateTracks = function () {
        var mobEmps = MobEmps.Items;
        for (var i = 0; i < mobEmps.length; i++) {

            var oldLastTrack = mobEmps[i].Tracks[mobEmps[i].Tracks.length - 1];

            if (oldLastTrack == undefined || oldLastTrack == null)
                continue;

            // old last location of old last track
            var oldLastLocation = oldLastTrack.Locations[oldLastTrack.Locations.length - 1];

            if (oldLastLocation == undefined || oldLastTrack == null)
                continue;

            // DONT WORK CORRECT - beacuse this == Window, must be oldLastTrack
            // проблема в контексте вызова этого метода - см. замыкание
            // можно обойти если создать глобальный объект,
            // но тогда возникнет проблема с генерацией функции
            oldLastTrack.OnRequestCompleteUpdateLocations = function (locations) {
                if (locations != null && locations.length > 0) {
                    this.Locations = this.Locations.concat(locations);
                }
            }

            MapService.GetLocationsOfCurrentTrackAfterLastLocation(
                oldLastTrack.TrackId,
                oldLastLocation.LocationId,
                oldLastTrack.OnRequestCompleteUpdateLocations
            );

            // DONT WORK CORRECT - beacuse this == Window, must be mobEmps[i]
            mobEmps[i].OnRequestCompleteUpdateTracks = function (tracks) {
                if (tracks != null && tracks.length > 0) {
                    this.Tracks = this.Tracks.concat(tracks);
                }
            }

            var newTracks = MapService.GetTracksOfMobEmpAfterLast(
                mobEmps[i].ID,
                oldLastTrack.TrackId,
                mobEmps[i].OnRequestCompleteUpdateTracks
            );

        }
    }

    TraceTrack.DestinationSation = {};
    var DestStation = TraceTrack.DestinationSation;
    DestStation.Items = [];

    DestStation.GetFormatDate = function (date) {
        return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + " "
                                + date.getDate() + "." + date.getMonth() + "." + date.getFullYear();
    }



    DestStation.Drawing = function (destinationStations) {
        TraceTrack.DestinationSation.Items = destinationStations;
        var lstDs = destinationStations;
        var mobEmps = TraceTrack.MobileEmps;
        for (var i = 0; i < lstDs.length; i++) {
            var ds = lstDs[i];
            var accountableMobEmp = mobEmps.FindById(ds.UserInfoCommonId);
            var accountableName = "ID:" + accountableMobEmp.ID + " " + accountableMobEmp.FirstName + " " + accountableMobEmp.LastName;
            var timeAssign = this.GetFormatDate(ds.TimeAssign);
            var strPastPartOfContentBody = "<em>Ответственный:</em><br />" + accountableName + "<br /><em>Время назначения:</em><br />" + timeAssign;
            var strContentBody = ds.TaskDescription == "" ?
                                    strPastPartOfContentBody :
                                    "<em>Описание:</em><br />" + ds.TaskDescription + "<br />" + strPastPartOfContentBody;

            ds.Placemark = new ymaps.Placemark(
                    [ds.Latitude, ds.Longitude],
                    {
                        iconContent: ds.ID,
                        hintContent: accountableName,
                        balloonContentHeader: "ID:" + ds.ID + " " + ds.Title,
                        balloonContentBody: strContentBody,
                        balloonContentFooter: "<a href=\"javascript:TraceTrack.DestinationSation.Remove(" + ds.ID + ")\">Удалить</a>"
                    },
                    { preset: "twirl#nightIcon" }
            );
            myMap.geoObjects.add(ds.Placemark);
            ds.Circle = {};
            ds.Circle.Radius = 100;
            ds.Circle.FillColor = "#DB7093";
            ds.Circle.FillOpacity = 0.7;
            ds.Circle.StrokeColor = "#990066";
            ds.Circle.StrokeOpacity = 0.8;
            ds.Circle.StrokeWidth = 2;

            ds.Circle.YMapObj = new ymaps.Circle(
                [
            // Координаты центра круга
                    [ds.Latitude, ds.Longitude],
            // Радиус круга в метрах
                    ds.Circle.Radius
                ],
                {},
                {
                    // Цвет заливки. Последние цифры "77" - прозрачность.
                    // Прозрачность заливки также можно указывать в "fillOpacity".
                    fillColor: ds.Circle.FillColor,
                    fillOpacity: ds.Circle.FillOpacity,
                    // Цвет и прозрачность линии окружности.
                    strokeColor: ds.Circle.StrokeColor,
                    strokeOpacity: ds.Circle.StrokeOpacity,
                    // Ширина линии окружности
                    strokeWidth: ds.Circle.StrokeWidth
                }
            );
            myMap.geoObjects.add(ds.Circle.YMapObj);
        }
    }
    DestStation.ClearById = function (id) {
        var map = TraceTrack.YMap;
        var map = TraceTrack.YMap;
        var currDs = DestStation.Items;
        for (var i = 0; i < currDs.length; i++) {
            if (currDs[i].ID == id) {
                if (currDs[i].Placemark != undefined) {
                    map.geoObjects.remove(currDs[i].Placemark);
                    currDs[i].Placemark = null;
                }
                if (currDs[i].Circle != undefined && currDs[i].Circle.YMapObj != undefined) {
                    map.geoObjects.remove(currDs[i].Circle.YMapObj);
                    currDs[i].Circle.YMapObj = null;
                }
                return;
            }
        }
    }
    DestStation.Remove = function (id) {
        var lstDs = TraceTrack.DestinationSation.Items;
        for (var i = 0; i < lstDs.length; i++) {
            if (lstDs[i].ID == id) {
                MapService.RemoveDestinationStation(id,
                function (id) {
                    TraceTrack.DestinationSation.ClearById(id);
                },
                function () {
                    alert("При удалении пункта назначения произошла ошибка");
                });
                return;
            }
        }
    }

    DestStation.Clear = function () {
        var map = TraceTrack.YMap;
        var currDs = DestStation.Items;
        for (var i = 0; i < currDs.length; i++) {
            if (currDs[i].Placemark != undefined) {
                map.geoObjects.remove(currDs[i].Placemark);
                currDs[i].Placemark = null;
            }
            if (currDs[i].Circle != undefined && currDs[i].Circle.YMapObj != undefined) {
                map.geoObjects.remove(currDs[i].Circle.YMapObj);
                currDs[i].Circle.YMapObj = null;
            }
        }
        DestStation.Items = [];
        var succAdd = DestStation.Dialog.AddedItems.Success;
        for (var i = 0; i < succAdd.length; i++) {
            map.geoObjects.remove(succAdd[i])
        }
        DestStation.Dialog.AddedItems.Success = [];
    }



    DestinationStationDialog.Init("ucDestinationStation");

    DestStation.Dialog = {};

    DestStation.Dialog.AddedItems = {};
    DestStation.Dialog.AddedItems.Processing = {};
    DestStation.Dialog.AddedItems.Success = [];
    DestStation.Dialog.AddedItems.UnSuccess = [];

    DestStation.Dialog.Button = {};
    DestStation.Dialog.Button.Control = new ymaps.control.Button({
        data: {
            content: 'Пункт назанчения',
            title: 'Добавить пункт назначения'
        }
    }, {
        selectOnClick: true,
        position: { top: 5, left: 100 }
    }
    );


    DestStation.Dialog.Button.OnMapClick = function (e) {
        this.deselect();
        this.disable();
        var coordPosition = e.get("coordPosition");
        // for Yandex Lat - Lon
        DestinationStationDialog.Show(coordPosition[1], coordPosition[0]);
    }

    DestStation.Dialog.Button.OnSelect = function (e) {
        this.events.add('click', DestStation.Dialog.Button.OnMapClick, e.get("target"));
    }
    DestStation.Dialog.Button.OnDeselect = function (e) {
        this.events.remove('click', DestStation.Dialog.Button.OnMapClick, e.get("target"));

    }
    DestStation.Dialog.Button.Control.events.add('select', DestStation.Dialog.Button.OnSelect, myMap);
    DestStation.Dialog.Button.Control.events.add('deselect', DestStation.Dialog.Button.OnDeselect, myMap);
    myMap.controls.add(DestStation.Dialog.Button.Control);


    DestStation.Dialog.OnHideEvent = function (e) {
        if (e.IsCancel == false) {
            var mobEmp = MobEmps.FindById(e.mobileEmployeeID);
            var mobEmpName = mobEmp.FirstName + " " + mobEmp.LastName;
            DestStation.Dialog.AddedItems.Processing = new ymaps.Placemark(
                    [e.Latitude, e.Longitude],
                    {
                        hintContent: mobEmpName,
                        balloonContentHeader: e.Title,
                        balloonContentBody: e.TaskDescription,
                        balloonContentFooter: mobEmpName + " - отправка данных..."
                    },
                    { preset: "twirl#whiteDotIcon" }
            );
            myMap.geoObjects.add(DestStation.Dialog.AddedItems.Processing);
        }
        else {
            DestStation.Dialog.Button.Control.enable();
        }
    }
    DestinationStationDialog.AddOnHide(DestStation.Dialog.OnHideEvent);

    DestStation.Dialog.OnEndRequest = function (sender, args) {
        var error = args.get_error();
        //sender.remove_endRequest(DestStation.Dialog.OnEndRequest);
        DestStation.Dialog.Button.Control.enable();

        if (error == undefined) {
            DestStation.Dialog.AddedItems.Success.push(DestStation.Dialog.AddedItems.Processing);
            DestStation.Dialog.AddedItems.Processing.options.set({ preset: "twirl#darkgreenIcon" });
            var mobEmp = MobEmps.FindById(DestinationStationDialog.GetMobEmpId());
            var mobEmpName = mobEmp.FirstName + " " + mobEmp.LastName;
            DestStation.Dialog.AddedItems.Processing.properties.set({
                iconContent: DestinationStationDialog.GetMobEmpId(),
                hintContent: mobEmpName,
                balloonContentHeader: DestinationStationDialog.GetTitle(),
                balloonContentBody: DestinationStationDialog.GetTaskDescription(),
                balloonContentFooter: mobEmpName + " - " + DestStation.GetFormatDate(DestinationStationDialog.GetTimeAssign())
            });
        }
        else {
            var adDs = DestStation.Dialog.AddedItems.Processing;
            adDs.options.set({ preset: "twirl#redDotIcon" });
            adDs.properties.set({
                balloonContentFooter: "Произошла ошибка при отправке данных"
            });

            DestStation.Dialog.AddedItems.UnSuccess.push(adDs);
            adDs.events.add("balloonclose", function () {
                TraceTrack.YMap.geoObjects.remove(this);
            }, adDs);
            DestStation.Dialog.AddedItems.Processing = {};
        }
    }
    DestinationStationDialog.AddEndRequest(DestStation.Dialog.OnEndRequest);

    DestStation.OnRequestComplete = function (arDestStations) {
        DestStation.Clear();
        DestStation.Drawing(arDestStations);
    }

    MobEmps.UpdateInterval = 30000;

    MobEmps.OnRequestComplete = function (mobEmps) {
        MobEmps.Clear();
        MobEmps.Items = mobEmps;
        MapService.GetDestinationStationsOfSupervisor(DestStation.OnRequestComplete);
        MobEmps.DrawTracksOfMobEmps(mobEmps);
        setTimeout(
            function () {
                MapService.GetMobileEmpUserInfоDetails(TraceTrack.MobileEmps.OnRequestComplete);
            },
            MobEmps.UpdateInterval
        );

        // DONT WORK CORRECT
        //setTimeout(MobEmps.UpdateTracks, MobEmps.UpdateInterval);
    }


    MapService.GetMobileEmpUserInfоDetails(TraceTrack.MobileEmps.OnRequestComplete);

});

