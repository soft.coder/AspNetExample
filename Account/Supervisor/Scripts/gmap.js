﻿function pageLoad() {
    var header = document.getElementById("siteMasterHeader");
    var headerHeight = parseInt(window.getComputedStyle(header, null).height);
    var windowSize = vSeRGv.getWindowClientWidthHeight();
    var mapHeight = windowSize.ClientHeight - headerHeight;
    var mapWidth = windowSize.ClientWidth;
    var mapDom = document.getElementById("map");

    mapDom.style.width = mapWidth + "px";
    mapDom.style.height = mapHeight + "px";


    var lastKnownLocation = new google.maps.LatLng(46.349636, 48.03034);
    var myOptions = {
        zoom: 17,
        center: lastKnownLocation,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(mapDom, myOptions);
}