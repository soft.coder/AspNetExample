﻿TraceTrack = {};
TraceTrack.Placemark = [];
TraceTrack.Circle = {};
TraceTrack.Circle.Style = {
    // Цвет заливки. Последние цифры "77" - прозрачность.
    // Прозрачность заливки также можно указывать в "fillOpacity".
    fillColor: "#DB709377",
    // Цвет и прозрачность линии окружности.
    strokeColor: "#990066",
    strokeOpacity: 0.8,
    // Ширина линии окружности
    strokeWidth: 2
};
TraceTrack.Circle.Radius = 100;
TraceTrack.Circle.Items = [];
TraceTrack.MobileEmps = {};

TraceTrack.MobileEmps.UpdateInterval = 30000;

TraceTrack.MobileEmps.FindById = function (id) {
    var mobEmps = TraceTrack.MobileEmps.Items;
    for (var i = 0; i < mobEmps.length; i++) {
        if (mobEmps[i].ID == id)
            return mobEmps[i];
    }
}
TraceTrack.MobileEmps.Color = {};

TraceTrack.MobileEmps.Color.GetRandomColorHex = function () {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}
TraceTrack.MobileEmps.Clear = function (removePredicate) {
    var mobEmps = TraceTrack.MobileEmps.Items;
    var map = TraceTrack.Map.OSM;
    for (var i = 0; i < mobEmps.length; i++) {
        var lastPos = mobEmps[i].LastPosition;
        if (lastPos != undefined)
            removePredicate(lastPos.Circle.MapObject);
        var tracks = mobEmps[i].Tracks;
        for (var j = 0; j < tracks.length; j++) {
            if (tracks[j].MapObject != undefined)
                removePredicate(tracks[j].MapObject);
        }
    }
}
TraceTrack.MobileEmps.DrawTracksOfMobEmps = function (mobEmps, createLineMapObj, createCircleMapObj) {
    for (var i = 0; i < mobEmps.length; i++) {
        mobEmps[i].ColorOfTrack = TraceTrack.MobileEmps.Color.GetRandomColorHex();
        var tracks = mobEmps[i].Tracks;
        for (var j = 0; j < tracks.length; j++) {
            var locations = tracks[j].Locations;

            if (locations.length < 2)
                continue;

            var contentOfTrack = "TrackId: " + tracks[j].TrackId + ": " + mobEmps[i].FirstName + " " + mobEmps[i].LastName;
            var optionsOfTrack = { strokeColor: mobEmps[i].ColorOfTrack, strokeWidth: 5, strokeOpacity: 0.8, title: contentOfTrack };

            tracks[j].MapObject = createLineMapObj(locations, optionsOfTrack);

            if (j == tracks.length - 1) {
                var lastLocation = locations[locations.length - 1];
                var options = {}
                options.fillColor = TraceTrack.MobileEmps.Color.GetRandomColorHex();
                options.radius = 5;
                options.title = mobEmps[i].FirstName + " " + mobEmps[i].LastName;
                mobEmps[i].LastPositionMapObj = createCircleMapObj(
                    lastLocation,
                    options
                );
            }
        }
    }
}

TraceTrack.DestinationSation = {};
TraceTrack.DestinationSation.Items = [];

TraceTrack.DestinationSation.GetFormatDate = function (date) {
    return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + " "
                                + date.getDate() + "." + date.getMonth() + "." + date.getFullYear();
}


TraceTrack.DestinationSation.Drawing = function (destinationStations, createMarkerObj, createCircleMapObj) {
    TraceTrack.DestinationSation.Items = destinationStations;
    var lstDs = destinationStations;
    var mobEmps = TraceTrack.MobileEmps;
    for (var i = 0; i < lstDs.length; i++) {
        var ds = lstDs[i];
        var accountableMobEmp = mobEmps.FindById(ds.UserInfoCommonId);
        var accountableName = accountableMobEmp.FirstName + " " + accountableMobEmp.LastName;
        var timeAssign = this.GetFormatDate(ds.TimeAssign);
        var location = {
            Longitude: ds.Longitude,
            Latitude: ds.Latitude
        };
        var makerOptions = {
            id: ds.ID,
            userInfoCommonId: ds.UserInfoCommonId,
            fullName: accountableName,
            title: ds.Title,
            taskDescription: ds.TaskDescription,
            timeAssign: timeAssign,
            colorOfTrack: accountableMobEmp.ColorOfTrack
        };
        ds.MarkerObj = createMarkerObj(location, makerOptions);
        circleOptions = {
            radius: 25,
            fillColor: "#DB7093",
            fillOpacity: 0.7,
            strokeColor: "#990066",
            strokeOpacity: 0.8,
            strokeWidth: 2
        };
        //ds.CircleObj = createCircleMapObj(location, circleOptions);
    }
}
TraceTrack.DestinationSation.Clear = {};
TraceTrack.DestinationSation.Clear.RemoveHandler = {};
TraceTrack.DestinationSation.Clear.All = function () {
    var destinationStations = TraceTrack.DestinationSation.Items;
    var i;
    for (i = 0; i < destinationStations.length; i++) {
        if (destinationStations[i].MarkerObj == undefined)
            continue;
        TraceTrack.DestinationSation.Clear.RemoveHandler(destinationStations[i]);
    }
    console.log("Clear.All - Total Remove: " + i);
}
TraceTrack.DestinationSation.Clear.ById = function (id) {
    var destinationStations = TraceTrack.DestinationSation.Items;
    for (var i = 0; i < destinationStations.length; i++) {
        if (destinationStations[i].ID == id) {
            TraceTrack.DestinationSation.Clear.RemoveHandler(destinationStations[i]);
            return;
        }
    }
}
TraceTrack.DestinationSation.Remove = function (id) {
    var lstDs = TraceTrack.DestinationSation.Items;
    for (var i = 0; i < lstDs.length; i++) {
        if (lstDs[i].ID == id) {
            MapService.RemoveDestinationStation(id,
                function (id) {
                    TraceTrack.DestinationSation.Clear.ById(id);
                },
                function () {
                    alert("При удалении пункта назначения произошла ошибка");
                });
            return;
        }
    }
}



window.onload = function () {
    var header = document.getElementById("siteMasterHeader");
    var headerHeight = parseInt(vSeRGv.getComputedStyle(header).height);
    var windowSize = vSeRGv.getWindowClientWidthHeight();
    var mapHeight = windowSize.ClientHeight - headerHeight;
    var mapWidth = windowSize.ClientWidth;
    var mapDom = document.getElementById("map");
    mapDom.style.width = mapWidth + "px";
    mapDom.style.height = mapHeight + "px";
    var keyboardControl = new OpenLayers.Control.KeyboardDefaults();
    var options = {
        controls: [
            new OpenLayers.Control.Navigation({
                handleRightClicks: true,
                dragPanOptions: {
                    enableKinetic: true
                }
            }),
            new OpenLayers.Control.Zoom(),
            new OpenLayers.Control.ScaleLine(),
            new OpenLayers.Control.OverviewMap(),
            keyboardControl
          ]
    }

    var map = new OpenLayers.Map("map", options);
    var mapnik = new OpenLayers.Layer.OSM();
    map.addLayer(mapnik);
    var centerOfMap = new OpenLayers.LonLat(48.03034, 46.349636);
    map.setCenter(centerOfMap // Center of the map
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
          ), 15 // Zoom level
        );

    var vectorLayer = new OpenLayers.Layer.Vector("TracksLayer", { renderers: ["Canvas", "SVG", "VML"] });
    map.addLayer(vectorLayer);

    var vectorLayerForMarkers = new OpenLayers.Layer.Vector("Vector Layer for Markers Labels", {
        styleMap: new OpenLayers.StyleMap({ 'default': {
            pointRadius: 0,
            // label with \n linebreaks
            label: "${labelHtml}",
            fontColor: "${labelColor}",
            fontWeight: "bold",
            labelAlign: "cm",
            // only for SVG
            labelYOffset: -10,
            labelOutlineColor: "#D1D1D1",
            labelOutlineWidth: 1,
            fontOpacity: 1
        }
        }),
        renderers: ["Canvas"]
    });
    map.addLayer(vectorLayerForMarkers);

    var markers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(markers);




    function createCircleMapObj(location, options) {
        var pointStyle = {
            fillColor: options.fillColor || "#6666cc",
            fillOpacity: options.fillOpacity || 0.8,
            pointRadius: options.radius || 5,
            title: options.title || "",
            strokeColor: options.strokeColor || "#336666",
            strokeOpacity: options.strokeOpacity || 1,
            strokeWidth: options.strokeWidth || 2
        };

        var newLonLat = new OpenLayers.LonLat(location.Longitude, location.Latitude);
        // closure
        newLonLat = newLonLat.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
        var newPoint = new OpenLayers.Geometry.Point(newLonLat.lon, newLonLat.lat);
        var newPointFeature = new OpenLayers.Feature.Vector(newPoint, null, pointStyle);
        // closure
        vectorLayer.addFeatures([newPointFeature]);
        return newPointFeature;
    }

    function createLineMapObj(locations, options) {
        var lineStyle = {
            strokeColor: options.strokeColor || "#336666",
            strokeOpacity: options.strokeOpacity || 0.8,
            strokeWidth: options.strokeWidth || 5,
            title: options.title
        };

        if (location.length < 2)
            return;
        var points = new Array();
        for (var i = 0; i < locations.length; i++) {
            var newLonLat = new OpenLayers.LonLat(locations[i].Longitude, locations[i].Latitude);
            // closure
            newLonLat = newLonLat.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
            var newPoint = new OpenLayers.Geometry.Point(newLonLat.lon, newLonLat.lat);
            points.push(newPoint)
        }
        var newLineStringGeometry = new OpenLayers.Geometry.LineString(points);
        var newLineStringFeature = new OpenLayers.Feature.Vector(newLineStringGeometry, null, lineStyle);
        // closure
        vectorLayer.addFeatures([newLineStringFeature]);
        return newLineStringFeature;
    }

    function createPopupHtmlContent(options) {
        var html = "<p><strong>ID" + options.id + ": " + options.title + "</strong></p>" +
                    "<p>" + options.taskDescription + "</p>" +
                    "<p>ID" + options.userInfoCommonId + ": " + options.fullName + "</p>" +
                    "<p><em>" + options.timeAssign + "</em></p>" +
                    "<p><a href=\"javascript:TraceTrack.DestinationSation.Remove(" + options.id + ")\">Удалить</a></p>";
        return html;
    }
    function createLabelForMarker(lonLat, text, color) {
        var labelOfMarkerPoint = new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat);
        var labelOfMarkerFeature = new OpenLayers.Feature.Vector(labelOfMarkerPoint, { labelHtml: text, labelColor: color });
        // closure
        vectorLayerForMarkers.addFeatures([labelOfMarkerFeature]);
        return labelOfMarkerFeature;
    }

    function createMarkerObj(location, options) {
        var size = new OpenLayers.Size(48, 48);
        var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
        var icon = new OpenLayers.Icon('Img/marker.png', size, offset);
        var lonLat = new OpenLayers.LonLat(location.Longitude, location.Latitude);
        lonLat = lonLat.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
        var marker = new OpenLayers.Marker(lonLat, icon);

        var popupHtmlContent = createPopupHtmlContent(options);
        var popup = new OpenLayers.Popup(null,
                       lonLat,
                       new OpenLayers.Size(200, 200),
                       popupHtmlContent,
                       true,
                       function (e) {
                           // closure
                           map.removePopup(this);
                           e.cancelBubble = true;
                       });
        popup.autoSize = true;
        popup.setBackgroundColor("#FCFBB5");
        marker.events.register("mousedown", marker, function () {
            //one closure
            this.map.addPopup(popup, true)
        });
        // save popup in prop of marker
        marker.ds_popup = popup;
        //closure
        markers.addMarker(marker);
        // Dont use because SVG is slow rendering, and bad view 
        // save label vector feature in prop of marker
        marker.ds_label = createLabelForMarker(lonLat, "ID: " + options.id, options.colorOfTrack);
        return marker;
    }





    DestinationStationDialog.Init("ucDestinationStation");
    var DestStation = TraceTrack.DestinationSation;
    DestStation.Dialog = {};


    DestStation.Dialog.Button = {};
    var btnDlg = DestStation.Dialog.Button;


    btnDlg.Control = new OpenLayers.Control.Button({
        displayClass: "DsButton",
        title: 'Добавить пункт назначения',
        trigger: function () {
            if (this._isEnabled == true || this._isEnabled == null) {
                if (this.active == false || this.active == null)
                    this.activate();
                else
                    this.deactivate();
            }
        }
    });

    OpenLayers.Util.extend(btnDlg.Control, {
        _isEnabled: true,
        enable: function () {
            if (this._isEnabled != true) {
                this._isEnabled = true;
                vSeRGv.removeClass(this.panel_div, "Disabled");
            }
        },
        disable: function () {
            if (this._isEnabled != false) {
                this._isEnabled = false;
                vSeRGv.addClass(this.panel_div, "Disabled");
            }
        },
        dialog: DestinationStationDialog,
        onMapClick: function (e) {
            this.disable();
            // closure
            keyboardControl.deactivate();
            var lonlat = this.map.getLonLatFromPixel(e.xy);
            lonlat = lonlat.transform(map.getProjectionObject(), new OpenLayers.Projection("EPSG:4326"));
            this.dialog.Show(lonlat.lon, lonlat.lat);
        },
        onHideEvent: function (e) {
            if (e.IsCancel == true)
                this.enable();
            this.deactivate();
            keyboardControl.activate();
        },
        onEndRequest: function (sender, args) {
            var error = args.get_error();
            this.enable();
            if (error == undefined) {
                var dlgDs = this.dialog;
                var mobEmp = TraceTrack.MobileEmps.FindById(dlgDs.GetMobEmpId());
                var mobEmpName = mobEmp.FirstName + " " + mobEmp.LastName;
                var lonlat = new OpenLayers.LonLat(dlgDs.GetLongitude(), dlgDs.GetLatitude());
                //lonlat = lonlat.transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject())
                createMarkerObj({ Longitude: lonlat.lon, Latitude: lonlat.lat }, { title: dlgDs.GetTitle(),
                    taskDescription: dlgDs.GetTaskDescription(),
                    id: dlgDs.GetMobEmpId(),
                    fullName: mobEmpName,
                    timeAssign: TraceTrack.DestinationSation.GetFormatDate(dlgDs.GetTimeAssign())
                });
            }
            else
                alert("Произошла ошибка при добавлении нового пункта назначения.");
        }
    });
    btnDlg.Control.events.on({
        "activate": function () {
            this.map.events.register("click", this, this.onMapClick);
        },
        "deactivate": function () {
            this.map.events.unregister("click", this, this.onMapClick);
        }
    });
    DestinationStationDialog.AddOnHide(btnDlg.Control.onHideEvent, btnDlg.Control);
    DestinationStationDialog.AddEndRequest(btnDlg.Control.onEndRequest, btnDlg.Control);


    btnDlg.Panel = new OpenLayers.Control.Panel();
    btnDlg.Panel.addControls([btnDlg.Control]);
    map.addControl(btnDlg.Panel);


    TraceTrack.DestinationSation.Clear.RemoveHandler = function (ds) {
        if (ds.MarkerObj != undefined) {
            vectorLayerForMarkers.destroyFeatures([ds.MarkerObj.ds_label]);
            markers.removeMarker(ds.MarkerObj);
            //ds.MarkerObj.ds_label.destroy();
            ds.MarkerObj.ds_popup.destroy();
            ds.MarkerObj.destroy();
            ds.MarkerObj = null;
        }
        else {
            alert("MarkerObj of ds.ID:" + ds[i].ID + " undefined");
        }
    }

    TraceTrack.DestinationSation.OnRequestComplete = function (destinationStations) {
        TraceTrack.DestinationSation.Clear.All();
        TraceTrack.DestinationSation.Items = destinationStations;
        TraceTrack.DestinationSation.Drawing(destinationStations, createMarkerObj, createCircleMapObj);
    }

    TraceTrack.MobileEmps.OnRequestComplete = function (mobEmps) {
        vectorLayer.removeAllFeatures();
        TraceTrack.MobileEmps.Items = mobEmps;
        MapService.GetDestinationStationsOfSupervisor(TraceTrack.DestinationSation.OnRequestComplete);
        TraceTrack.MobileEmps.DrawTracksOfMobEmps(mobEmps, createLineMapObj, createCircleMapObj);
        setTimeout(
            function () {
                MapService.GetMobileEmpUserInfоDetails(TraceTrack.MobileEmps.OnRequestComplete);
            },
            TraceTrack.MobileEmps.UpdateInterval
        );
    }
    MapService.GetMobileEmpUserInfоDetails(TraceTrack.MobileEmps.OnRequestComplete);
}