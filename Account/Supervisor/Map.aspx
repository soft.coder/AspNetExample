﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Account/Supervisor/Supervisor.master" AutoEventWireup="true" CodeFile="Map.aspx.cs" Inherits="Account_Supervisor_Default" %>
<%@ Register Src="~/usercontrols/destinationstationdialog.ascx" TagPrefix="uc" TagName="DestinationStationDialog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
        <script type="text/javascript" src="Scripts/zoom_control-trace_route.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManagerMap" runat="server">
        <Services>
            <asp:ServiceReference Path="~/Account/Supervisor/MapService.asmx" />
        </Services>
    </asp:ScriptManager>
    <uc:DestinationStationDialog runat="server" ID="ucDestinationStation" />
    <div id="map" style="width:800px; height:600px; clear:both"></div>
</asp:Content>

