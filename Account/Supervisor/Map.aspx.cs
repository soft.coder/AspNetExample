﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Supervisor_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            ChooseMapProvider();
        }
    }

    private void ChooseMapProvider()
    {
        if (Request.QueryString["map"] != null)
        {
            string mapProvider = Request.QueryString["map"];
            switch (mapProvider)
            {
                case "ymap":
                    LoadYandexMap();
                    break;
                case "gmap":
                    LoadGoogleMap();
                    break;
                case "osm":
                    LoadOpenStreetMap();
                    break;
                default:
                    LoadOpenStreetMap();
                    break;
            }
        }
        else
            LoadOpenStreetMap();
    }
    private void LoadScript(string url)
    {
        string scriptTag = String.Format("<script type=\"text/javascript\" src=\"{0}\" ></script>", url);
        this.Page.Header.Controls.Add(new LiteralControl { Text = scriptTag });
    }
    private void LoadCss(string url)
    {
        string scriptTag = String.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\"/>", url);
        this.Page.Header.Controls.Add(new LiteralControl { Text = scriptTag });
    }
    private void LoadOpenStreetMap()
    {
        LoadScript("http://www.openlayers.org/api/OpenLayers.js");
        LoadScript("Scripts/osm.js");
        LoadCss("Styles/osm.css");
    }

    private void LoadGoogleMap()
    {
        LoadScript("http://maps.googleapis.com/maps/api/js?key=AIzaSyCNvcSJ5S7R9kRx9-ZgtWz2rRcK2OKElUQ&sensor=false");
        LoadScript("Scripts/gmap.js");
    }

    private void LoadYandexMap()
    {
        LoadScript("http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU&mode=debug");
        LoadScript("Scripts/ymaps.js");
    }
}