﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using System.Configuration;

public partial class Account_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void LoginUser_LoggedIn(object sender, EventArgs e)
    {
        Login loginView = (Login)sender;
        TextBox txtUserName = (TextBox)loginView.FindControl("UserName");

        string[] userRole = Roles.GetRolesForUser(txtUserName.Text);

        if (userRole.Contains(RolesOfTraceTrack.Supervisor))
        {
            loginView.DestinationPageUrl = ConfigurationManager.AppSettings["DefaultSupervisorPage"];
        }
        else if (userRole.Contains(RolesOfTraceTrack.Manager))
        {
            loginView.DestinationPageUrl = ConfigurationManager.AppSettings["DefaultManagerPage"];
        }
        else if (userRole.Contains(RolesOfTraceTrack.Admin))
        {
            loginView.DestinationPageUrl = ConfigurationManager.AppSettings["DefaultAdminPage"];
        }        
    }
}
