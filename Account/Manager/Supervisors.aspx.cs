﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Account_Manager_Supervisors : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            this.ucSubordinationUsers.SuperiorUserId = this.GetUserIdOfCurrentUser();
    }
    protected Guid GetUserIdOfCurrentUser()
    {
        Guid supervisorUserId = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());
        return supervisorUserId;
    }
}