﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Account/Manager/Manager.master" AutoEventWireup="true"
    CodeFile="RegisterSupervisor.aspx.cs" Inherits="Account_Register" %>
<%@ Register Src="~/usercontrols/registeruser.ascx" TagPrefix="uc" TagName="RegisterUser" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <uc:RegisterUser runat="server" ID="ucRegisterUser" RoleForNewUser="Supervisor" 
                        GreetingUserCommonInfo="Информация о супервайзере"
                        GreetingCompany="Информация о компании"
                        ProfilePage="~/Account/Manager/ManagerProfile.aspx"
                        GreetingCreatingUser="Создание супервайзера"
                        CreateUserButtonText="Создать супервайзера"
                        GreetingCompleteStep="Регистрация супервайзера завершена" />
</asp:Content>