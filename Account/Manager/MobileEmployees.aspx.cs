﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;

public partial class Account_Manager_Supervisors : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        //if(!Page.IsPostBack)
            LoadSubordinateMobileEmployee();
    }
    private void LoadSubordinateMobileEmployee()
    {
        SubordinationDB sdb = new SubordinationDB();
        DataSet ds = sdb.GetSubordinationsUserInfo(this.GetUserIdOfCurrentUser());
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            Guid supervisorUserId = (Guid)ds.Tables[0].Rows[i]["UserId"];
            UserControls_SubordinationUsers ucMobileEmpOfSupervisor = (UserControls_SubordinationUsers)Page.LoadControl("~\\UserControls\\SubordinationUsers.ascx");
            ucMobileEmpOfSupervisor.SuperiorTitle = true;
            ucMobileEmpOfSupervisor.SuperiorUserId = supervisorUserId;
            this.panelOfMobileEmployee.Controls.Add(ucMobileEmpOfSupervisor);
        }
    }
    protected Guid GetUserIdOfCurrentUser()
    {
        Guid supervisorUserId = new Guid(Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString());
        return supervisorUserId;
    }
}