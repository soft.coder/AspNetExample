﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Account/Manager/Manager.master" AutoEventWireup="true"
    CodeFile="RegisterMobileEmployee.aspx.cs" Inherits="Account_Register" %>
<%@ Register Src="~/usercontrols/registeruser.ascx" TagPrefix="uc" TagName="RegisterUser" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <uc:RegisterUser runat="server" ID="ucRegisterUser" RoleForNewUser="MobileEmployee" 
                        GreetingUserCommonInfo="Информация о мобильном сотруднике"
                        GreetingCompany="Информация о компании"
                        ProfilePage="~/Account/Manager/ManagerProfile.aspx"
                        GreetingCreatingUser="Создание мобильного сотрудника"
                        CreateUserButtonText="Создать"
                        GreetingCompleteStep="Регистрация мобильного сотрудника завершена" />
</asp:Content>