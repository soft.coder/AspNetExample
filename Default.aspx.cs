﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (User.Identity.IsAuthenticated)
        {
            if (User.IsInRole(RolesOfTraceTrack.Supervisor))
            {
                Response.Redirect(ConfigurationManager.AppSettings["DefaultSupervisorPage"]);
            }
            else if (User.IsInRole(RolesOfTraceTrack.Manager))
            {
                Response.Redirect(ConfigurationManager.AppSettings["DefaultManagerPage"]);
            }
            else if (User.IsInRole(RolesOfTraceTrack.Admin))
            {
                Response.Redirect(ConfigurationManager.AppSettings["DefaultAdminPage"]);
            }
        }
    }
}
