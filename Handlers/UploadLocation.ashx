﻿<%@ WebHandler Language="C#" Class="uploadLocation" %>

using System;
using System.Web;

using System.Text;

using System.Web.Security;
using System.Collections.Generic;
using System.Linq;

public class uploadLocation : IHttpHandler {
    
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        context.Response.HeaderEncoding = Encoding.UTF8;

        string key;

        try
        {

            if (context.Application[AppStateTraceTrack.GuidOfUser] != null)
            {

                Dictionary<Guid, string> guidOfUsers = (Dictionary<Guid, string>)context.Application[AppStateTraceTrack.GuidOfUser];


                if (context.Request.QueryString["key"] != null)
                {
                    key = (string)context.Request.QueryString["key"];
                    context.Trace.Warn("key: ", key);
                    Guid keyGuid;

                    try
                    {
                        keyGuid = new Guid(key);
                    }
                    catch (Exception ex)
                    {
                        context.Trace.Warn("keyGuid parse error: ", ex.Message);
                        throw ex;
                    }

                    if (guidOfUsers.ContainsKey(keyGuid))
                    {
                        string username = guidOfUsers[keyGuid];
                        context.Trace.Warn("username: ", username);

                        Dictionary<string, List<TrackDetails>> UsersTracks;
                        if (context.Application[AppStateTraceTrack.UsersTracks] != null)
                            UsersTracks = (Dictionary<string, List<TrackDetails>>)context.Application[AppStateTraceTrack.UsersTracks];
                        else
                        {
                            UsersTracks = new Dictionary<string, List<TrackDetails>>();
                            context.Application[AppStateTraceTrack.UsersTracks] = UsersTracks;
                            context.Trace.Write("Create users tracks in app state");
                        }

                        List<TrackDetails> CurrUserTracks;


                        if (UsersTracks.ContainsKey(username) == true)
                        {
                            CurrUserTracks = UsersTracks[username];
                        }
                        else
                        {
                            CurrUserTracks = new List<TrackDetails>();
                            UsersTracks.Add(username, CurrUserTracks);
                            context.Trace.Write("Create tracks for user");
                        }
                        bool isStartNewTrack;

                        try
                        {
                            isStartNewTrack = ParseBooleanFromQueryString(context, "newtrack");
                        }
                        catch (Exception ex)
                        {
                            isStartNewTrack = false;
                        }
                        
                        if (isStartNewTrack)
                            CurrUserTracks.Add(CreateNewTrack(context, username));

                        try
                        {
                            LocationDetails location = new LocationDetails();                            
                            location = ParseLocationFromQueryString(context);

                            // SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.
                            // in long DateTime.Now.Ticks
                            // temp solution
                            location.Time = DateTime.Now;
                            // SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. 
                            
                            if (CurrUserTracks.Count == 0)
                                CurrUserTracks.Add(CreateNewTrack(context, username));
                            

                            CurrUserTracks.Last<TrackDetails>().Locations.Add(location);
                            LocationDB locDb = new LocationDB();
                            locDb.InsertLocation(CurrUserTracks.Last<TrackDetails>().TrackId, location);
                            
                            context.Trace.Write("Add new location");
                        }
                        catch (ArgumentException ex)
                        {
                            context.Trace.Warn(ex.Message);
                            context.Response.Write(ex.Message);
                            throw ex;
                        }
                        context.Response.Write("Success");
                    }
                    else
                        context.Response.Write("AccessDenied");
                }
            }
            else
                context.Response.Write("ReAuth");

        }
        catch (Exception ex)
        {
            context.Trace.Warn("unexpexted error", ex.Message);
            throw;
        }
    }
        
    private static bool ParseBooleanFromQueryString(HttpContext context, string paramName)
    {
        bool typeBool;
        if (context.Request.QueryString[paramName] != null)
        {

            if (bool.TryParse(context.Request.QueryString[paramName], out typeBool) == false)
                throw new ApplicationException("cant parse boolean type from query string paramName: " + paramName);
            else
                return typeBool;
        }
        else
            throw new ApplicationException("paramName: " + paramName + " cant find in query string");
    }

    private static TrackDetails CreateNewTrack(HttpContext context, string username)
    {
        Guid UserId = new Guid(Membership.GetUser(username).ProviderUserKey.ToString());
        UserInfoCommonDB userDb = new UserInfoCommonDB();
        int? userInfoId = userDb.GetUserInfoCommonId(UserId);
        if (userInfoId.HasValue)
        {
            TrackDB trackDb = new TrackDB();
            long? trackId = trackDb.InsertTrack(userInfoId.Value);
            if (trackId.HasValue)
            {
                TrackDetails trackNew =  new TrackDetails(trackId.Value, username);
                context.Trace.Write("Start new Track");
                return trackNew;
            }
        }
        else
            throw new ApplicationException("cant find for username: " + username + " userId: " + UserId + " associated record in userInfoCommon table");
        throw new ApplicationException("unexpected error when create new track");
    }

    private static LocationDetails ParseLocationFromQueryString(HttpContext context)
    {
        LocationDetails location = new LocationDetails();
        
        DateTime? time = ParseDateTimeQueryString(context, "time");
        if (time.HasValue != null)
            location.Time = time.Value;
        else
            throw new ArgumentException("error: time cant parse or construct or find");

        double? longitude = ParseDoubleQueryString(context, "longitude");
        if (longitude.HasValue)
            location.Longitude = longitude.Value;
        else
            throw new ArgumentException("error: longitude can't parsed, or finded");

        double? latitude = ParseDoubleQueryString(context, "latitude");
        if (latitude.HasValue)
            location.Latitude = latitude.Value;
        else
            throw new ArgumentException("error: latitude can't parsed, or find");

        location.Accuracy = ParseFloatQueryString(context, "accuracy");

        location.Provider = (string)context.Request.QueryString["provider"];
        context.Trace.Warn("provider:", location.Provider.ToString());

        location.Bearing = ParseFloatQueryString(context, "bearing");

        location.Speed = ParseFloatQueryString(context, "speed");

        location.Distance = ParseFloatQueryString(context, "distance");

        return location;
    }
    private static DateTime? ParseDateTimeQueryString(HttpContext context, string parameterName)
    {
        if (context.Request.QueryString[parameterName] != null)
        {
            long timeMs;
            if (long.TryParse((string)context.Request.QueryString[parameterName], out timeMs) == false)
            {
                context.Trace.Warn(parameterName, "can't parsed to long");
                return null;
            }
            else
            {
                try
                {
                    DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                    DateTime Time = new DateTime(timeMs * 10000 + startTime.Ticks);
                    
                    
                    context.Trace.Write(parameterName, Time.ToString());
                    return Time;
                }
                catch (Exception ex)
                {
                    context.Trace.Warn(parameterName + " = new DateTime(timeMs);", ex.Message);
                    return null;

                }
            }
        }
        else
            return null;
    }
    private static double? ParseDoubleQueryString(HttpContext context, string parameterName)
    {
        if (context.Request.QueryString[parameterName] != null)
        {
            double doubleValue;
            string strLatitude = (string)context.Request.QueryString[parameterName];
            strLatitude = strLatitude.Replace(".", ",");
            if (double.TryParse(strLatitude, out doubleValue) == false)
            {
                context.Trace.Warn(parameterName, "can't parsed");
                return null;
            }
            else
            {
                context.Trace.Write("latitude:", doubleValue.ToString());
                return doubleValue;
            }
        }
        else
            return null;
    }

    private static float? ParseFloatQueryString(HttpContext context, string parameterName)
    {

        if (context.Request.QueryString[parameterName] != null)
        {
            float floatValue;
            string strFloatValue = (string)context.Request.QueryString[parameterName];
            strFloatValue = strFloatValue.Replace(".", ",");
            if (float.TryParse(strFloatValue, out floatValue) == false)
            {
                context.Trace.Warn(parameterName, "can't parsed");
                return null;
            }
            else
            {
                context.Trace.Write(parameterName, floatValue.ToString());
                return floatValue;
            }
        }
        else
            return null;
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}