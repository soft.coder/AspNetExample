﻿<%@ WebHandler Language="C#" Class="GetImageUploadPreviewHandler" %>

using System;
using System.Web;
using System.Web.SessionState;

public class GetImageUploadPreviewHandler : IHttpHandler , IRequiresSessionState {
    
    public void ProcessRequest (HttpContext context)  {
        context.Response.ContentType = (string)context.Session["ImageForUploadMimeType"];
        byte[] img = (byte[])context.Session["ImageForUpload"];
        context.Response.BinaryWrite(img);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}