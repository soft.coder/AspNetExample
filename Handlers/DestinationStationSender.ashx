﻿<%@ WebHandler Language="C#" Class="DestinationStationSender" %>

using System;
using System.Web;

using System.Text;

using System.Web.Security;
using System.Collections.Generic;
using System.Linq;

public class DestinationStationSender : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        context.Response.HeaderEncoding = Encoding.UTF8;
        string key;

        try
        {

            if (context.Application[AppStateTraceTrack.GuidOfUser] != null)
            {

                Dictionary<Guid, string> guidOfUsers = (Dictionary<Guid, string>)context.Application[AppStateTraceTrack.GuidOfUser];


                if (context.Request.QueryString["key"] != null)
                {
                    key = (string)context.Request.QueryString["key"];
                    context.Trace.Warn("key: ", key);
                    Guid keyGuid;

                    try
                    {
                        keyGuid = new Guid(key);
                    }
                    catch (Exception ex)
                    {
                        context.Trace.Warn("keyGuid parse error: ", ex.Message);
                        throw ex;
                    }

                    if (guidOfUsers.ContainsKey(keyGuid))
                    {
                        string username = guidOfUsers[keyGuid];
                        context.Trace.Warn("username: ", username);

                        MembershipUser user = Membership.GetUser(username);

                        if (context.Request.QueryString["idcomplete"] == null)
                        {
                            GetDestinationStationOfMobileEmployee(context, user);
                        }
                        else
                        {
                            CompleteDestinationStation(context);
                        }    
                        
                            
                    }
                    else
                        context.Response.Write("AccessDenied");
                }
            }
            else
                context.Response.Write("ReAuth");

        }
        catch (Exception ex)
        {
            context.Trace.Warn("unexpexted error", ex.Message);
            throw;
        }
    }

    private static void CompleteDestinationStation(HttpContext context)
    {
        try
        {
            long idcomplete = long.Parse(context.Request.QueryString["idcomplete"]);

            DestinationStationDB dsDb = new DestinationStationDB();
            dsDb.CompleteDestinationStation(idcomplete);

        }
        catch (ArgumentException ex)
        {
            context.Trace.Warn(ex.Message);
            context.Response.Write(ex.Message);
            throw;
        }
    }

    private void GetDestinationStationOfMobileEmployee(HttpContext context, MembershipUser user)
    {
        try
        {            
            DestinationStationDB dsDb = new DestinationStationDB();
            List<DestinationStationDetails> lstDs = dsDb.GetDestinationStationsOfMobileEmployee((Guid)user.ProviderUserKey);
            string strJsonDs = DestinationStationToJSON(lstDs);
            context.Response.Write(strJsonDs);

            context.Trace.Write("Get List Of DestinationStation");
        }
        catch (ArgumentException ex)
        {
            context.Trace.Warn(ex.Message);
            context.Response.Write(ex.Message);
            throw ex;
        }
    }

    private string DestinationStationToJSON(List<DestinationStationDetails> lstDs)
    {
        if (lstDs.Count == 0)
            return "";
        StringBuilder strRepsonse = new StringBuilder();
        strRepsonse.AppendLine("[");
        for (int i = 0; i < lstDs.Count; i++)
        {
            DestinationStationDetails ds = lstDs[i];
            strRepsonse.AppendLine("\t{");
            strRepsonse.AppendLine(string.Format("\t\tID:{0},", ds.ID));
            strRepsonse.AppendLine(string.Format("\t\tUserInfoCommonId:{0},", ds.UserInfoCommonId));
            strRepsonse.AppendLine(string.Format("\t\tLongitude:{0},", ds.Longitude.ToString().Replace(",", ".")));
            strRepsonse.AppendLine(string.Format("\t\tLatitude:{0},", ds.Latitude.ToString().Replace(",", ".")));
            strRepsonse.AppendLine(string.Format("\t\tTitle:\"{0}\",", ds.Title));
            strRepsonse.AppendLine(string.Format("\t\tTaskDescription:\"{0}\",", ds.TaskDescription));
            strRepsonse.AppendLine(string.Format("\t\tTimeAssign:{0},", DateToJsonObjString(ds.TimeAssign)));
            strRepsonse.AppendLine(string.Format("\t\tTimeComplete:{0}", DateToJsonObjString(ds.TimeComplete)));

            if (i + 1 < lstDs.Count)
                strRepsonse.AppendLine("\t},");
            else
                strRepsonse.AppendLine("\t}");
        }
        strRepsonse.AppendLine("]");
        return strRepsonse.ToString();
    }
    private string DateToJsonObjString(DateTime? date)
    {
        string strDate;
        if (date.HasValue)
        {
            strDate = "{" + string.Format("year:{0},month:{1},day:{2},hour:{3},minute:{4},second:{5}", new object[]{
                date.Value.Year,
                date.Value.Month,
                date.Value.Day,
                date.Value.Hour,
                date.Value.Minute,
                date.Value.Second
            }) + "}";
        }
        else
            strDate = "null";
        return strDate;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}