﻿<%@ WebHandler Language="C#" Class="Authentication" %>

using System;
using System.Web;

using System.Web.Security;

using System.Collections.Generic;
using System.Linq;

public class Authentication : IHttpHandler {

    private string responseNameGuidField = "newUserGuid:";
    
    public void ProcessRequest (HttpContext context) {
        
        context.Response.ContentType = "text/plain";

        try
        {

            if (context.Request.QueryString["username"] != null && context.Request.QueryString["password"] != null)
            {
                string username = context.Request.QueryString["username"];
                string password = context.Request.QueryString["password"];
                if (Membership.ValidateUser(username, password) == true && Roles.IsUserInRole(username, "MobileEmployee"))
                {
                    Guid newUserGuid = Guid.NewGuid();
                    if (context.Application["GuidOfUser"] == null)
                    {
                        Dictionary<Guid, string> guidsOfUsers = new Dictionary<Guid, string>();
                        guidsOfUsers.Add(newUserGuid, username);
                        context.Application["GuidOfUser"] = guidsOfUsers;
                    }
                    else
                    {
                        Dictionary<Guid, string> guidsOfUsers = (Dictionary<Guid, string>)context.Application["GuidOfUser"];
                        RemoveOldGuid(context, username, guidsOfUsers);
                        guidsOfUsers.Add(newUserGuid, username);
                        context.Application["GuidOfUser"] = guidsOfUsers;
                    }
                    context.Trace.Write("Authentication", "username=" + username + " password=" + password + "newGuid=" + newUserGuid.ToString());
                    
                    context.Response.Write(responseNameGuidField + newUserGuid.ToString());
                }
                else
                    context.Response.Write("AccessDenied");
            }
        }
        catch (Exception ex)
        {
            context.Trace.Warn("unexpected error", ex.Message);
            throw;
        }

        
    }

    private static bool RemoveOldGuid(HttpContext context, string username, Dictionary<Guid, string> guidsOfUsers)
    {
        if (guidsOfUsers.ContainsValue(username))
        {
            Guid? oldGuid = null;
            foreach (KeyValuePair<Guid, string> item in guidsOfUsers)
            {
                if (item.Value == username)
                {
                    oldGuid = item.Key;
                }
            }
            if (oldGuid.HasValue)
            {
                guidsOfUsers.Remove(oldGuid.Value);
                context.Trace.Write("removeOldGuid:", oldGuid.Value.ToString());
                if (guidsOfUsers.ContainsValue(username))
                    RemoveOldGuid(context, username, guidsOfUsers);
                return true;
            }
        }
        return false;
    }
    
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}