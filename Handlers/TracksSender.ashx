﻿<%@ WebHandler Language="C#" Class="TracksSender" %>

using System;
using System.Web;

using System.Text;
using System.Web.Security;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.IO;

public class TracksSender : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        context.Response.HeaderEncoding = Encoding.UTF8;

        string key;

        try
        {

            if (context.Application[AppStateTraceTrack.GuidOfUser] != null)
            {

                Dictionary<Guid, string> guidOfUsers = (Dictionary<Guid, string>)context.Application[AppStateTraceTrack.GuidOfUser];


                if (context.Request.QueryString["key"] != null)
                {
                    key = (string)context.Request.QueryString["key"];
                    context.Trace.Warn("key: ", key);
                    Guid keyGuid;

                    try
                    {
                        keyGuid = new Guid(key);
                    }
                    catch (Exception ex)
                    {
                        context.Trace.Warn("keyGuid parse error: ", ex.Message);
                        throw ex;
                    }

                    if (guidOfUsers.ContainsKey(keyGuid))
                    {
                        string username = guidOfUsers[keyGuid];
                        context.Trace.Warn("username: ", username);

                        MembershipUser user = Membership.GetUser(username);

                        Dictionary<string, TrackDetails> LastTrackOfUsers;
                        if (context.Application[AppStateTraceTrack.LastTrackOfUsers] != null)
                            LastTrackOfUsers = (Dictionary<string, TrackDetails>)context.Application[AppStateTraceTrack.LastTrackOfUsers];
                        else
                        {
                            LastTrackOfUsers = new Dictionary<string, TrackDetails>();
                            context.Application[AppStateTraceTrack.LastTrackOfUsers] = LastTrackOfUsers;
                            context.Trace.Write("Create last track of users in app state");
                        }
                        
                        bool isStartNewTrack;
                        try
                        {
                            isStartNewTrack = bool.Parse(context.Request.QueryString["newtrack"]);
                        }
                        catch (Exception ex)
                        {
                            isStartNewTrack = false;
                        }

                        TrackDetails CurrUserLastTrack;
                        if (isStartNewTrack == true)
                        {
                            CurrUserLastTrack = CreateNewTrack(context, username);
                            if (LastTrackOfUsers.ContainsKey(username))
                                LastTrackOfUsers[username] = CurrUserLastTrack;
                            else
                                LastTrackOfUsers.Add(username, CurrUserLastTrack);                            
                            context.Trace.Write("Create last track of current user");
                        }
                        else if (LastTrackOfUsers.ContainsKey(username) == true)
                        {
                            CurrUserLastTrack = LastTrackOfUsers[username];
                        }
                        else
                        {
                            CurrUserLastTrack = GetLastTrackOfUser(context, username);
                            LastTrackOfUsers.Add(username, CurrUserLastTrack);
                        }
                        try
                        {
                            List<List<LocationDetails>> lstTracksIncoming = ParseTracksFromJson(context);                            
                            LocationDB locDb = new LocationDB();
                            for (int i = 0; i < lstTracksIncoming.Count; i++)
                            {
                                List<LocationDetails> track = lstTracksIncoming[i];
                                if (i > 0)
                                {
                                    CurrUserLastTrack = CreateNewTrack(context, username);
                                    LastTrackOfUsers[username] = CurrUserLastTrack;
                                }
                                
                                for (int j = 0; j < track.Count; j++)
                                {
                                    CurrUserLastTrack.Locations.Add(track[j]);
                                    locDb.InsertLocation(CurrUserLastTrack.TrackId, track[j]);
                                }
                            }
                            context.Trace.Write("Add new locations of tracks");
                        }
                        catch (ArgumentException ex)
                        {
                            context.Trace.Warn(ex.Message);
                            context.Response.Write(ex.Message);
                            throw ex;
                        }
                    }
                    else
                        context.Response.Write("AccessDenied");
                }
            }
            else
                context.Response.Write("ReAuth");

        }
        catch (Exception ex)
        {
            context.Trace.Warn("unexpexted error", ex.Message);
            throw;
        }
    }

    private static List<List<LocationDetails>> ParseTracksFromJson(HttpContext context)
    {        
        Stream bodyStream = context.Request.InputStream;
        byte[] bodyByteArray = new byte[context.Request.InputStream.Length];
        bodyStream.Read(bodyByteArray, 0, (int)bodyStream.Length);
        String strBody = Encoding.ASCII.GetString(bodyByteArray);        
        strBody = strBody.Replace("\\\\\\", "\\");        
        
        JavaScriptSerializer jss = new JavaScriptSerializer();
        List<List<LocationDetails>> tracks = jss.Deserialize<List<List<LocationDetails>>>(strBody);
        return tracks;
    }
    private static TrackDetails CreateNewTrack(HttpContext context, string username)
    {
        Guid UserId = new Guid(Membership.GetUser(username).ProviderUserKey.ToString());
        UserInfoCommonDB userDb = new UserInfoCommonDB();
        int? userInfoId = userDb.GetUserInfoCommonId(UserId);
        if (userInfoId.HasValue)
        {
            TrackDB trackDb = new TrackDB();
            long? trackId = trackDb.InsertTrack(userInfoId.Value);
            if (trackId.HasValue)
            {
                TrackDetails trackNew = new TrackDetails(trackId.Value, username);
                context.Trace.Write("Start new Track");
                return trackNew;
            }
        }
        else
            throw new ApplicationException("cant find for username: " + username + " userId: " + UserId + " associated record in userInfoCommon table");
        throw new ApplicationException("unexpected error when create new track");
    }


    private static TrackDetails GetLastTrackOfUser(HttpContext context, string username)
    {
        Guid UserId = new Guid(Membership.GetUser(username).ProviderUserKey.ToString());
        TrackDB trackDb = new TrackDB();
        return trackDb.GetLastTrackOfUserByUserId(UserId);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}