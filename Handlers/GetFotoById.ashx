﻿<%@ WebHandler Language="C#" Class="GetFotoById" %>

using System;
using System.Web;

using System.IO;
using System.Web.SessionState;

public class GetFotoById : IHttpHandler, IRequiresSessionState {

    
    public void ProcessRequest (HttpContext context) {
        
        

        try
        {
            int userInfoCommonId = int.Parse(context.Server.HtmlDecode(context.Request.QueryString["id"]));
            UserInfoCommonDB udb = new UserInfoCommonDB();
            FotoDetails fotoData = udb.GetUserFoto(userInfoCommonId);

            if (fotoData.Foto == null && fotoData.FotoMimeType == null)
            {
                if (context.Session["noFoto"] == null)
                {
                    fotoData.FotoMimeType = "image/png";
                    fotoData.Foto = File.ReadAllBytes(context.Server.MapPath("..\\Img\\user_anonymous.png"));
                    context.Session["noFoto"] = fotoData;
                }
                else
                {
                    fotoData = (FotoDetails)context.Session["noFoto"];
                }
            }            
            context.Response.ContentType = fotoData.FotoMimeType;
            context.Response.BinaryWrite(fotoData.Foto);
            
        }
        catch (Exception ex)
        {            
            throw ex;
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}