﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="loginWrap">
    <h2>
        TraceTrack
    </h2>
    <p style="text-align:center">
        Диспетчерская навигационная система
    </p>
    <p style="text-align:center">
    <asp:LoginView ID="DefalutPageLoginView" runat="server" EnableViewState="false">
        <AnonymousTemplate>
            [ <a href="~/Account/Login.aspx" ID="HeadLoginStatus" runat="server" tabindex="1">Войти</a> ]
        </AnonymousTemplate>
        <LoggedInTemplate>
            Добро пожаловать <span class="bold"><asp:LoginName ID="HeadLoginName" runat="server" /></span>!
            [ <asp:LoginStatus ID="HeadLoginStatus" runat="server" LogoutAction="Redirect" LogoutText="Выйти" LogoutPageUrl="~/"/> ]
        </LoggedInTemplate>
    </asp:LoginView>
    </p>
</div>
</asp:Content>
