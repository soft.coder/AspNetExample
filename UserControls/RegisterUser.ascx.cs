﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Web.Security;


public partial class UserControls_RegisterUser : System.Web.UI.UserControl
{
    #region InnerEnum
    public enum CompanyAlternative
    {
        NoInfo,
        Create,
        Choose
    }
    #endregion

    #region publicProp
    private string greetingUserCommonInfo = "Информация о пользователе";
    public string GreetingUserCommonInfo
    {
        get { return greetingUserCommonInfo; }
        set { greetingUserCommonInfo = value; }
    }
    private string greetingCompany = "Информация о компании";
    public string GreetingCompany
    {
        get { return greetingCompany; }
        set { greetingCompany = value; }
    }
    private string greetingCreatingUser = "Создание пользователя";
    public string GreetingCreatingUser
    {
        get { return greetingCreatingUser; }
        set { greetingCreatingUser = value; }
    }
    private string greetingCompleteStep = "Новый пользователь зарегистрирован";
    public string GreetingCompleteStep
    {
        get { return greetingCompleteStep; }
        set { greetingCompleteStep = value; }
    }
    private string createUserButtonText = "Создать пользователя";
    public string CreateUserButtonText
    {
        get { return createUserButtonText; }
        set { createUserButtonText = value; }
    }
    public string CompleteSuccessText
    {
        get { return RegisterUser.CompleteSuccessText; }
        set { RegisterUser.CompleteSuccessText = value; }
    }
    private string roleForNewUser;
    public string RoleForNewUser
    {
        get { return roleForNewUser; }
        set { roleForNewUser = value; }
    }
    private string profilePage = "/Account/Admin/AdminProfile.aspx";
    public string ProfilePage
    {
        get { return profilePage; }
        set { profilePage = value; }
    }
    #endregion publicProp


    #region protectedProp
    protected UserControls_SuperiorChoose superiorChoose
    {
        get
        {
            return (UserControls_SuperiorChoose)this.wsSubordination.ContentTemplateContainer.FindControl("ucSuperiorChoose");
        }
    }
    protected UserControls_UserInfoCreate userCommonInfo
    {
        get
        {
            return (UserControls_UserInfoCreate)this.wsUserAddInfo.ContentTemplateContainer.FindControl("ucUserInfoCreate");
        }
    }
    protected Address userAddress
    {
        get
        {
            return (Address)this.wsUserAddress.ContentTemplateContainer.FindControl("ucAddressUser");
        }
    }
    protected UserControls_ContactPhoneInit userPhones
    {
        get
        {
            return (UserControls_ContactPhoneInit)this.wsUserPhones.ContentTemplateContainer.FindControl("ucContactPhoneUser");
        }
    }
    protected UserControls_ImageLoader userPhoto
    {
        get
        {
            return (UserControls_ImageLoader)this.wsUserPhoto.ContentTemplateContainer.FindControl("ucImageLoader");
        }
    }
    protected UserControls_CompanyChoose companyChoose
    {
        get
        {
            return (UserControls_CompanyChoose)this.wsCompanyChoose.ContentTemplateContainer.FindControl("ucCompanyChoose");
        }
    }
    protected UserControls_CompanyCreate companyCommonInfo
    {
        get
        {
            return (UserControls_CompanyCreate)this.wsCompanyInfo.ContentTemplateContainer.FindControl("ucCompanyCreate");
        }
    }
    protected Address companyAddress
    {
        get
        {
            return (Address)this.wsCompanyAddress.ContentTemplateContainer.FindControl("ucAddressCompany");
        }
    }
    protected UserControls_ContactPhoneInit companyContactPhones
    {
        get
        {
            return (UserControls_ContactPhoneInit)this.wsCompanyPhones.ContentTemplateContainer.FindControl("ucCompanyContactPhone");
        }
    }
    protected CompanyAlternative AltCompanyInfo
    {
        get
        {
            RadioButtonList rdb = (RadioButtonList)this.wsCompanyAlternative.ContentTemplateContainer.FindControl("rdbListCompanyAlternative");
            if (rdb.SelectedValue == "companyNo")
                return CompanyAlternative.NoInfo;
            else if (rdb.SelectedValue == "companyChoose")
                return CompanyAlternative.Choose;
            else if (rdb.SelectedValue == "companyCreate")
                return CompanyAlternative.Create;
            else
                throw new ArgumentException("unexcepted data");
        }
    }
    protected MembershipUser RegisteredUser
    {
        get
        {
            return Membership.GetUser(RegisterUser.UserName);
        }
    }
    protected Uri PrevPage
    {
        get { return (Uri)ViewState["PreviousPage"]; }
        set { ViewState["PreviousPage"] = value; }
    }
    #endregion protectedProp



    protected void Page_Load(object sender, EventArgs e)
    {
        IntiGretting();

        if (!Page.IsPostBack)
        {
            InitDestPage();

            InitSubordinationStep();
        }
    }



    #region Init

    private void InitDestPage()
    {
        if (Request.UrlReferrer != null)
        {
            this.PrevPage = Request.UrlReferrer;
            this.RegisterUser.CancelDestinationPageUrl = this.PrevPage.AbsoluteUri;
            this.RegisterUser.ContinueDestinationPageUrl = this.PrevPage.AbsoluteUri;
        }
        else
        {
            RegisterUser.CancelDestinationPageUrl = this.ProfilePage;
            RegisterUser.ContinueDestinationPageUrl = this.ProfilePage;
        }
    }

    private void IntiGretting()
    {
        foreach (WizardStepBase wsb in RegisterUser.WizardSteps)
        {
            if (wsb is TemplatedWizardStep)
            {
                TemplatedWizardStep tws = (TemplatedWizardStep)wsb;

                HtmlGenericControl hUserInfoCommon = (HtmlGenericControl)tws.ContentTemplateContainer.FindControl("hGreetingUserInfoCommon");
                if (hUserInfoCommon != null)
                    hUserInfoCommon.InnerText = this.GreetingUserCommonInfo;

                HtmlGenericControl hCompany = (HtmlGenericControl)tws.ContentTemplateContainer.FindControl("hGreetingCompany");
                if (hCompany != null)
                    hCompany.InnerText = this.GreetingCompany;

                HtmlGenericControl hCreatingUser = (HtmlGenericControl)tws.ContentTemplateContainer.FindControl("hGreetingCreatingUser");
                if (hCreatingUser != null)
                    hCreatingUser.InnerText = this.GreetingCreatingUser;

                Button CreateUserButton = (Button)tws.ContentTemplateContainer.FindControl("CreateUserButton");
                if (CreateUserButton != null)
                    CreateUserButton.Text = this.createUserButtonText;
            }
            if (wsb is CompleteWizardStep)
            {
                CompleteWizardStep cws = (CompleteWizardStep)wsb;
                HtmlGenericControl hGrettingCompleteCreateUser = (HtmlGenericControl)cws.ContentTemplateContainer.FindControl("hGrettingCompleteCreateUser");
                if (hGrettingCompleteCreateUser != null)
                    hGrettingCompleteCreateUser.InnerText = this.GreetingCompleteStep;
            }
        }
    }

    private void InitSubordinationStep()
    {
        Button btnPrevOfWsUserAddInfo = (Button)this.wsUserAddInfo.ContentTemplateContainer.FindControl("btnPrev");
        if (this.RoleForNewUser == RolesOfTraceTrack.MobileEmployee)
        {
            RegisterUser.ActiveStepIndex = 0;
            btnPrevOfWsUserAddInfo.Visible = true;
        }
        else
        {
            RegisterUser.ActiveStepIndex = 1;
            btnPrevOfWsUserAddInfo.Visible = false;
        }
    }

    #endregion Init

    protected void RegisterUser_ActiveStepChanged(object sender, WizardNavigationEventArgs e)
    {
        if (Page.IsValid == false)
            e.Cancel = true;
    }


    #region CreatedNewUser

    protected void RegisterUser_CreatedUser(object sender, EventArgs e)
    {
        //FormsAuthentication.SetAuthCookie(RegisterUser.UserName, false /* createPersistentCookie */);

        //string continueUrl = RegisterUser.ContinueDestinationPageUrl;
        //if (String.IsNullOrEmpty(continueUrl))
        //{
        //    continueUrl = "~/";
        //}
        //Response.Redirect(continueUrl);

        Roles.AddUserToRole(RegisterUser.UserName, this.RoleForNewUser);

        SaveAddtionInfo();
    }

    protected void RegisterUser_SendingMail(object sender, MailMessageEventArgs e)
    {
        e.Message.Body = e.Message.Body.Replace("&lt;%UserName%&gt;", RegisterUser.UserName);
        e.Message.Body = e.Message.Body.Replace("&lt;%Password%&gt;", RegisterUser.Password);
        e.Message.Body = e.Message.Body.Replace("&lt;%Email%&gt;", RegisterUser.Email);
        Uri contextUri = HttpContext.Current.Request.Url;
        string curPort = contextUri.Port == 80 ? string.Empty : ":" + contextUri.Port.ToString();
        string absoluteLoginUrl = string.Format("{0}://{1}{2}{3}", new string[] { contextUri.Scheme, contextUri.Host, 
                                                                                curPort , FormsAuthentication.LoginUrl });
        e.Message.Body = e.Message.Body.Replace("&lt;%LoginUrl%&gt;", absoluteLoginUrl);
        e.Message.Body = e.Message.Body.Replace("&lt;%LoginUrl%&gt;", absoluteLoginUrl);
    }
    
    private void SaveAddtionInfo()
    {
        if (this.userCommonInfo != null)
        {
            try
            {
                UserInfoCommonDB uiDb = new UserInfoCommonDB();
                Guid userId = new Guid(this.RegisteredUser.ProviderUserKey.ToString());
                if (userId != null)
                {
                    switch (this.AltCompanyInfo)
                    {
                        case CompanyAlternative.NoInfo:
                            CreateUserWithoutCompany(uiDb, userId);
                            break;
                        case CompanyAlternative.Create:
                            CreateUserWithNewCompany(uiDb, userId);
                            break;
                        case CompanyAlternative.Choose:
                            CreateUserWithChoosedCompany(uiDb, userId);
                            break;
                        default:
                            throw new ArgumentException("undefiend CompanyAlternative");
                    }
                }
                else
                    throw new ArgumentException("userId of registered user is empty string");
            }
            catch (Exception ex)
            {
                Membership.DeleteUser(RegisteredUser.UserName);
                throw ex;
            }
        }
    }


    private void CreateUserWithNewCompany(UserInfoCommonDB uiDb, Guid userId)
    {
        CompanyDB comDb = new CompanyDB();
        if (this.companyCommonInfo.CompanyName != string.Empty)
        {
            int? insertedCompanyId = comDb.InsertCompany(this.companyCommonInfo.CompanyName,
                this.companyCommonInfo.ContactPerson,
                this.companyCommonInfo.CompanyEmail,
                this.companyCommonInfo.CompanyWebSite,
                this.companyAddress.TownIdSelected,
                this.companyAddress.AddressInfo);

            if (insertedCompanyId.HasValue)
            {
                int? insertedUserInfoId = CreateUserInfoCommon(uiDb, userId, insertedCompanyId.Value);

                CreateSubordination(uiDb, insertedUserInfoId);

                if (insertedUserInfoId.HasValue)
                    CreateUserPhones(insertedUserInfoId.Value);

                CreateCompanyPhones(insertedCompanyId.Value);
            }
            else
                throw new ArgumentException("enum companyAlt is company create, company created db method return null of created companyId");
        }
    }



    private void CreateUserWithChoosedCompany(UserInfoCommonDB uiDb, Guid userId)
    {
        if (this.companyChoose.SelectedCompanyId.HasValue)
        {
            int? insertedUserInfoId = CreateUserInfoCommon(uiDb, userId, this.companyChoose.SelectedCompanyId.Value);

            CreateSubordination(uiDb, insertedUserInfoId);

            if (insertedUserInfoId.HasValue)
                CreateUserPhones(insertedUserInfoId.Value);
            
            CreateCompanyPhones(this.companyChoose.SelectedCompanyId.Value);
        }
        else
            throw new ArgumentException("enum companyAlt is company choose, but companyChoose uc has no value of selected company id");
    }

    private void CreateUserWithoutCompany(UserInfoCommonDB uiDb, Guid userId)
    {
        int? insertedUserInfoId = CreateUserInfoCommon(uiDb, userId, null);

        CreateSubordination(uiDb, insertedUserInfoId);

        if (insertedUserInfoId.HasValue)
            CreateUserPhones(insertedUserInfoId.Value);
    }

    private List<long?> CreateUserPhones(int insertedUserInfoId)
    {
        if (this.userPhones.ContactPhones != null)
        {
            PhoneDB phoneDb = new PhoneDB();
            List<long?> insetedPhoneIds = new List<long?>();
            foreach (PhoneDetails phone in this.userPhones.ContactPhones)
            {
                if (phone.PhoneNumber == string.Empty)
                    throw new ArgumentException("PhoneNumber empty string");
            
                long? phoneId = phoneDb.InsertPhoneOfUser(insertedUserInfoId,
                    phone.PhoneDescription,
                    phone.PhoneNumber,
                    phone.TypePhoneId);
            
                insetedPhoneIds.Add(phoneId);
            }
            return insetedPhoneIds;
        }
        return null;
    }
    private List<long?> CreateCompanyPhones(int companyId)
    {
        if (this.companyContactPhones.ContactPhones != null)
        {
            PhoneDB phoneDb = new PhoneDB();
            List<long?> insetedPhoneIds = new List<long?>();
            foreach (PhoneDetails phone in this.companyContactPhones.ContactPhones)
            {
                if (phone.PhoneNumber == string.Empty)
                    throw new ArgumentException("PhoneNumber empty string");

                long? phoneId = phoneDb.InsertPhoneOfCompany(companyId,
                    phone.PhoneDescription,
                    phone.PhoneNumber,
                    phone.TypePhoneId);

                insetedPhoneIds.Add(phoneId);
            }
            return insetedPhoneIds;
        }
        return null;
    }
    private int? CreateUserInfoCommon(UserInfoCommonDB uiDb, Guid userId, int? companyId)
    {
        int? insertedUserInfoId = uiDb.InsertUserInfo(userId,
            companyId,
            this.userCommonInfo.FirstName,
            this.userCommonInfo.LastName,
            this.userCommonInfo.Patronymic,
            this.userAddress.TownIdSelected,
            this.userAddress.AddressInfo,
            this.userCommonInfo.Sex,
            this.userCommonInfo.DateOfBirth,
            this.userPhoto.Image,
            this.userPhoto.ImageMimeType,
            this.userCommonInfo.WebSite);
        return insertedUserInfoId;
    }

    private void CreateSubordination(UserInfoCommonDB uiDb, int? insertedUserInfoId)
    {
        SubordinationDB sDb = new SubordinationDB();
        if (this.RoleForNewUser == RolesOfTraceTrack.Supervisor)
        {
            Guid currManagerUserId = new Guid(Membership.GetUser(Page.User.Identity.Name).ProviderUserKey.ToString());
            int? currManagerUserInfoCommonId = uiDb.GetUserInfoCommonId(currManagerUserId);
            if (currManagerUserInfoCommonId.HasValue && insertedUserInfoId.HasValue)
                sDb.InsertSubordination(currManagerUserInfoCommonId.Value, insertedUserInfoId.Value);
        }
        else if (this.RoleForNewUser == RolesOfTraceTrack.MobileEmployee)
        {
            if (insertedUserInfoId.HasValue && this.superiorChoose.SelectedUserInfoCommonId.HasValue)
                sDb.InsertSubordination(this.superiorChoose.SelectedUserInfoCommonId.Value, insertedUserInfoId.Value);
        }
    }

    #endregion CreatedNewUser
    

    #region CompanyChoose

    protected void RegisterUser_wsCompanyAlternative_OnActivate(object sender, EventArgs e)
    {
    }
    
    protected void RegisterUser_wsCompanyAlternative_btnNext_OnClick(object sender, EventArgs e)
    {
        int stepIndx;
        switch (this.AltCompanyInfo)
        {
            case CompanyAlternative.NoInfo:
                stepIndx = RegisterUser.WizardSteps.IndexOf(this.RegisterUserWizardStep) - 1;
                RegisterUser.ActiveStepIndex = stepIndx;
                break;
            case CompanyAlternative.Create:
                stepIndx = RegisterUser.WizardSteps.IndexOf(this.wsCompanyInfo) - 1;
                RegisterUser.ActiveStepIndex = stepIndx;
                break;
            case CompanyAlternative.Choose:
                stepIndx = RegisterUser.WizardSteps.IndexOf(this.wsCompanyChoose) - 1;
                RegisterUser.ActiveStepIndex = stepIndx;
                break;
        }
    }

    protected void RegisterUser_wsCompanyChoose_btnNext_OnClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int stepIndx;
            stepIndx = RegisterUser.WizardSteps.IndexOf(this.RegisterUserWizardStep) - 1;
            RegisterUser.ActiveStepIndex = stepIndx;
        }
    }
    
    protected void CustomValidatorCompanyChoose_OnServerValidate(object sender, ServerValidateEventArgs e)
    {
        if (this.companyChoose.SelectedCompanyId == null)
            e.IsValid = false;
    }

    #endregion CompanyChoose

    #region SuperiorChoose


    protected void CustomValidatorSuperiorChooseIsSelect_OnServerValidate(object sender, ServerValidateEventArgs e)
    {
        if (this.superiorChoose.SelectedUserInfoCommonId.HasValue == false)
            e.IsValid = false;
    }
    protected void CustomValidatorSuperiorChooseIsEmpty_OnServerValidate(object sender, ServerValidateEventArgs e)
    {
        if (this.superiorChoose.IsSuperiorEmpty == true)
            e.IsValid = false;
    }

    #endregion SuperiorChoose

    protected void RegisterUser_SendMailError(object sender, SendMailErrorEventArgs e)
    {
    }

}