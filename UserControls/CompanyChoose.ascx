﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyChoose.ascx.cs" Inherits="UserControls_CompanyChoose" %>

<asp:GridView ID="gwCompany" runat="server" AutoGenerateColumns="False" 
    CellPadding="4" DataSourceID="sourceCompany" ForeColor="#333333" 
    GridLines="None" Width="100%" DataKeyNames="CompanyId" EnablePersistedSelection="True"
    onselectedindexchanged="gwCompany_SelectedIndexChanged" AllowPaging="True" 
    PageSize="5" >
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
        <asp:CommandField ShowSelectButton="True" SelectText="Выбрать" />
        <asp:BoundField DataField="CompanyId" HeaderText="CompanyId" Visible="False" />
        <asp:BoundField HeaderText="Название" DataField="CompanyName"/>
        <asp:BoundField DataField="ContactPerson" HeaderText="Контакт" />
        <asp:HyperLinkField Visible="false" DataNavigateUrlFields="WebSite" DataTextField="WebSite" HeaderText="Веб-сайт" />
        <asp:HyperLinkField DataNavigateUrlFields="Email" DataNavigateUrlFormatString="mailto:{0}" 
                            DataTextField="Email" HeaderText="Email" />        
    </Columns>
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333"  />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333"  />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
</asp:GridView>
<asp:ObjectDataSource ID="sourceCompany" runat="server" 
    SelectMethod="GetCompanies" TypeName="CompanyDB"></asp:ObjectDataSource>
