﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegisterUser.ascx.cs" Inherits="UserControls_RegisterUser" %>
<%@ Register Src="~/usercontrols/superiorchoose.ascx" TagPrefix="uc" TagName="SuperiorChoose" %>
<%@ Register Src="~/usercontrols/companychoose.ascx" TagPrefix="uc" TagName="CompanyChoose" %>
<%@ Register Src="~/usercontrols/imageloader.ascx" TagPrefix="uc" TagName="ImageLoader" %>
<%@ Register Src="~/usercontrols/userinfocreate.ascx" TagPrefix="uc" TagName="UserInfoCreate" %>
<%@ Register Src="~/usercontrols/companycreate.ascx" TagPrefix="uc" TagName="CompanyCreate" %>
<%@ Register Src="~/usercontrols/dateselector.ascx" TagPrefix="uc" TagName="DateSelector" %>
<%@ Register Src="~/usercontrols/contactphoneinit.ascx" TagPrefix="uc" TagName="ContactPhoneInit" %>
<%@ Register Src="~/usercontrols/address.ascx" TagPrefix="uc" TagName="Address" %>



<div class="registerWrap">
<asp:CreateUserWizard ID="RegisterUser" runat="server" EnableViewState="true"
                        oncreateduser="RegisterUser_CreatedUser"  
                        OnNextButtonClick="RegisterUser_ActiveStepChanged"
                        AutoGeneratePassword="true"                           
                        DuplicateEmailErrorMessage="Введенный вами адрес электронной почты уже используется."
                        DuplicateUserNameErrorMessage="Пожалуйста, введите другое имя пользователя."
                        LoginCreatedUser="false"
                        CompleteSuccessText="Вы успешно создали пользователя"                        
                        ContinueButtonText="Продолжить"
                        
        onsendingmail="RegisterUser_SendingMail" 
        onsendmailerror="RegisterUser_SendMailError"
        ActiveStepIndex="0">
    <MailDefinition From="tracetrack@ya.ru"
                    Subject="TraceTrack - Информация о регистрации в системе" 
                    Priority="High"
                    IsBodyHtml="true"
                    BodyFileName="~/Account/Admin/MailRegisterNonameManagerMessage.htm">
    </MailDefinition>
    <LayoutTemplate>
        <asp:PlaceHolder ID="wizardStepPlaceholder" runat="server"></asp:PlaceHolder>
        <asp:PlaceHolder ID="navigationPlaceholder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>
    <WizardSteps>
        <asp:TemplatedWizardStep ID="wsSubordination" AllowReturn="true">
            <ContentTemplate>                    
                <h2 runat="server" id="hGreetingUserInfoCommon"></h2>
                <span class="failureNotification">
                    <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" 
                        ValidationGroup="RegisterUserValidationGroup"/>
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Подчиняется супервайзеру:</legend>
                        <uc:SuperiorChoose runat="server" ID="ucSuperiorChoose" EmtpySuperiorText="Вы не создали ни одного супервайзера" />
                        <asp:CustomValidator ID="CustomValidatorSuperiorChooseIsEmpty" runat="server"
                                                 EnableClientScript="false"  Display="None"
                                                 OnServerValidate="CustomValidatorSuperiorChooseIsEmpty_OnServerValidate"  
                                                 ValidationGroup="RegisterUserValidationGroup" CssClass="failureNotification"
                                                 ErrorMessage="Сначала создайте супервайзера">
                        </asp:CustomValidator>
                        <asp:CustomValidator ID="CustomValidatorSuperiorChooseIsSelect" runat="server"
                                                 EnableClientScript="false"  Display="None"
                                                 OnServerValidate="CustomValidatorSuperiorChooseIsSelect_OnServerValidate"  
                                                 ValidationGroup="RegisterUserValidationGroup" CssClass="failureNotification"
                                                 ErrorMessage="Выберите супервайзера которому подчиняется мобильный сотрудник">
                        </asp:CustomValidator>
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnNext" runat="server" CommandName="MoveNext" Text="Далее" 
                                CausesValidation="true"
                                ValidationGroup="RegisterUserValidationGroup"/>
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:TemplatedWizardStep>
        <asp:TemplatedWizardStep ID="wsUserAddInfo" AllowReturn="true">
            <ContentTemplate>                    
                <h2 runat="server" id="hGreetingUserInfoCommon"></h2>
                <span class="failureNotification">
                    <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" 
                        ValidationGroup="RegisterUserValidationGroup"/>
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Общая информация</legend>
                        <uc:UserInfoCreate runat="server" ID="ucUserInfoCreate" ValidationGroup="RegisterUserValidationGroup"/>
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnPrev" runat="server" CommandName="MovePrevious" Text="Назад" />
                        <asp:Button ID="btnNext" runat="server" CommandName="MoveNext" Text="Далее" TabIndex="0"
                                ValidationGroup="RegisterUserValidationGroup"/>
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:TemplatedWizardStep>
        <asp:TemplatedWizardStep ID="wsUserAddress" AllowReturn="true">
            <ContentTemplate>
                <h2 runat="server" id="hGreetingUserInfoCommon"></h2>
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Адрес проживания</legend>
                        <uc:Address runat="server" ID="ucAddressUser" ClassNameDropDownList="ddlRegFullWidth"/>
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnPrev" runat="server" CommandName="MovePrevious" Text="Назад" />
                        <asp:Button ID="btnNext" runat="server" CommandName="MoveNext" Text="Далее" UseSubmitBehavior="true" />
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:TemplatedWizardStep>
        <asp:TemplatedWizardStep ID="wsUserPhones" AllowReturn="true">
            <ContentTemplate>
                <h2 runat="server" id="hGreetingUserInfoCommon"></h2>
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Телефоны</legend>
                        <uc:ContactPhoneInit runat="server" ID="ucContactPhoneUser" />
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnPrev" runat="server" CommandName="MovePrevious" Text="Назад" />
                        <asp:Button ID="btnNext" runat="server" CommandName="MoveNext" Text="Далее"  TabIndex="0" />
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:TemplatedWizardStep>
        <asp:TemplatedWizardStep ID="wsUserPhoto" AllowReturn="true">
            <ContentTemplate>
                <h2 runat="server" id="hGreetingUserInfoCommon"></h2>
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Фотография</legend>
                        <uc:ImageLoader runat="server" ID="ucImageLoader" />
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnPrev" runat="server" CommandName="MovePrevious" Text="Назад" />
                        <asp:Button ID="btnNext" runat="server" CommandName="MoveNext" Text="Далее"  TabIndex="0" />
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
           </asp:TemplatedWizardStep>
        <asp:TemplatedWizardStep ID="wsCompanyAlternative" AllowReturn="true" OnActivate="RegisterUser_wsCompanyAlternative_OnActivate">
            <ContentTemplate>
                <h2 runat="server" id="hGreetingCompany"></h2>         
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Компания</legend>
                        <asp:RadioButtonList ID="rdbListCompanyAlternative" runat="server" CssClass="aspRadioButton" >
                            <asp:ListItem Text="Нет информации о компании" Selected="True" Value="companyNo"></asp:ListItem>
                            <asp:ListItem Text="Выбрать компанию" Value="companyChoose"></asp:ListItem>
                            <asp:ListItem Text="Создать компанию" Value="companyCreate"></asp:ListItem>
                        </asp:RadioButtonList>
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnPrev" runat="server" CommandName="MovePrevious" Text="Назад" />
                        <asp:Button ID="btnNext" runat="server" OnClick="RegisterUser_wsCompanyAlternative_btnNext_OnClick"
                                    CommandName="MoveNext" Text="Далее" TabIndex="0"/>
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:TemplatedWizardStep>
        <asp:TemplatedWizardStep ID="wsCompanyChoose" AllowReturn="true" OnActivate="RegisterUser_wsCompanyAlternative_OnActivate">
            <ContentTemplate>
                <h2 runat="server" id="hGreetingCompany"></h2> 
                <span class="failureNotification">
                    <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" 
                        ValidationGroup="RegisterUserValidationGroup"/>        
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Компания</legend>
                        <uc:CompanyChoose runat="server" ID="ucCompanyChoose" />
                        <asp:CustomValidator ID="CustomValidatorCompanyChoose" runat="server"
                                                 EnableClientScript="false"  Display="None"
                                                 OnServerValidate="CustomValidatorCompanyChoose_OnServerValidate"  
                                                 ValidationGroup="RegisterUserValidationGroup" CssClass="failureNotification"
                                                 ErrorMessage="Выберите компанию, либо вернитесь назад">
                        </asp:CustomValidator>
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnPrev" runat="server" CommandName="MovePrevious" Text="Назад" />
                        <asp:Button ID="btnNext" runat="server" OnClick="RegisterUser_wsCompanyChoose_btnNext_OnClick"
                                    ValidationGroup="RegisterUserValidationGroup" CausesValidation="true"
                                    CommandName="MoveNext" Text="Далее" TabIndex="0"/>
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:TemplatedWizardStep>
        <asp:TemplatedWizardStep ID="wsCompanyInfo" AllowReturn="true">
            <ContentTemplate>
                <h2 runat="server" id="hGreetingCompany"></h2>
                <span class="failureNotification">
                    <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" 
                        ValidationGroup="RegisterUserValidationGroup"/>
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Cоздание компании</legend>
                        <uc:CompanyCreate runat="server" ID="ucCompanyCreate" ValidationGroup="RegisterUserValidationGroup" />
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnBack" runat="server" CommandName="MovePrevious" Text="Назад"/>
                        <asp:Button ID="btnNext" runat="server" CommandName="MoveNext" Text="Далее" TabIndex="0"
                                ValidationGroup="RegisterUserValidationGroup"/>
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:TemplatedWizardStep>
        <asp:TemplatedWizardStep ID="wsCompanyAddress" AllowReturn="true">
            <ContentTemplate>
                <h2 runat="server" id="hGreetingCompany"></h2>   
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Юридический адрес компании</legend>
                        <uc:Address runat="server" ID="ucAddressCompany" ClassNameDropDownList="ddlRegFullWidth" />
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnPrev" runat="server" CommandName="MovePrevious" Text="Назад" />
                        <asp:Button ID="btnNext" runat="server" CommandName="MoveNext" Text="Далее" TabIndex="0"/>
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:TemplatedWizardStep>
        <asp:TemplatedWizardStep ID="wsCompanyPhones" AllowReturn="true">
            <ContentTemplate>
                <h2 runat="server" id="hGreetingCompany"></h2>   
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Телефоны компании</legend>
                        <uc:ContactPhoneInit runat="server" ID="ucCompanyContactPhone" />
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnPrev" runat="server" CommandName="MovePrevious" Text="Назад" />
                        <asp:Button ID="btnNext" runat="server" CommandName="MoveNext" Text="Далее" TabIndex="0" />
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:TemplatedWizardStep>
        <asp:CreateUserWizardStep ID="RegisterUserWizardStep" runat="server" >
            <ContentTemplate>
                <h2 runat="server" id="hGreetingCreatingUser"></h2>
                <span class="failureNotification">
                    <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" 
                        ValidationGroup="RegisterUserValidationGroup"/>
                <div class="accountInfo">
                    <fieldset class="register">
                        <legend>Создание пользователя</legend>
                        <p>
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Имя пользователя:</asp:Label>
                            <asp:TextBox ID="UserName" runat="server" CssClass="textEntry"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                                    CssClass="failureNotification" ErrorMessage="Введите имя пользоателя." ToolTip="Введите имя пользоателя." 
                                    ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                            <asp:TextBox ID="Email" runat="server" CssClass="textEntry"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" 
                                    CssClass="failureNotification" 
                                    ErrorMessage="Введите адрес электронной почты (Email)." 
                                    ToolTip="Введите адрес электронной почты (Email)" 
                                    ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorAspNetUserEmail" runat="server" 
                                    ErrorMessage="Введите правильный email" ToolTip="Введите правильный email"
                                    ControlToValidate="Email" CssClass="failureNotification"
                                    ValidationExpression="^\S+@\S+\.\S+$" ValidationGroup="RegisterUserValidationGroup">*</asp:RegularExpressionValidator>
                        </p>
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="btnPrev" runat="server" CommandName="MovePrevious" Text="Назад" />
                        <asp:Button ID="CreateUserButton" runat="server" CommandName="MoveNext" Text="Создать менеджера" 
                                ValidationGroup="RegisterUserValidationGroup"/>
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Отмена" />
                    </p>
                </div>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:CreateUserWizardStep>
        <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server" Title="UserCretaingСomplete">
            <ContentTemplate>
                <h2 runat="server" style="text-align: center" id="hGrettingCompleteCreateUser"></h2>
                <p style="text-align:center">
                <asp:Button runat="server" CommandName="Countinue" Text="Продолжить" />
                </p>
            </ContentTemplate>
            <CustomNavigationTemplate>
            </CustomNavigationTemplate>
        </asp:CompleteWizardStep>
    </WizardSteps>
</asp:CreateUserWizard>
</div>