﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_UserInfoCreate : System.Web.UI.UserControl
{
    public string LastName
    {
        get { return txtLastName.Text; }
    }
    public string FirstName
    {
        get { return txtFirstName.Text; } 
    }
    public string Patronymic
    {
        get { return txtPatronymic.Text; }
    }
    public string WebSite
    {
        get { return txtUserWebSite.Text; } 
    }
    public DateTime? DateOfBirth
    {
        get { return ucDateSelectorDateOfBirth.SelectedDate; } 
    }
    public bool? Sex
    {
        get
        {
            switch (ddlSex.SelectedValue)
            {
                case "male":
                    return true;
                case "female":
                    return false;
                default:
                    return null;
            }
        }
    }
    public string ValidationGroup
    {
        get { return RegularExpressionValidatorUserWebSite.ValidationGroup; }
        set { RegularExpressionValidatorUserWebSite.ValidationGroup = value; } 
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}