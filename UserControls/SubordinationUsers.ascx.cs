﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

public partial class UserControls_SubordinationUsers : System.Web.UI.UserControl
{
    protected string GetFotoUrl()
    {
        return "/WebServerPart/Handlers/GetUserFoto.ashx";
    }
    protected string GetSexStringOfUser(object sex)
    {

        switch (sex.ToString())
        {
            case "True":
                return "Мужской";
            case "False":
                return "Женский";
            default:
                return "";
        }
    }
    public Guid? SuperiorUserId
    {
        get
        {
            if (ViewState["UserId"] != null)
                return (Guid)ViewState["UserId"];
            else
                return null;
        }
        set
        {
            ViewState["UserId"] = value.Value;
        }
    }
    public bool SuperiorTitle
    {
        get;
        set;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.AddStylesheet(ResolveClientUrl("ContactPhonesOfUser.css"));
        if ( this.SuperiorTitle == true ) 
            ShowSuperiorTitle();
    }

    private void ShowSuperiorTitle()
    {

        if (this.SuperiorUserId.HasValue)
        {
            this.h1LblListOfSubordinateUsers.Visible = true;
            this.lblListOfSubordinateUsers.Visible = true;
            UserInfoCommonDB udb = new UserInfoCommonDB();
            DataSet ds = udb.GetUserInfo(this.SuperiorUserId.Value);
            int userInfoCommonId = ds.Tables["UserInfo"].Rows[0].Field<int>("UserInfoCommonId");
            string firstName = ds.Tables["UserInfo"].Rows[0].Field<string>("FirstName");
            string lastName = ds.Tables["UserInfo"].Rows[0].Field<string>("LastName");
            string patronymic = ds.Tables["UserInfo"].Rows[0].Field<string>("Patronymic");
            lblListOfSubordinateUsers.Text = string.Format("ID: {0} - {1} {2} {3}", userInfoCommonId, lastName, firstName, patronymic); 
        }
    }
    public void AddStylesheet(string url)
    {
        string link = String.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", url);
        this.Page.Header.Controls.Add(new LiteralControl { Text = link });
    }
    protected void sourceSubordinationsUsers_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (this.SuperiorUserId.HasValue)
            e.InputParameters["objUserId"] = this.SuperiorUserId.Value;
        else
            e.Cancel = true;
    }
    protected void sourceSubordinationsUsers_Updating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        UserControls_ImageLoader ucImageLoader = (UserControls_ImageLoader)lvSubordinateUsers.EditItem.FindControl("ucImageLoader");
        e.InputParameters["Foto"] = ucImageLoader.Image;
        e.InputParameters["FotoMimeType"] = ucImageLoader.ImageMimeType;

        Address ucAddress = (Address)lvSubordinateUsers.EditItem.FindControl("ucAddress");
        e.InputParameters["TownId"] = ucAddress.TownIdSelected;
        e.InputParameters["Address"] = ucAddress.AddressInfo;

        UserControls_DateOfBirth ucDataSelector = (UserControls_DateOfBirth)lvSubordinateUsers.EditItem.FindControl("ucDateSelector");
        e.InputParameters["DateOfBirth"] = ucDataSelector.SelectedDate;
    }
}