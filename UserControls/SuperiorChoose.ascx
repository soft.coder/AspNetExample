﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SuperiorChoose.ascx.cs" Inherits="UserControls_SuperiorChoose" EnableViewState="true" %>

<asp:GridView ID="gwSuperior" runat="server" DataSourceID="sourceSubordinationsMobEmp" 
    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
    GridLines="None" 
    OnSelectedIndexChanged="gwSuperior_OnSelectedIndexChanged" 
    OnDataBound="gwSuperior_OnDataBound"
    DataKeyNames="UserInfoCommonId">
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
        <asp:CommandField SelectText="Выбрать" ShowSelectButton="True" />
        <asp:BoundField DataField="UserInfoCommonId" HeaderText="ID" />
        <asp:BoundField DataField="UserName" HeaderText="Логин" />
        <asp:BoundField DataField="LastName" HeaderText="Фамилия" />
        <asp:BoundField DataField="FirstName" HeaderText="Имя " />
        <asp:BoundField DataField="Patronymic" HeaderText="Отчество" />
    </Columns>
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
</asp:GridView>
<asp:ObjectDataSource ID="sourceSubordinationsMobEmp" runat="server" 
    OnSelecting="sourceSuperior_OnSelecting"
    SelectMethod="GetSubordinationsUserInfo" TypeName="SubordinationDB">
    <SelectParameters>
        <asp:Parameter Name="UserId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:Label ID="lblEmptySubordinateSuperviors" runat="server" Visible="false" Text="Список начальников пуст"></asp:Label>

