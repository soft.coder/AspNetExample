﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageLoader.ascx.cs" Inherits="UserControls_ImageLoader" %>

<p style="text-align:center;">
<asp:Image ID="imgPreview" runat="server" Width="128px" Height="128px" ImageUrl="~/Img/user_anonymous.png"  />
</p>
<p style="text-align:center;">
<asp:FileUpload ID="uploadImage" runat="server" ToolTip="Загрузить аватар"/>
<%--<asp:RegularExpressionValidator runat="server" ID="valUpTest" ControlToValidate="uploadImage" ValidationGroup="uploadImageValGroup"
            ErrorMessage="Только рисунки (.jpg, .bmp, .png, .gif)" CssClass="failureNotification"
            ToolTip="Только рисунки (.jpg, .bmp, .png, .gif)" 
            ValidationExpression="(.jpg|.JPG|.gif|.GIF|.bmp|.BMP|.png|.PNG)$" >*
            </asp:RegularExpressionValidator>--%>
<asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" 
                            ErrorMessage="Требуется выбрать изображение для загрузки" 
                            ToolTip="Требуется выбрать изображение для загрузки" CssClass="failureNotification"
                            ControlToValidate="uploadImage" ValidationGroup="uploadImageValGroup" >*
</asp:RequiredFieldValidator>
<asp:CustomValidator ID="CustomValidatorFileSize" runat="server" ValidationGroup="uploadImageValGroup"
                    EnableClientScript="false" OnServerValidate="CustomValidatorFileSize_ServerValidate"
                    ErrorMessage="Максимальный размер файла: 4 Mб" ToolTip="Максимальный размер файла: 4 Mб" 
                    ControlToValidate="uploadImage" Display="Dynamic" CssClass="failureNotification">*</asp:CustomValidator> 
<asp:CustomValidator ID="CustomValidatorFileExtension" runat="server" ValidationGroup="uploadImageValGroup"
                    EnableClientScript="false" OnServerValidate="CustomValidatorFileExtension_ServerValidate"
                    ErrorMessage="Только рисунки (.jpg, .bmp, .png)" ToolTip="Только рисунки (.jpg, .bmp, .png)" 
                    ControlToValidate="uploadImage" Display="Dynamic" CssClass="failureNotification">*</asp:CustomValidator> 
</p>
<p style="text-align:center">
<asp:Button ID="btnUploadImage" OnClick="btnUploadImage_OnClick" runat="server" Text="Загрузить" CausesValidation="true" ValidationGroup="uploadImageValGroup" />
</p>
<p>
<asp:ValidationSummary ID="UploadImageValidationSummary" runat="server" CssClass="failureNotification" 
        ValidationGroup="uploadImageValGroup"/>
</p>