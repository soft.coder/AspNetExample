﻿/* File Created: июля 14, 2012 */
/// <reference path="zoom_control-trace_route.js" />

DestinationStationDialog = {};

(function () {
    
    ids = {};

    DestinationStationDialog.GetLongitude = function(){
        return parseFloat(document.getElementById(ids.hfLongitdue).value);
    }
    DestinationStationDialog.SetLongitude = function(longitude){
        if( typeof(longitude) == "number" ) 
            document.getElementById(ids.hfLongitdue).value = longitude;
        else
            throw new TypeError("longitude arg of function must me a Number");
    }
    DestinationStationDialog.GetLatitude = function(){
        return parseFloat(document.getElementById(ids.hfLatitude).value);
    }
    DestinationStationDialog.SetLatitude = function(latitude){
        if( typeof(latitude) == "number" ) 
            document.getElementById(ids.hfLatitude).value = latitude;
        else
            throw new TypeError("latitude arg of function must me a Number");
    }
    DestinationStationDialog.GetDestinationStationId =  function() { 
        return parseInt(document.getElementById(ids.hfDestinationStationId).value);
    }
    DestinationStationDialog.GetTimeAssign = function() { 
        return new Date(document.getElementById(ids.hfTimeAssign).value);
    }
    DestinationStationDialog.GetTitle = function() {   
        return document.getElementById(ids.hfTitle).value;
    }
    DestinationStationDialog.GetTaskDescription = function(){
        return document.getElementById(ids.hfTaskDescription).value;
    }
    DestinationStationDialog.GetMobEmpId = function() { 
        return parseInt(document.getElementById(ids.hfMobEmpId).value);
    }

    // Start Events handlers

    var hideHandlers = [];
    
    DestinationStationDialog.AddOnHide = function(listener, context){
        if ( typeof(listener) != "function")
            throw new TypeError("listener must be a function");
        newHideHandler = {};
        newHideHandler.listener = listener;
        newHideHandler.context = context;
        hideHandlers.push(newHideHandler);
    }
    DestinationStationDialog.RemoveOnHide = function(listener, context){
        for ( var i = 0 ; i < hideHandlers.length ; i++ ) {
            if ( hideHandlers[i].listener == listener && hideHandlers[i].context == context) 
                return hideHandlers.splice(i, 1);
        }
    }
    var fireOnHide = function (arg) { 
        for ( var i = 0 ; i < hideHandlers.length ; i++ ) {
            hideHandlers[i].listener.call(hideHandlers[i].context, arg);
        }
    }

    var endRequestHandlers = [];

    function endRequest (sender, args){
        for( var i = 0 ; i < endRequestHandlers.length; i++){
            endRequestHandlers[i].listener.call(endRequestHandlers[i].context, sender, args);
        }
    }
    

    DestinationStationDialog.AddEndRequest = function(listener, context){
        if ( typeof(listener) != "function")
            throw new TypeError("endRequestEvent must be a function");
        newEndRequestHander = {};
        newEndRequestHander.listener = listener;
        newEndRequestHander.context = context;
        endRequestHandlers.push(newEndRequestHander);
    }
    DestinationStationDialog.RemoveEndRequest = function(listener, context){
        for ( var i = 0 ; i < endRequestHandlers.length ; i++ ) {
            if ( endRequestHandlers[i].listener == listener && endRequestHandlers[i].context == context) 
                return endRequestHandlers.splice(i, 1);
        }
    }

    // End Events Handlers

    DestinationStationDialog.Init = function (ucIdServerId) {
        ids.hfLongitdue = ucIdServerId + "_hfLongitude";
        ids.hfLatitude = ucIdServerId + "_hfLatitude";
        ids.hfDestinationStationId = ucIdServerId + "_hfDestinationStationId";
        ids.hfTimeAssign = ucIdServerId + "_hfTimeAssign";
        ids.hfTitle = ucIdServerId + "_hfTitle";
        ids.hfTaskDescription = ucIdServerId + "_hfTaskDescription";
        ids.hfMobEmpId = ucIdServerId + "_hfMobEmpId";
        ids.destinationstationDialog = ucIdServerId + "_destinationstationDialog";
        ids.destinationstationOverlay = ucIdServerId + "_destinationstationOverlay";
        ids.ddlMobEmpDS = ucIdServerId + "_ddlMobEmpDS";
        ids.txtTitleDS = ucIdServerId + "_txtTitleDS";
        ids.txtTaskDescriptionDS = ucIdServerId + "_txtTaskDescriptionDS";

        this.DialogDOM = document.getElementById(ids.destinationstationDialog);
        this.OverlayDOM = document.getElementById(ids.destinationstationOverlay);

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);
        
        var dsDialog = this.DialogDOM;
        var clientSize = vSeRGv.getWindowClientWidthHeight();
        var dsDialogCompStyle = vSeRGv.getComputedStyle(dsDialog);
        if (dsDialog != null) {
            if (dsDialogCompStyle != null) {
                var halfWidthDsDialog = parseInt((parseInt(dsDialogCompStyle.width) / 2).toFixed(0));
                var leftMargin = parseInt((parseInt(clientSize.ClientWidth) / 2).toFixed(0)) + halfWidthDsDialog;
                var leftMargin = (leftMargin / 2).toFixed(0);
                dsDialog.style.left = leftMargin + "px";
            }
            else {
                var halfWidthDsDialog = (360).toFixed(0);
                var leftMargin = clientSize.ClientWidth + halfWidthDsDialog;
                dsDialog.style.left = leftMargin;
            }
        }
    }

    DestinationStationDialog.Show = function (longitude, latitude) {
        if (longitude == undefined || latitude == undefined)
            throw new TypeError("longitude or latitude is undefined");
        if (typeof (longitude) != "number" || typeof (latitude) != "number")
            throw new TypeError("longitude or latitude is not a number");

        var dlgDs = DestinationStationDialog;
        this.SetLongitude(longitude);
        this.SetLatitude(latitude);
        dlgDs.OverlayDOM.style.display = "inherit";
        dlgDs.DialogDOM.style.display = "inherit";
    }
    DestinationStationDialog.Hide = function (isCancel) {
        if (isCancel == undefined)
            throw new TypeError("isCancel parameter is undefined");

        var dlgDs = DestinationStationDialog;
        dlgDs.OverlayDOM.style.display = "none";
        dlgDs.DialogDOM.style.display = "none";
        var ddlMobEmp = document.getElementById(ids.ddlMobEmpDS);
        var txtTitleDS = document.getElementById(ids.txtTitleDS);
        var txtTaskDescriptionDS = document.getElementById(ids.txtTaskDescriptionDS);

        var e = {
            IsCancel: isCancel,
            Longitude: this.GetLongitude(),
            Latitude: this.GetLatitude(),
            mobileEmployeeID: parseInt(ddlMobEmp.value),
            Title: txtTitleDS.value,
            TaskDescription: txtTaskDescriptionDS.value
        }
        fireOnHide(e);
    }



})();
//window.onload = DestinationStationDialog.Init;