﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserInfoCreate.ascx.cs" Inherits="UserControls_UserInfoCreate" %>
<%@ Register Src="~/usercontrols/dateselector.ascx" TagPrefix="uc" TagName="DateSelector" %>
<p>
    <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName">Фамилия:</asp:Label>
    <asp:TextBox ID="txtLastName" runat="server" CssClass="textEntry" MaxLength="200"></asp:TextBox>
</p>
<p>
    <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName">Имя:</asp:Label>
    <asp:TextBox ID="txtFirstName" runat="server" CssClass="textEntry" MaxLength="200"></asp:TextBox>
</p>

<p>
    <asp:Label ID="lblPatronymic" runat="server" AssociatedControlID="txtPatronymic">Отчество:</asp:Label>
    <asp:TextBox ID="txtPatronymic" runat="server" CssClass="textEntry" MaxLength="200"></asp:TextBox>
</p>

<p>
    <asp:Label ID="lblWebSite" runat="server" AssociatedControlID="txtUserWebSite">Веб-сайт:</asp:Label>
    <asp:TextBox ID="txtUserWebSite" runat="server" CssClass="textEntry" MaxLength="200"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidatorUserWebSite" runat="server"
                                    ControlToValidate="txtUserWebSite"
                                    ErrorMessage="Введите правильный адрес веб-сайта" 
                                    CssClass="failureNotification"
                                    ValidationExpression="(http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?">*</asp:RegularExpressionValidator>

</p>
<p>
    <asp:Label ID="lblDateOfBirth" runat="server" AssociatedControlID="ucDateSelectorDateOfBirth">Дата рождения:  </asp:Label>
    <uc:DateSelector runat="server" ID="ucDateSelectorDateOfBirth" />
</p>
<p>
    <asp:Label ID="lblSex" runat="server" AssociatedControlID="ddlSex">Пол:</asp:Label>
    <asp:DropDownList ID="ddlSex" runat="server" >
        <asp:ListItem Selected="True" Value="">Выберите пол</asp:ListItem>
        <asp:ListItem Value="male">Мужской</asp:ListItem>
        <asp:ListItem Value="female">Женский</asp:ListItem>
    </asp:DropDownList>
</p>
