﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_ContactPhoneOfUser : System.Web.UI.UserControl
{
    public int? UserInfoCommonId
    {
        get
        {
            if (ViewState["UserInfoCommonId"] == null)
                return null;
            else
                return (int?)ViewState["UserInfoCommonId"];
        }
        set
        {
            if (value.HasValue)
                ViewState["UserInfoCommonId"] = value;
            else
                ViewState["UserInfoCommonId"] = null;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void sourcePhoneInsert_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        if (this.UserInfoCommonId.HasValue)
            e.InputParameters["UserInfoCommonId"] = this.UserInfoCommonId;
        else
            e.Cancel = true;

    }
    protected void sourcePhoneInsert_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (this.UserInfoCommonId.HasValue)
            this.lvUserPhones.DataBind();
    }
    protected void sourceUserContactPhones_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (this.UserInfoCommonId.HasValue)
            e.InputParameters["UserInfoCommonId"] = this.UserInfoCommonId.Value;
        else
            e.Cancel = true;
    }
}