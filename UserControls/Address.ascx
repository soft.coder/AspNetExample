﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Address.ascx.cs" Inherits="Address" EnableViewState="true" %>
<p>
    <asp:Label ID="lblUserCountry" runat="server" AssociatedControlID="ddlUserCountry" >Страна:</asp:Label>
    <asp:DropDownList ID="ddlUserCountry" runat="server" AutoPostBack="true" EnableViewState="true"
                        DataSourceID="sourceCountries" DataTextField="CountryName" 
                            DataValueField="CountryId" AppendDataBoundItems="True"
                            OnSelectedIndexChanged="ddlUserCountry_OnSelectedIndexChanged">
                        <asp:ListItem Text="Выберите страну" Value=""></asp:ListItem>
    </asp:DropDownList>
</p>
<p>
    <asp:Label ID="lblUserRegion" runat="server" AssociatedControlID="ddlUserRegion">Регион:</asp:Label>
    <asp:DropDownList ID="ddlUserRegion" runat="server" AutoPostBack="true"  Enabled="false"
                        DataSourceID="sourceRegions" EnableViewState="true" 
                        DataTextField="RegionName" DataValueField="RegionId" AppendDataBoundItems="True"
                        OnSelectedIndexChanged="ddlUserRegion_OnSelectedIndexChanged">
                        <asp:ListItem Text="Выберите регион" Value=""></asp:ListItem>
    </asp:DropDownList>
    <asp:ObjectDataSource ID="sourceRegions" runat="server" 
        SelectMethod="GetRegionsOfCountry" TypeName="AddressDB" 
        onselecting="sourceRegions_Selecting" onselected="sourceRegions_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlUserCountry"
                Name="CountryId" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:ObjectDataSource>
</p>

<p>
    <asp:Label ID="lblUserTown" runat="server" AssociatedControlID="ddlUserTown">Город:</asp:Label>
    <asp:DropDownList ID="ddlUserTown" runat="server" Enabled="false"
                        DataSourceID="sourceTowns"  EnableViewState="true"
                        DataTextField="TownName" DataValueField="TownId" 
                        AppendDataBoundItems="True" >
                        <asp:ListItem Text="Выберите город" Value=""></asp:ListItem>
    </asp:DropDownList>
    <asp:ObjectDataSource ID="sourceTowns" runat="server" 
        onselecting="sourceTowns_Selecting" SelectMethod="GetTownsOfRegion" 
        TypeName="AddressDB" onselected="sourceTowns_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlUserRegion" Name="RegionId" 
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:ObjectDataSource>
</p>
<p>
    <asp:Label ID="lblAddress" runat="server" AssociatedControlID="txtAddress">Адрес:</asp:Label>
    <asp:TextBox ID="txtAddress" CssClass="textEntry" runat="server" MaxLength="500"></asp:TextBox>
</p>

<asp:ObjectDataSource ID="sourceCountries" runat="server"
                        TypeName="AddressDB"
                        SelectMethod="GetAllCountries">
</asp:ObjectDataSource>