﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DestinationStationDialog.ascx.cs" 
            Inherits="UserControls_DestinationStation" ClientIDMode="Static" %>
<asp:Panel ID="destinationstationOverlay" CssClass="ds-overlay" ClientIDMode="Predictable" runat="server">
</asp:Panel>
<asp:Panel runat="server" CssClass="ds-wrap" ID="destinationstationDialog" ClientIDMode="Predictable"  >
    <asp:UpdatePanel runat="server" ID="upDestinationstationDialog" ClientIDMode="Predictable" >
        <ContentTemplate>
            <asp:Label runat="server" CssClass="ds-header">Пункт назначения</asp:Label>
            <div class="ds-content">
                <asp:HiddenField ID="hfLongitude" runat="server" ClientIDMode="Predictable"/>
                <asp:HiddenField ID="hfLatitude" runat="server" ClientIDMode="Predictable"/>
                <asp:HiddenField ID="hfDestinationStationId" runat="server" ClientIDMode="Predictable"/>
                <asp:HiddenField ID="hfTimeAssign" runat="server" ClientIDMode="Predictable" />
                <asp:HiddenField ID="hfTitle" runat="server"  ClientIDMode="Predictable"/>
                <asp:HiddenField ID="hfTaskDescription" runat="server" ClientIDMode="Predictable"/>
                <asp:HiddenField ID="hfMobEmpId" runat="server" ClientIDMode="Predictable"/>
                <p>
                    <asp:Label runat="server" AssociatedControlID="txtTitleDS" ClientIDMode="Predictable">Название:</asp:Label>
                    <asp:TextBox runat="server" ID="txtTitleDS" CssClass="ds-entry" ClientIDMode="Predictable"/>
                </p>
    
                <p>
                    <asp:Label runat="server" AssociatedControlID="ddlMobEmpDS" ClientIDMode="Predictable">Ответсвенный:</asp:Label>
                    <asp:DropDownList ID="ddlMobEmpDS" CssClass="ds-select" runat="server" ClientIDMode="Predictable">
                        <asp:ListItem Text="Анастасия Букова" Value="9" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Андрей Петров" Value="10"></asp:ListItem>
                        <asp:ListItem Text="Алёна Радкина" Value="10"></asp:ListItem>
                    </asp:DropDownList>
                </p>
                <p>
                    <asp:Label AssociatedControlID="txtTaskDescriptionDS" runat="server" ClientIDMode="Predictable">Описание:</asp:Label>
                    <asp:TextBox ID="txtTaskDescriptionDS"  Columns="20" Rows="5" TextMode="MultiLine" 
                                    CssClass="ds-entry" runat="server" ClientIDMode="Predictable"></asp:TextBox>
                </p>    
                <p style="text-align:right"> 
                    <asp:Button ID="btnSaveDS" Text="Сохранить" runat="server" ClientIDMode="Predictable"
                        onclick="btnSaveDS_Click" OnClientClick="DestinationStationDialog.Hide(false);" />
                    <input type="button" value="Отмена" onclick="DestinationStationDialog.Hide(true);"/>
                </p>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
