﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactPhonesOfUser.ascx.cs" Inherits="UserControls_ContactPhoneOfUser" %>

<ul style="list-style:none">
<asp:ListView ID="lvUserPhones" runat="server" DataKeyNames="ContactPhoneId" DataSourceID="sourceUserContactPhones">
    <ItemTemplate>
        <li>
            <span style="vertical-align:middle">
                <em><%# Eval("PhoneNumber")%></em>&nbsp;
                <%# Eval("TypePhoneName")%>&nbsp;
                <%# Eval("PhoneDescription")%>
            </span>
            <asp:ImageButton ID="ibRemovePhoneOfUser" runat="server" ToolTip="Удалить телефон" 
                    OnClientClick='return confirm("Вы уверены, что хотите удалить этот телефон?");'
                    CommandName="Delete"
                    ImageUrl="~/Img/delete.png" style="vertical-align:middle" />
        </li>
    </ItemTemplate>
</asp:ListView>
<asp:ObjectDataSource ID="sourceUserContactPhones" runat="server" 
        SelectMethod="GetPhonesOfUser" TypeName="PhoneDB" 
        DeleteMethod="DeletePhoneOfUser" 
        onselecting="sourceUserContactPhones_Selecting" >
    <SelectParameters>
        <asp:Parameter Name="UserInfoCommonId" Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="ContactPhoneId" Type="Int64"  />
    </DeleteParameters>
</asp:ObjectDataSource>
<li style="margin-top: 3px;">
<asp:FormView ID="fvAddUserPhone" runat="server" DataSourceID="sourcePhoneInsert">
    <ItemTemplate>
        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="New">Добавить телефон</asp:LinkButton>
    </ItemTemplate>
    <InsertItemTemplate>
        <table class="contactphoneadd-formview-insert">
        <tr>
            <td>
                <asp:Label ID="lblNumberOfPhone" runat="server" AssociatedControlID="txtNumberOfPhone">Номер: </asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNumberOfPhone" MaxLength="100" Text='<%# Bind("PhoneNumber") %>'/>
                <asp:RequiredFieldValidator ID="PhoneNumberRequiredFieldValidator" runat="server" 
                            ControlToValidate="txtNumberOfPhone"
                            ErrorMessage="Для добавления контактного телефона нужно ввести номер телефона"
                            ToolTip="Для добавления контактного телефона нужно ввести номер телефона"
                            ValidationGroup="AddPhoneValidationGroup"
                            CssClass="failureNotification">*
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="PhoneNubmerRegularExpressionValidator" runat="server" 
                        ErrorMessage="В поле номер телефона должно вводиться число" 
                        ToolTip="В поле номер телефона должно вводиться число, -, (, )"
                        ControlToValidate="txtNumberOfPhone" ValidationExpression="[\d|\-|\(|\)]+"
                        ValidationGroup="AddPhoneValidationGroup"
                        CssClass="failureNotification">*
                </asp:RegularExpressionValidator>
            </td>
        </tr>  
        <tr>
            <td>
                <asp:Label ID="lblPhoneDescription" runat="server" AssociatedControlID="txtPhoneDescription">Описание: </asp:Label>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPhoneDescription" MaxLength="300" Text='<%# Bind("PhoneDescription") %>'/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTypePhone" runat="server" AssociatedControlID="ddlTypePhone">Тип:</asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlTypePhone" 
                                DataSourceID="sourcePhoneInsert" 
                                DataTextField="TypePhoneName" DataValueField="TypePhoneId"
                                AppendDataBoundItems="true" SelectedValue='<%# Bind("TypePhoneId") %>'>
                    <asp:ListItem Enabled="true" Text="нет" Value=""></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:LinkButton ID="lbInsert" runat="server" CommandName="Insert" ValidationGroup="AddPhoneValidationGroup">Добавить</asp:LinkButton>
                &nbsp;&nbsp;
                <asp:LinkButton ID="lbCancel" runat="server" CommandName="Cancel">Отмена</asp:LinkButton>
            </td>
        </tr>
        </table>
    </InsertItemTemplate>
</asp:FormView>
</li>
</ul>
<asp:ObjectDataSource runat="server" ID="sourcePhoneInsert" 
        TypeName="PhoneDB" SelectMethod="GetTypesOfPhone" 
    InsertMethod="InsertPhoneOfUser" oninserting="sourcePhoneInsert_Inserting" 
    oninserted="sourcePhoneInsert_Inserted">
    <InsertParameters>
        <asp:Parameter Name="UserInfoCommonId" Type="Int32"/>
        <asp:Parameter Name="PhoneDescription" Type="String" />
        <asp:Parameter Name="PhoneNumber" Type="String" />
        <asp:Parameter Name="TypePhoneId" Type="Int32" ConvertEmptyStringToNull="true" />
    </InsertParameters>
</asp:ObjectDataSource>