﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateSelector.ascx.cs" Inherits="UserControls_DateOfBirth" EnableViewState="true" %>

<asp:DropDownList ID="ddlDay" runat="server" AutoPostBack="true" Enabled="false" AppendDataBoundItems="true" 
            CssClass="<%# CssClassNameOfDropDownList %>" 
            OnSelectedIndexChanged="ddlDay_OnSelectedIndexChanged">
    <asp:ListItem Text="День" Value=""></asp:ListItem>
</asp:DropDownList>
<asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="true" Enabled="false" AppendDataBoundItems="true"
        CssClass="<%# CssClassNameOfDropDownList %>" 
        OnSelectedIndexChanged="ddlMonth_OnSelectedIndexChanged">
    <asp:ListItem Text="Месяц" Value=""></asp:ListItem>
</asp:DropDownList>
<asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
        CssClass="<%# CssClassNameOfDropDownList %>" 
        OnSelectedIndexChanged="ddlYear_OnSelectedIndexChanged">
    <asp:ListItem Text="Год" Value=""></asp:ListItem>
</asp:DropDownList>
