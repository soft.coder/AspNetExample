﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyCreate.ascx.cs" Inherits="UserControls_CompanyCreate" %>

<p>
    <asp:Label ID="lblCompanyName" runat="server" AssociatedControlID="txtCompanyName">Название компании:</asp:Label>
    <asp:TextBox ID="txtCompanyName" CssClass="textEntry" runat="server" MaxLength="500"></asp:TextBox>
</p>
<p>
    <asp:Label ID="lblContactPerson" runat="server" AssociatedControlID="txtContactPerson">Контактное лицо:</asp:Label>
    <asp:TextBox ID="txtContactPerson" CssClass="textEntry" runat="server" MaxLength="500"></asp:TextBox>
</p>
<p>
    <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtCompanyEmail">Email:</asp:Label>
    <asp:TextBox ID="txtCompanyEmail" CssClass="textEntry" runat="server" MaxLength="300"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server" 
            ErrorMessage="Введите правильный email" ControlToValidate="txtCompanyEmail" CssClass="failureNotification"
            ValidationExpression="^\S+@\S+\.\S+$">*</asp:RegularExpressionValidator>
</p>
<p>
    <asp:Label ID="lblWebsite" runat="server" AssociatedControlID="txtCompanyWebSite">WebSite:</asp:Label>
    <asp:TextBox ID="txtCompanyWebSite" CssClass="textEntry" runat="server" MaxLength="300"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidatorCompanyWebSite" runat="server"
                                    ControlToValidate="txtCompanyWebSite"
                                    ErrorMessage="Введите правильный адрес веб-сайта компании" 
                                    ToolTip="Введите правильный адрес веб-сайта компании" 
                                    CssClass="failureNotification"
                                    ValidationExpression="(http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?">*</asp:RegularExpressionValidator>

</p>
