﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

public partial class UserControls_ImageLoader : System.Web.UI.UserControl, IDisposable
{
    private int maxWidth = 128;
    public int MaxWidth
    {
        get { return maxWidth; }
        set { maxWidth = value; } 
    }
    public byte[] Image
    {
        get
        {
            return (byte[])Session["ImageForUpload"];
        }
        private set
        {
            Session["ImageForUpload"] = value;
        }
    }
    public string ImageMimeType
    {
        get 
        {
            if (Session["ImageForUploadMimeType"] != null)
                return (string)Session["ImageForUploadMimeType"];
            else
                return string.Empty;
        }
        set { Session["ImageForUploadMimeType"] = value; }
    }
    private string[] AllowExtensions = new string[] { ".png", ".jpeg", ".jpg", ".bmp" };
    public string UploadImageExtension
    {
        get { return System.IO.Path.GetExtension(uploadImage.FileName).ToLower(); }
    }
    private ImageFormat UploadImageFormat
    {
        get
        {
            switch (this.UploadImageExtension)
            {
                case ".jpg":
                    return ImageFormat.Jpeg;
                case ".jpeg":
                    return ImageFormat.Jpeg;
                case ".png":
                    return ImageFormat.Png;
                case ".bmp":
                    return ImageFormat.Bmp;
                case ".gif":
                    return ImageFormat.Gif;
                default:
                    throw new ArgumentOutOfRangeException("unexpected image format");
            }
        }
    }
    public string UploadImageMimeType
    {
        get
        {
            switch (this.UploadImageExtension)
            {
                case ".jpg":
                    return "image/jpeg";
                case ".jpeg":
                    return "image/jpeg";
                case ".png":
                    return "image/png";
                case ".bmp":
                    return "image/bmp";
                case ".gif":
                    return "image/gif";
                default:
                    throw new ArgumentOutOfRangeException("unexpected image format");
            }
        }
    }
    public string DefaultImageUrl
    {
        set
        {
            this.imgPreview.ImageUrl = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private byte[] GetImageBits(Stream fs, int size)
    {
        byte[] img = new byte[size];
        fs.Read(img, 0, size);
        return img;
    }
    protected void btnUploadImage_OnClick(object sender, EventArgs e)
    {
        Page.Validate("uploadImageValGroup");
        if (Page.IsValid)
        {
            int sizeOfFile = uploadImage.PostedFile.ContentLength;
            int recordsAffected = -1;
            byte[] imgBytes = GetImageBits(uploadImage.PostedFile.InputStream, sizeOfFile);
            Stream bmpStream = new MemoryStream(imgBytes);
            Bitmap bmp = new Bitmap(bmpStream);
            float widhtNew, heightNew;
            widhtNew = this.MaxWidth;
            heightNew = (widhtNew / bmp.Width) * bmp.Height;
            //bmp.SetResolution(widhtNew, heightNew);
            System.Drawing.Image resImg = bmp.GetThumbnailImage((int)widhtNew, (int)heightNew, null, System.IntPtr.Zero);
            Bitmap resBmp = new Bitmap(resImg);


            using (MemoryStream ms = new MemoryStream())
            {
                resBmp.Save(ms, this.UploadImageFormat);
                this.Image = ms.ToArray();
                Session["ImageForUpload"] = this.Image;
                Session["ImageForUploadMimeType"] = this.UploadImageMimeType;
            }
            Uri contextUri = HttpContext.Current.Request.Url;
            string curPort = contextUri.Port == 80 ? string.Empty : ":" + contextUri.Port.ToString();

            imgPreview.ImageUrl = "~/Handlers/GetImageUploadPreviewHandler.ashx";
        }     


    }


    protected void CustomValidatorFileSize_ServerValidate(object source, ServerValidateEventArgs args)
    {

        int filesize = uploadImage.PostedFile.ContentLength;
        if (filesize > 4 * 1024 * 1024)
        {
            args.IsValid = false;
        }
    }
    protected void CustomValidatorFileExtension_ServerValidate(object source, ServerValidateEventArgs args)
    {
        var ext = System.IO.Path.GetExtension(uploadImage.FileName).ToLower();
        
        if (this.AllowExtensions.Contains(ext) == false)
        {
            args.IsValid = false;
        }
    }
    public void Dispose()
    {
        Session["ImageForUpload"] = null;
        Session["ImageForUploadMimeType"] = null;
    }

}