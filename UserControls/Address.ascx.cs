﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Address : System.Web.UI.UserControl
{
    public int? CountryIdSelected
    {
        get
        {
            int id;
            if ( int.TryParse( ddlUserCountry.SelectedValue, out id) )
                return id;
            else
                return null;
        }
        set
        {
            if (value.HasValue)
                ddlUserCountry.SelectedValue = value.Value.ToString();
        }
    }
    private long? regionIdSelectedSetter;
    public long? RegionIdSelected
    {
        get
        {
            long id;
            if (long.TryParse(ddlUserRegion.SelectedValue, out id))
                return id;
            else
                return null;
        }
        set
        {
            if ( value.HasValue ) 
                regionIdSelectedSetter = value.Value;
        }
    }
    private long? townIdSelectedSetter;
    public long? TownIdSelected
    {
        get
        {
            long id;
            if (long.TryParse(ddlUserTown.SelectedValue, out id))
                return id;
            else
                return null;
        }
        set
        {
            if(value.HasValue)
                this.townIdSelectedSetter = value.Value;
        }
    }
    public string AddressInfo
    {
        get
        {
            return txtAddress.Text;
        }
        set
        {
            txtAddress.Text = value;
        }
    }
    public string ClassNameDropDownList
    {
        set
        {
            ddlUserCountry.CssClass = value;
            ddlUserRegion.CssClass = value;
            ddlUserTown.CssClass = value;
        }
    }
    public string ClassNameLabel
    {
        set
        {
            lblAddress.CssClass = value;
            lblUserCountry.CssClass = value;
            lblUserRegion.CssClass = value;
            lblUserTown.CssClass = value;            
        }
    }
    public string ClassNameTextBox
    {
        set
        {
            txtAddress.CssClass = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void sourceRegions_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (e.InputParameters["CountryId"] == null)
        {
            e.Cancel = true;
            ddlUserRegion.Enabled = false;
        }
        else
        {
            RemoveOldItemsOfDropDownListWhithoutFirstElement(ddlUserRegion);
            RemoveOldItemsOfDropDownListWhithoutFirstElement(ddlUserTown);
            ddlUserRegion.Enabled = true;
        }
    }
    protected void sourceTowns_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        if (e.InputParameters["RegionId"] == null)
        {
            e.Cancel = true;
            ddlUserTown.Enabled = false;
        }
        else
        {
            RemoveOldItemsOfDropDownListWhithoutFirstElement(ddlUserTown);
            ddlUserTown.Enabled = true;
        }

    }

    protected void sourceRegions_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (this.regionIdSelectedSetter.HasValue)
            ddlUserRegion.SelectedValue = this.regionIdSelectedSetter.Value.ToString();
    }
    protected void sourceTowns_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (this.townIdSelectedSetter.HasValue)
            this.ddlUserTown.SelectedValue = this.townIdSelectedSetter.Value.ToString();
    }
    protected void ddlUserCountry_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlUserCountry.SelectedValue == "")
        //{
        //    ddlUserRegion.Enabled = false;
        //    ddlUserTown.Enabled = false;

        //}
        //else
        //{
        //    ddlUserRegion.Enabled = true;
        //    ddlUserTown.Enabled = false;
        //}
        ddlUserRegion.SelectedIndex = 0;
        ddlUserTown.SelectedIndex = 0;
    }
    protected void ddlUserRegion_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlUserRegion.SelectedValue == "")
        //{
        //    ddlUserTown.Enabled = false;
        //}
        //else
        //{
        //    ddlUserTown.Enabled = true;
        //}
        ddlUserTown.SelectedIndex = 0;
        
    }

    private void RemoveOldItemsOfDropDownListWhithoutFirstElement(DropDownList ddlAddress)
    {
        int countOfElements = ddlAddress.Items.Count;
        for (int i = countOfElements - 1; i > 0; i--)
        {
            ddlAddress.Items.RemoveAt(i);
        }
    }



}