﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_ContactPhoneInit : System.Web.UI.UserControl
{
    private string ViewStateIdCompanyPhones { get; set; }

    public List<PhoneDetails> ContactPhones
    {
        get
        {
            if (ViewState[ViewStateIdCompanyPhones] != null)
            {
                return (List<PhoneDetails>)ViewState[ViewStateIdCompanyPhones];
            }
            else
                return null;

        }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        ViewStateIdCompanyPhones = "CompanyPhones";
        if (ViewState[ViewStateIdCompanyPhones] != null)
        {
            CopmanyPhonesGridViewDataBind();
        }
    }

    private void CopmanyPhonesGridViewDataBind()
    {

        List<PhoneDetails> companyPhones = (List<PhoneDetails>)ViewState[ViewStateIdCompanyPhones];
        gwPhones.DataSource = companyPhones;
        gwPhones.DataBind();
    }
    protected void btnAddPhone_OnClick(object sender, EventArgs e)
    {
        Page.Validate("AddPhoneValidationGroup");
        if (Page.IsValid)
        {
            PhoneDetails phoneDetails;
            if (ddlTypePhone.SelectedValue == "")
                phoneDetails = new PhoneDetails(txtPhoneDescription.Text, txtPhoneNumber.Text);
            else
            {
                int typePhoneId = int.Parse(ddlTypePhone.SelectedValue);
                phoneDetails = new PhoneDetails(txtPhoneDescription.Text, txtPhoneNumber.Text,
                                                typePhoneId, ddlTypePhone.SelectedItem.Text);
            }

            if (ViewState[this.ViewStateIdCompanyPhones] == null)
            {
                List<PhoneDetails> companyPhones = new List<PhoneDetails>();
                companyPhones.Add(phoneDetails);
                ViewState[this.ViewStateIdCompanyPhones] = companyPhones;
            }
            else
            {
                List<PhoneDetails> companyPhones = (List<PhoneDetails>)ViewState[this.ViewStateIdCompanyPhones];
                companyPhones.Add(phoneDetails);
                ViewState[this.ViewStateIdCompanyPhones] = companyPhones;
            }
            CopmanyPhonesGridViewDataBind();

        }
    }
}