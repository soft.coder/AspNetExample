﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_CompanyChoose : System.Web.UI.UserControl
{
    public int? SelectedCompanyId
    {
        get
        {
            if (ViewState["SelectedCompanyId"] != null)
                return (int?)ViewState["SelectedCompanyId"];
            else
                return null;
        }
        private set
        {
            ViewState["SelectedCompanyId"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void gwCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.SelectedCompanyId = (int)gwCompany.SelectedDataKey["CompanyId"];
    }
}