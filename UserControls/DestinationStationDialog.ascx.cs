﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Security;

public partial class UserControls_DestinationStation : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            FillMobEmpsDropDownList();
            string keyScript = "DestinationStationDialog";
            string urlScript = ResolveClientUrl("DestinationStationDialog.js");
            if (Page.ClientScript.IsClientScriptIncludeRegistered(Page.GetType(), keyScript) == false)
            {
                Page.ClientScript.RegisterClientScriptInclude(keyScript, urlScript);
            }
            // TO DO Check if this stylesheet  added
            this.AddStylesheet(ResolveClientUrl("DestinationStationDialog.css"));
        }
    }

    private void FillMobEmpsDropDownList()
    {
        SubordinationDB sdb = new SubordinationDB();
        Guid UserId = new Guid(Membership.GetUser(Page.User.Identity.Name).ProviderUserKey.ToString());
        List<UserInfoDetails> lstUid = sdb.GetSubordinationsUsersInfoDetailByUserId(UserId);
        var fullname = from uid in lstUid
                       select new
                       {
                           UserInfoCommonId = uid.ID,
                           FullName = string.Format("{0} {1} {2}", uid.FirstName, uid.Patronymic, uid.LastName)
                       };
        ddlMobEmpDS.DataSource = fullname;
        ddlMobEmpDS.DataTextField = "FullName";
        ddlMobEmpDS.DataValueField = "UserInfoCommonId";
        ddlMobEmpDS.DataBind();
    }
    public void AddStylesheet(string url)
    {
        string link = String.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", url);
        this.Page.Header.Controls.Add(new LiteralControl { Text = link });
    }
    protected void btnSaveDS_Click(object sender, EventArgs e)
    {
        DestinationStationDetails ds = new DestinationStationDetails(
            null,
            int.Parse(ddlMobEmpDS.SelectedValue),
            double.Parse(hfLongitude.Value.Replace(".", ",")),
            double.Parse(hfLatitude.Value.Replace(".", ",")),
            txtTitleDS.Text,
            txtTaskDescriptionDS.Text,
            null,
            null
        );
        DestinationStationDB dsDb = new DestinationStationDB();
        DestinationStationDetails insertedDs = dsDb.InsertDestinationStation(ds);

        hfDestinationStationId.Value = insertedDs.ID.ToString();
        hfLatitude.Value = insertedDs.Latitude.ToString().Replace(",", ".");
        hfLongitude.Value = insertedDs.Longitude.ToString().Replace(",", ".");
        hfMobEmpId.Value = insertedDs.UserInfoCommonId.ToString();
        hfTaskDescription.Value = insertedDs.TaskDescription;
        DateTime da = insertedDs.TimeAssign.Value;
        string strda = string.Format("{0}/{1}/{2} {3}:{4}:{5}" , new object[] { 
            da.Month, da.Day, da.Year, da.Hour, da.Minute, da.Second
        });
        hfTimeAssign.Value =  strda;
        hfTitle.Value = insertedDs.Title;

        txtTitleDS.Text = "";
        txtTaskDescriptionDS.Text = "";
        ddlMobEmpDS.SelectedIndex = 0;
        
    }
}