﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_CompanyCreate : System.Web.UI.UserControl
{
    public string CompanyName
    {
        get
        {
            return txtCompanyName.Text;
        }
    }
    public string ContactPerson
    {
        get
        {
            return txtContactPerson.Text;
        }
    }
    public string CompanyEmail
    {
        get
        {
            return txtCompanyEmail.Text;
        }
    }
    public string CompanyWebSite
    {
        get
        {
            return txtCompanyWebSite.Text;
        }
    }
    public string ValidationGroup
    {
        get
        {
            return RegularExpressionValidatorEmail.ValidationGroup;
        }
        set
        {
            RegularExpressionValidatorEmail.ValidationGroup = value;
            RegularExpressionValidatorCompanyWebSite.ValidationGroup = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }
}