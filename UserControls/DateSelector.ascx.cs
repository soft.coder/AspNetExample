﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Globalization;

public partial class UserControls_DateOfBirth : System.Web.UI.UserControl
{
    private DateTimeFormatInfo DateFormat = new DateTimeFormatInfo();

    private int startYear = 1905;
    public int StartYear
    {
        get { return startYear; }
        set { startYear = value; } 
    }

    public int? SelectedYear
    {
        get
        {
            int selectedYear;
            if ( int.TryParse(this.ddlYear.SelectedValue, out selectedYear))
                return selectedYear;
            else 
                return null;
        }
    }
    public int? SelectedMonth
    {
        get
        {
            int selectedMonth;
            if (int.TryParse(this.ddlMonth.SelectedValue, out selectedMonth))
                return selectedMonth;
            else
                return null;
        }
    }
    public int? SelectedDay
    {
        get
        {
            int selectedDay;
            if (int.TryParse(this.ddlDay.SelectedValue, out selectedDay))
                return selectedDay;
            else
                return null;
        }
    }
    public DateTime? SelectedDate
    {
        get
        {
            if (ViewState["SelectedDate"] != null)
                return (DateTime)ViewState["SelectedDate"];
            else
                return null;
        }
         set
        {
            ViewState["SelectedDate"] = value;
        }
    }
    public DateTime? DefaultDate
    {
        get
        {
            return (DateTime?)ViewState["DefaultDate"];
        }
        set
        {
            ViewState["DefaultDate"] = value;
            if (ddlYear.Items.Count > 1 && value != null)
                InitDefaultDate();
        }
    }
    public string CssClassNameOfDropDownList
    {
        get;
        set;
    }
    private ListItem defaultDayValue = new ListItem("День", "");



    protected void Page_Load(object sender, EventArgs e)
    {
        if (ddlYear.Items.Count == 1)
        {
            InitDropDownLists();
        }
    }

    private void InitDefaultDate()
    {
        ddlYear.SelectedValue = this.DefaultDate.Value.Year.ToString();
        ddlMonth.SelectedValue = this.DefaultDate.Value.Month.ToString();        
        InitDdlDayInSelectedMonth();
        ddlDay.SelectedValue = this.DefaultDate.Value.Day.ToString();
        ddlYear.Enabled = true;
        ddlMonth.Enabled = true;
        ddlDay.Enabled = true;
    }

    private void InitDropDownLists()
    {
        for (int i = DateTime.Now.Year; i >= this.StartYear ; i--)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlYear.DataBind();

        string[] months = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
        for (int i = 0; i < 12; i++)
        {
            ddlMonth.Items.Add(new ListItem(months[i], (i + 1).ToString()));
        }
        ddlMonth.DataBind();
    }
    protected void ddlYear_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.SelectedDate = null;
        
        if (this.SelectedYear == null)
        {
            ddlMonth.SelectedIndex = 0;
            ddlMonth.Enabled = false;            
            ddlDay.SelectedIndex = 0;
            ddlDay.Enabled = false;            
            return;
        }
        if (this.SelectedYear >= this.StartYear && this.SelectedYear <= DateTime.Now.Year)
        {
            ddlMonth.SelectedIndex = 0;
            ddlMonth.Enabled = true;
            ddlDay.SelectedIndex = 0;
            ddlDay.Enabled = false;
            return;
        }
        // may be html hack, restore def val
        ddlYear.SelectedIndex = 0;
        ddlMonth.Enabled = false;
        ddlDay.Enabled = false;
    }
    protected void ddlMonth_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.SelectedDate = null;
        
        if (this.SelectedMonth == null)
        {
            ddlDay.Enabled = false;
            return;
        }
        if (this.SelectedMonth > 0 && this.SelectedMonth < 13)
        {
            InitDdlDayInSelectedMonth();
            return;
        }
        // may be html hack, restore def val
        ddlMonth.SelectedIndex = 0;
        ddlDay.Enabled = false;
    }

    private void InitDdlDayInSelectedMonth()
    {
        int dayInSelecteMonth = DateTime.DaysInMonth(this.SelectedYear.Value, this.SelectedMonth.Value);
        if (ddlDay.Items.Count > 1)
        {
            ddlDay.Items.Clear();
            ddlDay.Items.Add(defaultDayValue);
        }
        for (int i = 1; i <= dayInSelecteMonth; i++)
        {
            ddlDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlDay.Enabled = true;
    }
    protected void ddlDay_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.SelectedDay == null)
        {
            this.SelectedDate = null;
            return;
        }
        int dayInSelecteMonth = DateTime.DaysInMonth(this.SelectedYear.Value, this.SelectedMonth.Value);
        if (this.SelectedDay > 0 && this.SelectedDay <= dayInSelecteMonth)
        {
            DateTime date =  new DateTime(this.SelectedYear.Value, this.SelectedMonth.Value, this.SelectedDay.Value);
            this.SelectedDate = date;

            return;
        }
        // may be html hack, restore def val
        ddlYear.SelectedIndex = 0;
    }
}