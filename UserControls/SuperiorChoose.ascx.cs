﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Security;

public partial class UserControls_SuperiorChoose : System.Web.UI.UserControl
{
#region publicProp

    public int? SelectedUserInfoCommonId
    {
        get
        {
            if (ViewState["SelectedUserInfoCommonId"] != null)
                return (int?)ViewState["SelectedUserInfoCommonId"];
            else
                return null;
        }
        private set
        {
            ViewState["SelectedUserInfoCommonId"] = value;
        }
    }

    public string EmtpySuperiorText
    {
        get
        {
            return (string)ViewState["EmtpySuperiorText"];
        }
        set
        {
            ViewState["EmtpySuperiorText"] = value;
        }
    }

    public bool IsSuperiorEmpty
    {
        get
        {
            return (bool)ViewState["IsSuperiorEmpty"];
        }
        set
        {
            ViewState["IsSuperiorEmpty"] = value;
        }
    }

#endregion publicProp

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.EmtpySuperiorText != string.Empty)
        {
            lblEmptySubordinateSuperviors.Text = this.EmtpySuperiorText;
        }
    }
    protected void sourceSuperior_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        Guid userId = new Guid(Membership.GetUser(Page.User.Identity.Name).ProviderUserKey.ToString());
        UserInfoCommonDB uiDb = new UserInfoCommonDB();
        int? userInfoCommonId = uiDb.GetUserInfoCommonId(userId);
        if (userInfoCommonId.HasValue)
        {
            e.InputParameters["UserInfoCommonId"] = userInfoCommonId.Value;
        }
        else
            e.Cancel = true;
    }
    protected void gwSuperior_OnDataBound(object sender, EventArgs e)
    {
        if (gwSuperior.Rows.Count == 0)
        {
            lblEmptySubordinateSuperviors.Visible = true;
            this.IsSuperiorEmpty = true;
        }
        else
        {
            lblEmptySubordinateSuperviors.Visible = false;
            this.IsSuperiorEmpty = false;
        }
    }

    protected void gwSuperior_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.SelectedUserInfoCommonId = (int)gwSuperior.SelectedDataKey["UserInfoCommonId"];
    }
}