﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubordinationUsers.ascx.cs" Inherits="UserControls_SubordinationUsers" %>
<%@ Register Src="~/usercontrols/contactphonesofuser.ascx" TagPrefix="uc" TagName="ContactPhonesOfUser" %>
<%@ Register Src="~/usercontrols/imageloader.ascx" TagPrefix="uc" TagName="ImageLoader" %>
<%@ Register Src="~/usercontrols/dateselector.ascx" TagPrefix="uc" TagName="DateSelector" %>
<%@ Register Src="~/usercontrols/address.ascx" TagPrefix="uc" TagName="Address" %>

<h1 runat="server" visible="false" id="h1LblListOfSubordinateUsers" style="text-align:center; margin-bottom: -20px;">
    <asp:Label runat="server" ID="lblListOfSubordinateUsers" Visible="False"
                CssClass="lbl-list-subordinate-users"></asp:Label>
</h1>
<div class="listUsers">
    <asp:ListView ID="lvSubordinateUsers" runat="server" 
        DataSourceID="sourceSubordinationsUsers"
        DataKeyNames="UserInfoCommonId">
        <ItemSeparatorTemplate>
            <hr />
        </ItemSeparatorTemplate>
        <ItemTemplate>
            <table width="100%" class="listview-item">
                <tr>
                <td>
                <p style="text-align:center; margin-bottom:0px; line-height:1em" >
                    <b>ID: <%# Eval("UserInfoCommonId")%> - <%# Eval("LastName") %> <%# Eval("FirstName") %> <%# Eval("Patronymic")%></b> 
                    &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" CommandName="Edit" Text="Редактировать" />
                    &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CommandName="Edit" Text="Удалить"
                                            OnClientClick="return confirm('Вы уверены, что хотите удалить этого сотрудника?');"  />
                </p>
                <table>
                <tr valign="top">
                    <td>
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/Handlers/GetFotoById.ashx?id=" + Eval("UserInfoCommonId")%>' />
                    </td>
                    <td>
                        <ul style="list-style:none;">
                            <li><em>Cтрана:</em> <%# Eval("CountryName") %></li>
                            <li><em>Регион:</em> <%# Eval("RegionName") %></li>
                            <li><em>Город:</em> <%# Eval("TownName")%></li>
                            <li><em>Адрес:</em> <%# Eval("Address")%></li>
                
                        </ul>
                    </td>
                    <td>
                        <ul style="list-style:none;">
                            <li><em>День рождения:</em> <%# Eval("DateOfBirth", "{0:D}")%></li>
                            <li><em>Пол:</em> <%# GetSexStringOfUser(Eval("Sex"))%></li>
                            <li><em>Веб-сайт:</em> <%# Eval("WebSite")%></li>
                            <li><em>Компания:</em> <%# Eval("CompanyName")%></li>
                        </ul>
                    </td>
                    <td>
                        <uc:ContactPhonesOfUser runat="server" ID="ucContactPhonesOfUser"
                            UserInfoCommonId='<%# Eval("UserInfoCommonId") == DBNull.Value ? null : Eval("UserInfoCommonId") %>' />
                    </td>
                </tr>
                </table>
                </td>
                </tr>
            </table>
        </ItemTemplate>
        <EditItemTemplate>
            <table width="100%" class="listview-edit">
                <tr>
                <td>
                <p style="text-align:center; margin-bottom:0px; line-height:1em" >
                    <b>ID: <%# Eval("UserInfoCommonId")%></b> - 
                    <asp:TextBox runat="server" CssClass="full-name" ID="txtLastName" Text='<%# Bind("LastName")%>'/>
                    <asp:TextBox runat="server" CssClass="full-name" ID="txtFirstName" Text='<%# Bind("FirstName") %>' /> 
                    <asp:TextBox runat="server" CssClass="full-name" ID="txtPatronymic" Text='<%# Bind("Patronymic")%>' /> 
                </p>                
                <table>
                <tr valign="top">
                    <td>
                        <uc:ImageLoader runat="server" ID="ucImageLoader" 
                            DefaultImageUrl='<%# "~/Handlers/GetFotoById.ashx?id=" + Eval("UserInfoCommonId")%>' />
                    </td>
                    <td>
                        <uc:Address runat="server" ID="ucAddress" 
                                    CountryIdSelected='<%# Eval("CountryId") == DBNull.Value ? null : Eval("CountryId") %>' 
                                    RegionIdSelected='<%# Eval("RegionId") == DBNull.Value ? null : (long?)Eval("RegionId") %>'
                                    TownIdSelected='<%# Eval("TownId") == DBNull.Value ? null : (long?)Eval("TownId") %>'
                                    AddressInfo='<%# Eval("Address") == DBNull.Value ? "" : Eval("Address")%>'/>
                    </td>
                    <td>
                        <p>
                            <asp:Label ID="Label1" runat="server" AssociatedControlID="ucDateSelector">День рождения:</asp:Label> 
                            <uc:DateSelector runat="server" ID="ucDateSelector"  
                                            DefaultDate='<%#  Eval("DateOfBirth") == DBNull.Value ? null : (DateTime?)Eval("DateOfBirth") %>'
                                            CssClassNameOfDropDownList="listview-edit-dateselectors"  />
                        </p>
                        <p>
                            <asp:Label ID="Label2" runat="server" AssociatedControlID="ddlSex">Пол:</asp:Label>
                            <asp:DropDownList runat="server" ID="ddlSex" 
                                    SelectedValue='<%# Bind("Sex") %>'>
                                <asp:ListItem Text="Мужской" Value="True"></asp:ListItem>
                                <asp:ListItem Text="Женский" Value="False"></asp:ListItem>
                                <asp:ListItem Text="Не определен" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Label ID="Label3" runat="server" AssociatedControlID="txtWebSite">Веб-сайт: </asp:Label>
                            <asp:TextBox runat="server" ID="txtWebSite" Text='<%# Bind("WebSite")%>' />
                        </p>
                        <p>
                            <asp:Label ID="Label4" runat="server" AssociatedControlID="ddlComapany">Компания:</asp:Label> 
                            <asp:DropDownList runat="server" ID="ddlComapany" 
                                DataSourceID="sourceCompanies" DataTextField="CompanyName" DataValueField="CompanyId"
                                SelectedValue='<%#Bind("CompanyId")%>' AppendDataBoundItems="true">
                                <asp:ListItem Text="Нет компании" Value="" />
                            </asp:DropDownList>
                            <asp:ObjectDataSource runat="server" ID="sourceCompanies" TypeName="CompanyDB" SelectMethod="GetCompanies">
                            </asp:ObjectDataSource>
                        </p>
                    </td>
                </tr>
                </table>
                </td>
                </tr>
                <tr>
                    <td>
                    <p style="text-align:center">
                        <asp:Button ID="btnUpdate" CommandName="Update" Text="Обновить" runat="server"></asp:Button>
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnCancel" CommandName="Cancel" Text="Отмена" runat="server"></asp:Button>
                    </p>
                    </td>
                </tr>
            </table>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <h1 style="margin: 20px; text-align:center;">у этого сотрудника нет подчиненных</h1>
        </EmptyDataTemplate>
    </asp:ListView>
    <asp:ObjectDataSource ID="sourceSubordinationsUsers" runat="server" 
        SelectMethod="GetSubordinationsUserInfo" TypeName="SubordinationDB"  UpdateMethod="UpdateUserInfo"
        onselecting="sourceSubordinationsUsers_Selecting" 
        onupdating="sourceSubordinationsUsers_Updating">
        <SelectParameters>
            <asp:Parameter Name="objUserId" Type="Object" />
        </SelectParameters>
        <UpdateParameters > 
            <asp:Parameter Name="UserInfoCommonId" Type="Int32" />
            <asp:Parameter Name="CompanyId" Type="Int32" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="FirstName" Type="String" />
            <asp:Parameter Name="LastName" Type="String" />
            <asp:Parameter Name="Patronymic" Type="String" />
            <asp:Parameter Name="TownId" Type="Int64" />
            <asp:Parameter Name="Address" Type="String" />
            <asp:Parameter Name="Sex" Type="Boolean" ConvertEmptyStringToNull="true" />
            <asp:Parameter Name="DateOfBirth" Type="DateTime" />
            <asp:Parameter Name="Foto" Type="Byte" />
            <asp:Parameter Name="FotoMimeType" Type="String" />
            <asp:Parameter Name="WebSite" Type="String" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="UserInfoCommonId" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="sourceUserContactPhones" runat="server" 
        SelectMethod="GetPhonesOfUser" TypeName="PhoneDB">
        <SelectParameters>
            <asp:Parameter Name="UserInfoCommonId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</div>