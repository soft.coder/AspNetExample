﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactPhoneInit.ascx.cs" Inherits="UserControls_ContactPhoneInit" EnableViewState="true" %>
<p>
<asp:GridView ID="gwPhones" runat="server"
    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
    GridLines="None" style="margin-right: 0px" Width="100%">
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
        <asp:BoundField HeaderText="Тип" DataField="TypePhoneName" />
        <asp:BoundField HeaderText="Номер" DataField="PhoneNumber" />
        <asp:BoundField HeaderText="Описание" DataField="PhoneDescription" />
    </Columns>                                    
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" HorizontalAlign="Center" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />                                    
</asp:GridView>
</p>
<p>
<asp:Label ID="lblPhoneNumber" runat="server" AssociatedControlID="txtPhoneNumber">Номер телефона:</asp:Label>
<asp:TextBox ID="txtPhoneNumber" CssClass="textEntry" runat="server" MaxLength="100"></asp:TextBox>
<asp:RequiredFieldValidator ID="PhoneNumberRequiredFieldValidator" runat="server" 
            ControlToValidate="txtPhoneNumber"
            ErrorMessage="Для добавления контактного телефона нужно ввести номер телефона"
            ToolTip="Для добавления контактного телефона нужно ввести номер телефона"
            ValidationGroup="AddPhoneValidationGroup"
            CssClass="failureNotification">*
</asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="PhoneNubmerRegularExpressionValidator" runat="server" 
        ErrorMessage="В поле номер телефона должно вводиться число" 
        ToolTip="В поле номер телефона должно вводиться число"
        ControlToValidate="txtPhoneNumber" ValidationExpression="[\d|\-|\(|\)]+"
        ValidationGroup="AddPhoneValidationGroup"
        CssClass="failureNotification">*
</asp:RegularExpressionValidator>
</p>
<p>
<asp:Label ID="lblPhoneDescription" runat="server" AssociatedControlID="txtPhoneDescription">Описание:</asp:Label>
<asp:TextBox ID="txtPhoneDescription" CssClass="textEntry" runat="server" MaxLength="300"></asp:TextBox>
</p>
<p>
<asp:Label ID="lblTypePhone" runat="server" AssociatedControlID="ddlTypePhone">Тип телефона:</asp:Label>
<asp:DropDownList ID="ddlTypePhone" runat="server" 
    DataSourceID="sourceTypePhones"
    DataTextField="TypePhoneName" 
    DataValueField="TypePhoneId"
    AppendDataBoundItems="true">
    <asp:ListItem Text="Выберите тип телефона" Value=""></asp:ListItem>
</asp:DropDownList>
<asp:ObjectDataSource ID="sourceTypePhones" runat="server" 
    SelectMethod="GetTypesOfPhone" TypeName="PhoneDB"></asp:ObjectDataSource>
</p>
<p class="submitButton">
<asp:Button ID="btnAddPhone" runat="server" Text="Добавить" OnClick="btnAddPhone_OnClick"
        ValidationGroup="AddPhoneValidationGroup"/>
</p>
<p>
<asp:ValidationSummary ID="AddPhoneValidationSummary" runat="server" CssClass="failureNotification" 
        ValidationGroup="AddPhoneValidationGroup"/>
</p>